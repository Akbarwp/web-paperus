@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Penjualan Stok Barang Jadi</h1>
        </div>
        
        @if (session()->has('error'))
            <div class="alert alert-danger" role="alert">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M12.0007 10.5865L16.9504 5.63672L18.3646 7.05093L13.4149 12.0007L18.3646 16.9504L16.9504 18.3646L12.0007 13.4149L7.05093 18.3646L5.63672 16.9504L10.5865 12.0007L5.63672 7.05093L7.05093 5.63672L12.0007 10.5865Z" fill="rgba(255,0,0,1)"></path></svg>
                {{ session('error') }}
            </div>
        @endif

        <div class="form-group">
            <form action="{{ url('/doAddpenjualanbarangjadi') }}" method="post">
                @csrf
                <label class="label" for="readonlyTextInput">kode Pembuatan</label>
                <input id="readonlyTextInput" class="form-control" placeholder="Kode Pembuatan" name="kode_pembuatan" value="{{ $kode }}" readonly>

                <label class="label">Customer</label>
                <select class="form-select form-control d-block selectpicker border border-secondary" name="id_customer" id="id_customer" aria-label="Default select example">
                    <option selected disabled>Open this select menu</option>
                    @foreach ($customer as $item)
                        <option value="{{ $item->id_customer }}">{{ $item->nama_customer }}</option>
                    @endforeach
                </select>

                {{-- <div id="penjualan" style="display: none"> --}}
                <div id="penjualan">

                    <label class="label">Barang Jadi</label>
                    <select class="form-select form-control d-block selectpicker border border-secondary" name="id_produksi" id="id_produksi" aria-label="Default select example">
                        <option selected disabled>Open this select menu</option>
                        @foreach ($barangJadi as $item)
                            <option value="{{ $item->id_produksi }}">{{ $item->jenis_box }}</option>
                        @endforeach
                    </select>

                    <label for="exampleFormControlTextarea1" class="label">Quantity</label>
                    <input type="number" class="form-control" placeholder="Masukkan Quantity" name="qty">
                    
                    <label for="exampleFormControlTextarea1" class="label">Harga Satuan</label>
                    <input type="number" id="formHarga" class="form-control" placeholder="Masukkan harga satuan" name="harga_satuan">

                    <label for="exampleFormControlTextarea1" class="label">Total Harga</label>
                    <input class="form-control" type="number" placeholder="Masukkan total harga" name="total_harga" readonly>

                    <div style="display:flex;">
                        <div style="margin-left: -3%;" class="col-sm-6"></div>
                    </div>
                    <div style="display: flex;width: 100%;">
                        <div style="width: 100%;">
                            <label for="exampleFormControlTextarea1" class="label">Diskon</label>
                            <div style="display: flex;">
                                <input class="form-control" type="number" style="width: 100%;" placeholder="Masukkan Diskon" name="diskon" onkeyup="disc()">
                                <button readonly="readonly" class="btn" style="height: 10px;">%</button>
                            </div>
                        </div>
                        <div style="width: 100%;">
                            <label for="exampleFormControlTextarea1" class="label">Potongan Harga</label>
                            <input class="form-control" type="number" style="width: 100%;" placeholder="Masukkan Potongan Harga" name="potongan" onkeyup="pot()">
                        </div>
                    </div>

                    <label for="exampleFormControlTextarea1" class="label">Nett</label>
                    <input class="form-control" type="number" placeholder="Masukkan Nett" name='nett' readonly>
                </div>
                <br>
                {{-- <input type="hidden" name="data"> --}}
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <script>
        
        $("[name='qty']").keyup(function (e) { 
            var qty = parseInt($("[name='qty']").val());
            var harga_satuan = parseInt($("[name='harga_satuan']").val());
            var diskon = parseInt($("[name='diskon']").val());
            
            let total_nett = (harga_satuan*qty)-((harga_satuan*qty)*diskon/100);
            let total = (harga_satuan*qty);
            
            $("#nett").val(total_nett);
            $("[name='total_harga']").val(total);
        });

        $("[name='harga_satuan']").keyup(function (e) { 
            var qty = parseInt($("[name='qty']").val());
            var harga_satuan = parseInt($("[name='harga_satuan']").val());
            var diskon = parseInt($("[name='diskon']").val());
            
            let total_nett = (harga_satuan*qty)-((harga_satuan*qty)*diskon/100);
            let total = (harga_satuan*qty);
            
            $("[name='nett']").val(total_nett);
            $("[name='total_harga']").val(total);
        });

        function pot(){
            var potongan = parseInt($("[name='potongan']").val());
            var qty = parseInt($("[name='qty']").val());
            var harga_satuan = parseInt($("[name='harga_satuan']").val());
            
            var temp = (potongan/(qty*harga_satuan))*100;
            $("[name='diskon']").val(temp);

            var diskon = parseInt($("[name='diskon']").val());
            var nett = (harga_satuan*qty)-((harga_satuan*qty)*diskon/100);
            $("[name='nett']").val(nett);
        }

        function disc(){
            var diskon = parseInt($("[name='diskon']").val());
            var qty = parseInt($("[name='qty']").val());
            var harga_satuan = parseInt($("[name='harga_satuan']").val());
            
            var temp = (qty*harga_satuan)*(diskon/100);
            $("[name='potongan']").val(temp);
            
            var nett = (harga_satuan*qty)-((harga_satuan*qty)*diskon/100);
            $("[name='nett']").val(nett);
        }
    </script>
@endsection
