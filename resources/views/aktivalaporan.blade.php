@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="row mr-2">
        <div class="col-md-12">
            <div class="float-right">
                <a href="#" data-toggle="modal" data-target="#excel" target="_blank" class="btn btn-success btn-sm mb-2">
                    <i class="fa fa-file-excel"></i> Export Excel
                </a>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="excel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Export Excel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('/accounting/laporan_aktiva/printexcel') }}" target="_blank" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col justify-content-center d-flex">
                                    <div class="form ml-3">
                                        <input class="form-check-input" type="radio" name="jenis_aktiva" id="semua"
                                            value="Semua" checked>
                                        <label class="form-check-label mt-1" for="semua">
                                            Semua
                                        </label>
                                    </div>
                                </div>
                                <div class="col justify-content-center d-flex">
                                    <div class="form ml-3">
                                        <input class="form-check-input" type="radio" name="jenis_aktiva" id="tetap"
                                            value="Tetap">
                                        <label class="form-check-label mt-1" for="tetap">
                                            Aktiva Tetap
                                        </label>
                                    </div>
                                </div>
                                <div class="col justify-content-center d-flex">
                                    <div class="form ml-3">
                                        <input class="form-check-input" type="radio" name="jenis_aktiva" id="lancar"
                                            value="Lancar">
                                        <label class="form-check-label mt-1" for="lancar">
                                            Aktiva Lancar
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Export</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-rounded">
                    <div class="card">
                        <div class="row mx-2 mt-3">
                            <div class="col">
                                <h4 class="h3 mb-0 text-gray-800">Laporan Aktiva</h4>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <select name="jenis" class="form-control" id="jenis">
                                        <option value="">Semua Aktiva</option>
                                        <option value="tetap">Aktiva Tetap</option>
                                        <option value="lancar">Aktiva Lancar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <input type="date" id="tanggal" name="tanggal" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <select name="jenis_aktiva" id="kategori" class="form-control">
                                        <option value="">Semua</option>
                                        <option value="Biaya bulanan">
                                            Biaya bulanan</option>
                                        <option value="Pemeliharaan">
                                            Pemeliharaan</option>
                                        <option value="Servis">
                                            Servis</option>
                                        <option value="Pajak">
                                            Pajak</option>
                                        <option value="Kendaraan">
                                            Kendaraan</option>
                                        <option value="Pembelian aset">
                                            Pembelian aset</option>
                                        <option value="Rumah">
                                            Rumah</option>
                                        <option value="Tanah">
                                            Tanah</option>
                                        <option value="Lain-lain">
                                            Lain-lain</option>
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="col-2 mr-1">
                                <a href="#" data-toggle="modal" data-target="#excel" target="_blank"
                                    class="btn btn-success btn-sm">
                                    <i class="fa fa-file-excel"></i> Export Excel
                                </a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-rounded">
                    <div class="card-body" id="table-tetap">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="tbl-tetap">
                                        <thead>
                                            <tr>
                                                <td>120</th>
                                                <td colspan="6">Aktiva Tetap</td>
                                            </tr>
                                            <tr>
                                                <th>Kode</th>
                                                <th>Tanggal</th>
                                                <th>Kategori</th>
                                                <th>Nominal</th>
                                                <th>Durasi</th>
                                                <th>Penyusutan</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $total = 0;
                                            $total_pen = 0;
                                            @endphp
                                            @foreach ($aktiva_tetap as $item)
                                            <tr>
                                                <td>{{ $item->kode_nama_akun }}.{{$item->kode}}</td>
                                                <td id="tgl">{{ $item->created_at->format('d/m/Y') }}</td>
                                                @if ($item->jenis_lain == '-')
                                                <td id="ktg">{{ $item->jenis_aktiva }}</td>
                                                @else
                                                <td id="ktg">{{ $item->jenis_lain }}</td>
                                                @endif
                                                <td>Rp. {{ number_format($item->nominal, 0,',','.') }}</td>
                                                <td>{{ $item->durasi_aktiva }} bln</td>
                                                <td>Rp. {{ number_format($item->penyusutan, 0,',','.') }}</td>
                                                <td>{{ $item->keterangan }}</td>
                                            </tr>
                                            @php
                                            $total += $item->nominal;
                                            $total_pen += $item->penyusutan;
                                            @endphp
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>Rp. {{ number_format($total, 0,',','.') }}</th>
                                                <th>&nbsp;</th>
                                                <th>Rp. {{ number_format($total_pen, 0,',','.') }}</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-rounded">
                    <div class="card-body" id="table-lancar">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="tbl-lancar">
                                        <thead>
                                            <tr>
                                                <td>110</th>
                                                <td colspan="6">Aktiva Lancar</td>
                                            </tr>
                                            <tr>
                                                <th>Kode</th>
                                                <th>Tanggal</th>
                                                <th>Kategori</th>
                                                <th>Biaya</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $total_biaya = 0;
                                            @endphp
                                            @foreach ($aktiva_lancar as $item)
                                            <tr>
                                                <td>{{ $item->kode_nama_akun }}.{{$item->kode}}</td>
                                                <td id="tgl">{{ $item->created_at->format('d/m/Y') }}</td>
                                                @if ($item->jenis_lain == '-')
                                                <td id="ktg">{{ $item->jenis_aktiva }}</td>
                                                @else
                                                <td id="ktg">{{ $item->jenis_lain }}</td>
                                                @endif
                                                <td>Rp. {{ number_format($item->biaya, 0,',','.') }}</td>
                                                <td>{{ $item->keterangan }}</td>
                                            </tr>
                                            @php
                                            $total_biaya += $item->biaya;
                                            @endphp
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>Rp. {{ number_format($total_biaya, 0,',','.') }}</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#jenis').change(function () {
                var jenis = $(this).val();
                if (jenis == '') {
                    $('#table-tetap').show();
                    $('#table-lancar').show();
                } else if (jenis == 'tetap') {
                    $('#table-tetap').show();
                    $('#table-lancar').hide();
                } else if (jenis == 'lancar') {
                    $('#table-tetap').hide();
                    $('#table-lancar').show();
                }
            });
        });

        //datatable tetap and lancar
        $(document).ready(function () {
            $('#tbl-tetap').DataTable({
                dom: 'lBfrtip',
                "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                buttons: [
                    'colvis',
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            { 
                                extend: 'print',
                                footer: true 
                            },
                            { 
                                extend: 'copyHtml5',
                                footer: true 
                            },
                            { 
                                extend: 'pdfHtml5',
                                footer: true 
                            },
                        ]
                    },
                ],
            });
            $('#tbl-lancar').DataTable({
                dom: 'lBfrtip',
                "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                buttons: [
                    'colvis',
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            { 
                                extend: 'print',
                                footer: true 
                            },
                            { 
                                extend: 'copyHtml5',
                                footer: true 
                            },
                            { 
                                extend: 'pdfHtml5',
                                footer: true 
                            },
                        ]
                    },
                ],
            });
        });

        //button reset
        function reset() {
            $('#table-tetap').show();
            $('#table-lancar').show();
            $('#jenis').val('');
            $('#tanggal').datepicker('setDate', 'today');
            $('#kategori').val('');
        }

        // function reset() {
        //     var jenis = $('#jenis').val();
        //     var tanggal = $('#tanggal').val();
        //     var kategori = $('#kategori').val();

        //     $('#jenis').val('');
        //     $('#tanggal').val('');
        //     $('#kategori').val('');
            
        //     if (jenis == '' && tanggal == '' && kategori == '') {
        //         $('#table-tetap').show();
        //         $('#table-lancar').show();
        //     }
        // }

        $(document).ready(function () {
            $('#jenis').change(function () {
                var jenis = $(this).val();
                if (jenis == '') {
                    $('#kategori').empty();
                    $('#kategori').append('<option value="">Semua</option>');
                    $('#kategori').append('<option value="Kendaraan">Kendaraan</option>');
                    $('#kategori').append('<option value="Pembelian aset">Pembelian aset</option>');
                    $('#kategori').append('<option value="Rumah">Rumah</option>');
                    $('#kategori').append('<option value="Tanah">Tanah</option>');
                    $('#kategori').append('<option value="Biaya bulanan">Biaya bulanan</option>');
                    $('#kategori').append('<option value="Pemeliharaan">Pemeliharaan</option>');
                    $('#kategori').append('<option value="Servis">Servis</option>');
                    $('#kategori').append('<option value="Pajak">Pajak</option>');
                    $('#kategori').append('<option value="Lain-lain">Lain-lain</option>');
                } else if (jenis == 'lancar') {
                    $('#kategori').empty();
                    $('#kategori').append('<option value="">Kategori Aktiva Lancar</option>');
                    $('#kategori').append('<option value="Biaya bulanan">Biaya bulanan</option>');
                    $('#kategori').append('<option value="Pemeliharaan">Pemeliharaan</option>');
                    $('#kategori').append('<option value="Servis">Servis</option>');
                    $('#kategori').append('<option value="Pajak">Pajak</option>');
                    $('#kategori').append('<option value="Lain-lain">Lain-lain</option>');
                }else if(jenis == 'tetap'){
                    $('#kategori').empty();
                    $('#kategori').append('<option value="">Kategori Aktiva Tetap</option>');
                    $('#kategori').append('<option value="Kendaraan">Kendaraan</option>');
                    $('#kategori').append('<option value="Pembelian aset">Pembelian aset</option>');
                    $('#kategori').append('<option value="Rumah">Rumah</option>');
                    $('#kategori').append('<option value="Tanah">Tanah</option>');
                    $('#kategori').append('<option value="Lain-lain">Lain-lain</option>');
                }
            });
        });

        $(document).ready(function () {
            $('#tanggal').change(function () {
                var tanggal = $(this).val();
                var jenis = $('#jenis').val();
                if (jenis == '') {
                    if (tanggal == '') {
                        $('#table-tetap tbody tr').show();
                        $('#table-lancar tbody tr').show();
                    } else {
                        $('#table-tetap tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];
                            if (tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                        $('#table-lancar tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];
                            if (tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                } else if (jenis == 'tetap') {
                    if (tanggal == '') {
                        $('#table-tetap tbody tr').show();
                    } else {
                        $('#table-tetap tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];
                            if (tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                } else if (jenis == 'lancar') {
                    if (tanggal == '') {
                        $('#table-lancar tbody tr').show();
                    } else {
                        $('#table-lancar tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];
                            if (tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                }
            });
        });

        //get value kategori
        $(document).ready(function () {
            $('#kategori').change(function () {
                var kategori = $(this).val();
                var jenis = $('#jenis').val();
                var tanggal = $('#tanggal').val();
                if (jenis == '') {
                    if (kategori == '' && tanggal == '') {
                        $('#table-tetap tbody tr').show();
                        $('#table-lancar tbody tr').show();
                    } else {
                        $('#table-tetap tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];

                            var kategori_tetap = $(this).find('#ktg').text();

                            if (kategori_tetap == kategori && tgl_join == tanggal || kategori_tetap == kategori && tanggal == '' || kategori == '' && tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                        $('#table-lancar tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];

                            var kategori_lancar = $(this).find('#ktg').text();
                            if (kategori_lancar == kategori && tgl_join == tanggal || kategori_lancar == kategori && tanggal == '' || kategori == '' && tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                } else if (jenis == 'tetap') {
                    if (kategori == '' && tanggal == '') {
                        $('#table-tetap tbody tr').show();
                    } else {
                        $('#table-tetap tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];

                            var kategori_tetap = $(this).find('#ktg').text();
                            if (kategori_tetap == kategori && tgl_join == tanggal || kategori_tetap == kategori && tanggal == '' || kategori == '' && tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                } else if (jenis == 'lancar') {
                    if (kategori == '' && tanggal == '') {
                        $('#table-lancar tbody tr').show();
                    } else {
                        $('#table-lancar tbody tr').each(function () {
                            var tgl = $(this).find('#tgl').text();
                            var tgl_split = tgl.split('/');
                            var tgl_join = tgl_split[2] + '-' + tgl_split[1] + '-' + tgl_split[0];

                            var kategori_lancar = $(this).find('#ktg').text();

                            if (kategori_lancar == kategori && tgl_join == tanggal || kategori_lancar == kategori && tanggal == '' || kategori == '' && tgl_join == tanggal) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                }
            });
        });
    </script>
@endsection
