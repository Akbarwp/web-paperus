<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Jurnal Harian Single</title>

</head>
<style>
    /* font size */
    * {
        font-size: 11px;
        /* font-family: Verdana, Arial, sans-serif; */
    }

    .table-bordered thead tr th,
    .table-bordered tbody tr th,
    .table-bordered tfoot tr th,
    .table-bordered thead tr td,
    .table-bordered tbody tr td,
    .table-bordered tfoot tr td {
        border: 0.5px solid rgb(160, 160, 160);
    }

    .table-bordered thead tr th,
    .table-bordered tbody tr td,
    .table-bordered tfoot tr td {
        padding: 2px;
    }
</style>
<body>
    <button id="prr" onclick="this.style.display='none'; print(); this.style.display='block';"> > PRINT < </button>
    @php
        $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
        $month = date('m', strtotime($start_date));
        $year  = date('Y', strtotime($start_date));
    @endphp
    <h1><center>CV. Anzac Food</center></h1>
    <h1><center>Laporan Jurnal Harian per Akun</center></h1>
    <h2><center>{{ date('d', strtotime($start_date)) }} s/d {{ date('d', strtotime($end_date)) }} {{$bulan}} {{ $year }}</center></h2>
    @foreach($buku_besars as $buku_besar)
        <table class="" style="width: 100%;">
            <colgroup>
                <col style="width: 15%;">
                <col style="width: 20%;">
                <col style="width: 15%;">
                <col style="width: 15%;">
                <col style="width: 15%;">
                <col style="width: 15%;">
            </colgroup>
            <tr>
                <th style="text-align: left;" colspan="6">
                    Laporan {{$buku_besar['nama']}} : {{ date('d-m-Y', strtotime($start_date))}} - {{ date('d-m-Y', strtotime($end_date))}}
                </th>
            </tr>
            <tr>
                <td>Kode</td>
                <td colspan="4">{{$buku_besar['kode_akun']}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center">Saldo Awal</td>
                <td style="text-align:right">{{ number_format($buku_besar['saldo_awal'], 2, ",", ".") }}</td>
            </tr>
        </table>
        <table class="table table-bordered" style="width: 100%; margin-bottom: 20px;">
            <thead>
                <tr>
                    <th style="text-align:center; width: 15%">No Bukti</th>
                    <th style="text-align:center; width: 15%">Tgl Bukti</th>
                    <th style="text-align:center; width: 25%">Keterangan</th>
                    <th style="text-align:center; width: 15%">Masuk</th>
                    <th style="text-align:center; width: 15%">Keluar</th>
                    <th style="text-align:center; width: 15%">Saldo</th>
                </tr>
            </thead>
            <tbody>
                @php $tdebit = 0; $tkredit= 0; $tsaldo_akhir = 0; @endphp
                @foreach($buku_besar['jurnals'] as $jurnal)
                    @php $tdebit += $jurnal->debit; $tkredit += $jurnal->kredit; $tsaldo_akhir = $jurnal->saldo_akhir; @endphp
                    <tr>
                        <td>{{ $jurnal->bukti_transaksi}}</td>
                        <td>{{ date('d-m-Y', strtotime($jurnal->tanggal))}}</td>
                        <td>{{ $jurnal->keterangan}}</td>
                        <td style="text-align: rigth">{{ number_format($jurnal->debit, 0, ",", ".")}}</td>
                        <td style="text-align: rigth">{{ number_format($jurnal->kredit, 0, ",", ".")}}</td>
                        <td style="text-align: rigth">{{ number_format($jurnal->saldo_akhir, 0, ",", ".")}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td>Total</td>
                    <td style="text-align: rigth">{{number_format($tdebit, 0, ",", ".")}}</td>
                    <td style="text-align: rigth">{{number_format($tkredit, 0, ",", ".")}}</td>
                    <td style="text-align: rigth">{{number_format($tsaldo_akhir, 0, ",", ".")}}</td>
                </tr>
            </tbody>
        </table>
    @endforeach
    <script>
        print(){
            window.onload = function() { 
                document.getElementById("prr").style.display='none';
                window.print(); 
            }
            window.onafterprint = window.close;
            document.getElementById("prr").style.display='block';
        };
    </script>
</body>

</html>