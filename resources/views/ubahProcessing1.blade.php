@extends('layouts.master')
@section('content')
    <div class="container">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Ubah Processing1</h1>
        </div>
        @if ($errors->any())
            @foreach ($errors->all() as $err)
                <div class="alert alert-danger">{{ $err }}</div>
            @endforeach
        @endif
        <form method="post">
            @csrf
            <div class="form-group">

                @if ($cekEditSPK == 1)
                    <label class="label" for="readonlyTextInput">ID Processing1</label>
                    <input name="id" id="readonlyTextInput" class="form-control" value="{{ $processing1->id_proses1 }}" readonly>
                @else
                    <label class="label" for="readonlyTextInput">ID Processing Barang Jadi1</label>
                    <input name="id" id="readonlyTextInput" class="form-control" value="{{ $processing1->id_proses_barang_jadi }}" readonly>
                @endif

                <label class="label">Nama Proses</label>
                <input name="nama" class="form-control" placeholder="Masukkan Nama" value="{{ $processing1->proses }}" readonly>

                @if ($cekEditSPK == 1)
                    <label class="label">Nama Vendor</label>
                    <select data-live-search="true" class="selectpicker form-control" name="nama_brand" id="nama_brand">
                        @foreach ($vendor as $item)
                            <option value="{{ $item->nama_vendor }}" @if ($item->nama_vendor == $processing1->nama_brand) selected @endif>{{ $item->nama_vendor }}</option>
                        @endforeach
                    </select>
                @endif

                <label class="label">Jumlah</label>
                <input name="jumlah" id="jumlah" type="number" class="form-control" value="{{ $processing1->jumlah }}" onkeyup="total()" required>

                <label class="label">Harga Satuan</label>
                <input name="harga_satuan" id="harga_satuan" type="number" class="form-control" value="{{ $processing1->harga_satuan }}" onkeyup="total()" required>

                <label class="label">Harga Total</label>
                <input name="harga_total" id="harga_total" type="number" class="form-control" value="{{ $processing1->harga_total }}" readonly>

                <label class="label">Penerimaan</label>
                <div class="form-check">
                    <input class="form-check-input" value="1" type="radio" name="status" id="flexRadioDefault1" @if ($processing1->status == 1) checked @endif>
                    <label class="form-check-label" for="flexRadioDefault1">
                        Diterima
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" value="0" type="radio" name="status" id="flexRadioDefault2" @if ($processing1->status == 0) checked @endif>
                    <label class="form-check-label" for="flexRadioDefault2">
                        Ditolak
                    </label>
                </div>

                <br>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>

    <script>
        function total() {
            const jumlah = parseInt($("[name='jumlah']").val());
            const satuan = parseInt($("[name='harga_satuan']").val());
            const total = jumlah * satuan;

            $("[name='harga_total']").val(total);
        }
    </script>
@endsection
