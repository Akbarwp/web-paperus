@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Form DP/Pembayaran</h1>
        </div>
        <div class="form-group">
            <form method="post">
                @csrf
                <input type="text" name="kode" id="kode" value="{{ $pembayaran->id_pembayaran }}">
                
                <label class="label" for="readonlyTextInput">No. Penawaran</label>
                <select data-live-search="true" class="selectpicker form-control" name="id_penawaran">
                    <option selected>Pilih No. Penawaran</option>
                    @foreach ($penawaran as $prm)
                        <option value={{ $prm->id_penawaran }} @if ($prm->id_penawaran == $pembayaran->id_penawaran) selected @endif>{{ $prm->id_penawaran }}</option>
                    @endforeach
                </select>

                <label class="label">Nama Brand</label>
                <input class="form-control" name="nama_brand" placeholder="Masukkan Nama Brand" value="{{ $pembayaran->nama_brand }}">

                <label class="label">Nama PIC</label>
                <input class="form-control" name="pic" placeholder="Masukkan Nama PIC" value="{{ $pembayaran->pic }}">

                <label for="exampleFormControlTextarea1" class="label">Jenis Box</label>
                <input class="form-control" name="jenis_box" placeholder="Jenis Box" value="{{ $pembayaran->jenis_box }}">

                <label for="exampleFormControlTextarea1" class="label">Quantity</label>
                <input type="number" class="form-control" name="qty" placeholder="Masukkan Quantity" value="{{ $pembayaran->qty }}">

                <label for="exampleFormControlTextarea1" class="label">Jumlah Produksi</label>
                <input type="number" class="form-control" name="jum_produksi" placeholder="Masukkan Jumlah" value="{{ $pembayaran->jum_produksi }}">

                <label for="exampleFormControlTextarea1" class="label">Harga</label>
                <input type="number" class="form-control" name="harga" placeholder="Masukkan Harga" value="{{ $pembayaran->harga }}">

                <label for="exampleFormControlTextarea1" class="label">Pembayaran</label>
                <input class="form-control" name="pembayaran" onchange="sisaPembayaran()" placeholder="Masukkan Pembayaran" value="{{ $pembayaran->pembayaran }}">

                <label for="exampleFormControlTextarea1" class="label">Sisa</label>
                <input class="form-control" name="sisa" placeholder="Masukkan Sisa" value="{{ $pembayaran->sisa }}">

                <label for="exampleFormControlTextarea1" class="label">Termin Pembayaran</label>
                <input class="form-control" type="date" name="termin" value="<?php echo date('Y-m-d'); ?>" placeholder="Masukkan Termin Pembayaran">

                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <script>
        function sisaPembayaran() {
            var harga = parseInt($("[name='harga']").val());
            var qty = parseInt($("[name='qty']").val());
            var pembayaran = parseInt($("[name='pembayaran']").val());
            var temp = (harga * qty) - pembayaran;
            $("[name='sisa']").val(temp);
        }

        function nama_brand_change() {
            $.ajax({
                url: "autocomplete.php",
                method: "POST",
                data: {
                    query: $("[name='id_penawaran']").val(),
                    ctr: "Pembayaran"
                },
                success: function(data) {
                    var temp = data.split(",");
                    $("[name='nama_brand']").val(temp[0]);
                    $("[name='pic']").val(temp[1]);
                    $("[name='jenis_box']").val(temp[2]);
                    $("[name='qty']").val(temp[3]);
                    $("[name='jum_produksi']").val(temp[4]);
                    $("[name='harga']").val(temp[5]);
                }
            });
        }
    </script>
@endsection
