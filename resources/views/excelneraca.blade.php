<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Neraca {{now()}}</title>
</head>

<body>
    @php $sep_thsnd = $export_type == "export" ? "" : "."; @endphp
    @if ($export_type == "pdf")
    @php
        $bulan = \Carbon\Carbon::parse($now)->translatedFormat('F');
        $month = date('m', strtotime($now));
        $year  = date('Y', strtotime($now));
    @endphp
    <h3 class="title"><center>CV. Anzac Food</center></h3>
    <h3 class="title"><center>Laporan Neraca</center></h3>
    <h4 class="title"><center>{{ date('01', strtotime($now)) }} s/d {{ date('t', strtotime($now)) }} {{$bulan}} {{ $year }}</center></h4>
    @endif
    @foreach($comparisons as $idxSaldo => $comparison)
    <table style="width: 100%;">
        @if(count($comparisons) > 1)
        <thead>
            <tr>
                <th colspan="8" style="text-aling: center;">{{ $comparison->translatedFormat('F') }}</th>
            </tr>
        </thead>
        @endif
        <tbody>
            <tr>
                <td colspan="8"></td>
            </tr>
            <tr>
                <th style="text-align: left; border-bottom: 1px double black;">AKTIVA</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: right; border-bottom: 1px double black;">PASSIVA</th>
            </tr>

            {{-- Aktiva Lancar & Hutang Lancar --}}
            <tr>
                <th colspan="5" style="text-align: left; text-decoration: underline;">AKTIVA LANCAR :</th>
                <th colspan="3" style="text-align: left; text-decoration: underline;">HUTANG LANCAR :</th>
            </tr>
            @php 
                $total_data = max(count($neracas['Aktiva']['Aktiva Lancar']), count($neracas['Passiva']['Kewajiban']));
                $total_data = $total_data > 0 ? $total_data + 2 : 0;
                $total_aktiva_lancar = 0;
                $total_hutang = 0;
            @endphp
            @for ($i=0; $i<=$total_data;$i++)
                <tr>
                    {{-- Aktiva Lancar --}}
                    @php $akun = $neracas['Aktiva']['Aktiva Lancar'][$i] ?? null; @endphp
                    @if (!$akun)
                        <td colspan="5">&nbsp;</td>
                    @else
                        <td>{{ $akun['nama_akun'] }}</td>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">Rp.</td>
                        <td style="text-align: right;" data-format="#,##0">{{number_format($akun['saldo_akhir'][$idxSaldo],0,',',$sep_thsnd)}}</td>
                        @php $total_aktiva_lancar += $akun['saldo_akhir'][$idxSaldo]; @endphp
                    @endif

                    {{-- Hutang Lancar --}}
                    @php $akun = $neracas['Passiva']['Kewajiban'][$i] ?? null @endphp
                    @if (!$akun)
                        <td colspan="3">&nbsp;</td>
                    @else
                        <td>{{ $akun['nama_akun'] }}</td>
                        <td style="text-align: right;">Rp.</td>
                        <td style="text-align: right;" data-format="#,##0">{{number_format($akun['saldo_akhir'][$idxSaldo],0,',',$sep_thsnd)}}</td>
                        @php $total_hutang += $akun['saldo_akhir'][$idxSaldo]; @endphp
                    @endif
                </tr>
            @endfor
            <tr>
                <td>Jumlah aktiva lancar</td>
                <td></td>
                <td></td>
                <td style="text-align: right;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" data-format="#,##0">{{number_format($total_aktiva_lancar,0,',',$sep_thsnd)}}</td>
                <td>Jumlah Hutang</td>
                <td style="text-align: right;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" data-format="#,##0">{{number_format($total_hutang,0,',',$sep_thsnd)}}</td>
            </tr>
            {{-- ============================= --}}

            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>

            {{-- Aktiva Tetap & Modal --}}
            <tr>
                <th colspan="5" style="text-align: left; text-decoration: underline;">AKTIVA TETAP :</th>
                <th colspan="3" style="text-align: left; text-decoration: underline;">MODAL :</th>
            </tr>
            @php 
                $total_data = max(count($neracas['Aktiva']['Aktiva Tetap']), count($neracas['Passiva']['Modal']));
                // $total_data = $total_data > 0 ? $total_data + 2 : 0;
                $total_aktiva_tetap = 0;
                $total_modal = 0;
            @endphp
            @for ($i=0; $i<=$total_data;$i++)
                <tr>
                    {{-- Aktiva Tetap --}}
                    @php $akun = $neracas['Aktiva']['Aktiva Tetap'][$i] ?? null; @endphp
                    @if (!$akun)
                        <td colspan="5">&nbsp;</td>
                    @else
                        <td>{{ $akun['nama_akun'] }}</td>
                        <td style="text-align: right;">Rp.</td>
                        <td style="text-align: right;" data-format="#,##0">{{number_format($akun['saldo_akhir'][$idxSaldo],0,',',$sep_thsnd)}}</td>
                        <td></td>
                        <td></td>
                        @php $total_aktiva_tetap += $akun['saldo_akhir'][$idxSaldo]; @endphp
                    @endif

                    {{-- Modal --}}
                    @php $akun = $neracas['Passiva']['Kewajiban'][$i] ?? null @endphp
                    @if (!$akun)
                        <td colspan="3">&nbsp;</td>
                    @else
                        <td>{{ $akun['nama_akun'] }}</td>
                        <td style="text-align: right;">Rp.</td>
                        <td style="text-align: right;" data-format="#,##0">{{number_format($akun['saldo_akhir'][$idxSaldo],0,',',$sep_thsnd)}}</td>
                        @php $total_modal += $akun['saldo_akhir'][$idxSaldo]; @endphp
                    @endif
                </tr>
            @endfor
            <tr>
                <td>Jumlah aktiva tetap</td>
                <td style="text-align: right;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" data-format="#,##0">{{number_format($total_aktiva_tetap,0,',',$sep_thsnd)}}</td>
                <td></td>
                <td></td>
                <td>Jumlah modal</td>
                <td style="text-align: right;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" data-format="#,##0">{{number_format($total_modal,0,',',$sep_thsnd)}}</td>
            </tr>
            {{-- ==================== --}}

            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>

            <tr>
                <th style="text-align: left;">TOTAL AKTIVA</th>
                <th></th>
                <th></th>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px double black;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px double black;" data-format="#,##0">{{number_format($total_aktiva_lancar + $total_aktiva_tetap,0,',',$sep_thsnd)}}</td>
                <th style="text-align: left;">TOTAL PASIVA</th>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px double black;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px double black;" data-format="#,##0">{{number_format($total_hutang + $total_modal,0,',',$sep_thsnd)}}</td>
            </tr>
        </tbody>
    </table>
    @endforeach
</body>

</html>