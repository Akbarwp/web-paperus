@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @php
                $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
                $month = date('m', strtotime($start_date));
                $year = date('Y', strtotime($start_date));
            @endphp
            <div class="col-md-12 mb-3">
                <div class="card card-noborder b-radius">
                    <div class="card-body">
                        <form id="formFilter" action="{{ url('/accounting/jurnal_harian/filter') }}" method="get">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Filter Bulan</label>
                                        <input type="month" name="bulan" id="bulan"
                                            class="form-control form-control-lg"
                                            value="{{ !request()->has('bulan') ? date('Y-m') : request()->input('bulan') }}">
                                    </div>
                                </div>
                                {{-- <div class="col-4">
                                <div class="form-group">
                                    <label class="form-label">Filter Tanggal Awal - Akhir</label>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <input type="date" name="start_date" value="{{ request()->input('start_date') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <input type="date" name="end_date" value="{{ request()->input('end_date') }}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                                {{-- <div class="col-3">
                                <div class="form-group">
                                    <label class="form-label">Filter Periode</label>
                                    <select name="filter_periode" class="form-control">
                                        <option value="">Pilih Periode</option>
                                    </select>
                                </div>
                            </div> --}}
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Filter Transaksi</label>
                                        <select class="form-control" id="transaction_type" name="transaction_type">
                                            <option value="1" @if (request()->input('transaction_type') == '1') selected @endif>PPN
                                            </option>
                                            <option value="2" @if (request()->input('transaction_type') == '2') selected @endif>Non PPN
                                            </option>
                                            <option value="all" @if (request()->input('transaction_type') == 'all') selected @endif>All
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Pencarian</label>
                                        <input type="text" class="form-control" name="search"
                                            value="{{ request()->input('search') }}"
                                            placeholder="Tanggal, Kode, Nama akun, Keterangan, Ref, Saldo">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Filter Akun</label>
                                        @php $arr_akuns = request()->input('akuns') ?? []; @endphp
                                        <select class="form-control" id="filter_akuns" name="akuns[]" multiple
                                            style="width: 100%;">
                                            @foreach ($akuns as $akun)
                                                <option value="{{ $akun->kode_akun }}"
                                                    @if (in_array($akun->kode_akun, $arr_akuns)) selected @endif
                                                    class="perusahaan_{{ $akun->perusahaan }}">{{ $akun->kode_akun }} -
                                                    {{ $akun->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="col-4">
                                <div class="form-group">
                                    <label>Filter Barang</label>
                                    @php $arr_barangs = request()->input('barangs') ?? []; @endphp
                                    <select class="form-control" id="filter_barangs" name="barangs[]" multiple style="width: 100%;">
                                        @foreach ($barangs as $barang)
                                        <option value="{{ $barang->kode_barang }}" @if (in_array($barang->kode_barang, $arr_barangs)) selected @endif 
                                            class="perusahaan_{{ $barang->status_pajak == "ppn" ? 1 : 2 }}">{{ $barang->kode_barang }} - {{ $barang->nama_barang }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> --}}
                            </div>
                        </form>
                        <div class="d-flex mb-3">
                            <button type="submit" form="formFilter" class="btn btn-primary">Filter</button>
                            <div class="d-flex ml-auto">
                                <a href="{{ url('/accounting/jurnal_harian/pdf?type=print&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-secondary ml-2">
                                    <i class="mdi mdi-eye"></i>
                                </a>
                                <a href="{{ url('/accounting/jurnal_harian/pdf?type=pdf&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-danger ml-2">
                                    <i class="mdi mdi-file-pdf"></i> PDF
                                </a>
                                <a href="{{ url('/accounting/jurnal_harian/export?type=export&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-success ml-2">
                                    <i class="mdi mdi-file-excel"></i> Excel
                                </a>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="d-flex align-items-center justify-content-center ml-auto">
                                <small>Per Akun</small>
                                <a href="{{ url('/accounting/jurnal_harian/single?type=print&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-secondary ml-2">
                                    <i class="mdi mdi-eye"></i>
                                </a>
                                <a href="{{ url('/accounting/jurnal_harian/single?type=pdf&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-danger ml-2">
                                    <i class="mdi mdi-file-pdf"></i> PDF
                                </a>
                                <a href="{{ url('/accounting/jurnal_harian/single?type=export&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-success ml-2">
                                    <i class="mdi mdi-file-excel"></i> Excel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-noborder b-radius">
                    <div class="card-header">
                        <h4>
                            Jurnal Harian Tahun {{ $year }}
                        </h4>
                        <p>Periode : {{ date('d', strtotime($start_date)) }}-{{ date('d', strtotime($end_date)) }}
                            {{ $bulan }} {{ $year }}</p>
                    </div>
                    @php
                        $total_debit = 0;
                        $total_kredit = 0;
                        foreach ($jurnal_harians as $jhs) {
                            foreach ($jhs['debit'] as $jh) {
                                $total_debit += $jh['nominal'];
                            }
                            foreach ($jhs['kredit'] as $jh) {
                                $total_kredit += $jh['nominal'];
                            }
                        }
                    @endphp
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Saldo Awal</th>
                                        <th>Rp. {{ number_format($saldo_awal_first, 0, ',', '.') }}</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Rp. {{ number_format($total_debit, 0, ',', '.') }} </th>
                                        <th>Rp. {{ number_format($total_kredit, 0, ',', '.') }}</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align:center">Tanggal</th>
                                        <th style="text-align:center">Kode Akun</th>
                                        <th style="text-align:center">Nama Akun</th>
                                        <th style="text-align:center">Keterangan</th>
                                        <th style="text-align:center">Ref.</th>
                                        <th style="text-align:center">Debit</th>
                                        <th style="text-align:center">Kredit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jurnal_harians as $jhs)
                                        @foreach ($jhs['debit'] as $jh)
                                            <tr>
                                                <td class="text-center">
                                                    {{ \Carbon\Carbon::parse($jhs['tanggal'])->format('Y-m-d') }}</td>
                                                <td>{{ $jh['kode_akun'] }}</td>
                                                <td>{{ $jh['nama_akun'] }}</td>
                                                <td>{{ $jh['keterangan'] }}</td>
                                                <td>{{ $jh['ref'] ?? '' }}</td>
                                                <td>Rp. {{ number_format($jh['nominal'], 0, ',', '.') }}</td>
                                                <td>Rp. 0</td>
                                            </tr>
                                        @endforeach
                                        @foreach ($jhs['kredit'] as $jh)
                                            <tr>
                                                <td class="text-center">
                                                    {{ \Carbon\Carbon::parse($jhs['tanggal'])->format('Y-m-d') }}</td>
                                                <td>{{ $jh['kode_akun'] }}</td>
                                                <td>{{ $jh['nama_akun'] }}</td>
                                                <td>{{ $jh['keterangan'] }}</td>
                                                <td>{{ $jh['ref'] ?? '' }}</td>
                                                <td>Rp. 0</td>
                                                <td>Rp. {{ number_format($jh['nominal'], 0, ',', '.') }}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Rp. {{ number_format($total_debit, 0, ',', '.') }} </th>
                                        <th>Rp. {{ number_format($total_kredit, 0, ',', '.') }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script>
        $(document).ready(function() {
            $('input[name=bulan]').change(function () {
                $('input[name=bulan2]').val($(this).val());
                $('input[name=bulan3]').val($(this).val());
            });
            $("#filter_akuns").select2();
            $("#filter_barangs").select2();
            $("#transaction_type").change(function() {
                $("#filter_akuns option").attr('disabled', true);
                $("#filter_barangs option").attr('disabled', true);
                if ($(this).val() == "all") {
                    $(".perusahaan_1").attr('disabled', false);
                    $(".perusahaan_2").attr('disabled', false);
                } else {
                    $(".perusahaan_"+$(this).val()).attr('disabled', false);
                }
            });
            $("#transaction_type").trigger('change');
        } );

        $('').DataTable({
            dom: 'lBfrtip',
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            buttons: [
                'colvis',
                { 
                    extend: 'print',
                    footer: true 
                },
                { 
                    extend: 'copyHtml5',
                    footer: true 
                },
                { 
                    extend: 'excelHtml5',
                    footer: true 
                },
                { 
                    extend: 'pdfHtml5',
                    footer: true 
                }
            ],
        });
        
    </script>
@endsection
