<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jurnal Harian {{now()}}</title>
    <style>
        .title {
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
</head>

<body>
    @php $sep_thsnd = $export_type == "export" ? "" : "."; @endphp
    @if ($export_type == "pdf")
    @php
        $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
        $month = date('m', strtotime($start_date));
        $year  = date('Y', strtotime($start_date));
    @endphp
    <h3 class="title"><center>CV. Anzac Food</center></h3>
    <h3 class="title"><center>Laporan Jurnal Harian</center></h3>
    <h4 class="title"><center>{{ date('d', strtotime($start_date)) }} s/d {{ date('d', strtotime($end_date)) }} {{$bulan}} {{ $year }}</center></h4>
    @endif
    @php
        $total_debit = 0;
        $total_kredit = 0;
        foreach ($jurnal_harians as $jhs) {
            foreach ($jhs['debit'] as $jh) {
                $total_debit += $jh['nominal'];
            }
            foreach ($jhs['kredit'] as $jh) {
                $total_kredit += $jh['nominal'];
            }
        }
    @endphp
    <table class="table table-bordered" border="1" align="center" style="width: 100%; border-collapse: collapse;">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th @if($sep_thsnd) colspan="2" @endif>Saldo Awal</th>
                @if($sep_thsnd) <th>Rp.</th> @endif
                <th data-format="#,##0">{{ number_format($saldo_awal_first,0,',',$sep_thsnd)}}</th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                @if($sep_thsnd) <th>Rp.</th> @endif
                <th data-format="#,##0">{{ number_format($total_debit,0,',',$sep_thsnd)}}</th>
                @if($sep_thsnd)<th> Rp. </th>@endif 
                <th data-format="#,##0">{{ number_format($total_kredit,0,',',$sep_thsnd)}}</th>
            </tr>
            <tr>
                <th style="text-align:center; font-weight: bold;">Tanggal</th>
                <th style="text-align:center; font-weight: bold;">Kode Akun</th>
                <th style="text-align:center; font-weight: bold;">Nama Akun</th>
                <th style="text-align:center; font-weight: bold;">Keterangan</th>
                <th style="text-align:center; font-weight: bold;">Ref.</th>
                <th style="text-align:center; font-weight: bold;" @if($sep_thsnd) colspan="2" @endif>Debit</th>
                <th style="text-align:center; font-weight: bold;" @if($sep_thsnd) colspan="2" @endif>Kredit</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($jurnal_harians as $jhs)
                @foreach ($jhs['debit'] as $jh)
                <tr>
                    <td class="text-center">{{\Carbon\Carbon::parse($jhs['tanggal'])->format('Y-m-d')}}</td>
                    <td style="text-align: left;">{{$jh['kode_akun']}}</td>
                    <td>{{$jh['nama_akun']}}</td>
                    <td>{{$jh['keterangan']}}</td>
                    <td>{{$jh['ref']??''}}</td>
                    @if($sep_thsnd) <td>Rp.</td> @endif
                    <td data-format="#,##0">{{ number_format($jh['nominal'],0,',',$sep_thsnd)}}</td>
                    @if($sep_thsnd) <td>Rp.</td> @endif
                    <td>0</td>
                </tr>
                @endforeach
                @foreach ($jhs['kredit'] as $jh)
                <tr>
                    <td class="text-center">{{\Carbon\Carbon::parse($jhs['tanggal'])->format('Y-m-d')}}</td>
                    <td style="text-align: left;">{{$jh['kode_akun']}}</td>
                    <td>{{$jh['nama_akun']}}</td>
                    <td>{{$jh['keterangan']}}</td>
                    <td>{{$jh['ref']??''}}</td>
                    @if($sep_thsnd) <td>Rp.</td> @endif
                    <td>0</td>
                    @if($sep_thsnd) <td>Rp.</td> @endif
                    <td data-format="#,##0">{{number_format($jh['nominal'],0,',',$sep_thsnd)}}</td>
                </tr>
                @endforeach
            @endforeach
            <tr>
                <td class="text-center">Total</td>
                <td style="text-align: left;"></td>
                <td></td>
                <td></td>
                <td></td>
                @if($sep_thsnd) <td>Rp.</td> @endif
                <td data-format="#,##0">{{number_format($total_debit,0,',',$sep_thsnd)}}</td>
                @if($sep_thsnd) <td>Rp.</td> @endif
                <td data-format="#,##0">{{number_format($total_kredit,0,',',$sep_thsnd)}}</td>
            </tr>
        </tbody>
    </table>

</body>

</html>