@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @php
                $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
                $month = date('m', strtotime($start_date));
                $year = date('Y', strtotime($start_date));
            @endphp
            <div class="col-md-12 mb-3">
                <div class="card card-noborder b-radius">
                    <div class="card-body">
                        <form id="formFilter" action="{{ url('/accounting/buku_besar/filter') }}" method="get">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Filter Bulan</label>
                                        <input type="month" name="bulan" id="bulan"
                                            class="form-control form-control-lg"
                                            value="{{ !request()->has('bulan') ? date('Y-m') : request()->input('bulan') }}">
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Filter Transaksi</label>
                                        <select class="form-control" id="transaction_type" name="transaction_type">
                                            <option value="1" @if (request()->input('transaction_type') == '1') selected @endif>PPN
                                            </option>
                                            <option value="2" @if (request()->input('transaction_type') == '2') selected @endif>Non PPN
                                            </option>
                                            <option value="all" @if (request()->input('transaction_type') == 'all') selected @endif>All
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Pencarian</label>
                                        <input type="text" class="form-control" name="search"
                                            value="{{ request()->input('search') }}"
                                            placeholder="Tanggal, Kode, Nama akun, Keterangan, Ref, Saldo">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Filter Akun</label>
                                        @php $arr_akuns = request()->input('akuns') ?? []; @endphp
                                        <select class="form-control" id="filter_akuns" name="akuns[]" multiple
                                            style="width: 100%;">
                                            @foreach ($akuns as $akun)
                                                <option value="{{ $akun->kode_akun }}"
                                                    @if (in_array($akun->kode_akun, $arr_akuns)) selected @endif
                                                    class="perusahaan_{{ $akun->perusahaan }}">{{ $akun->kode_akun }} -
                                                    {{ $akun->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <div class="d-flex">
                            <button type="submit" form="formFilter" class="btn btn-primary">Filter</button>
                            <div class="d-flex ml-auto">
                                <a href="{{ url('/accounting/buku_besar/pdf?type=print&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-secondary ml-2">
                                    <i class="mdi mdi-eye"></i>
                                </a>
                                <a href="{{ url('/accounting/buku_besar/pdf?type=pdf&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-danger ml-2">
                                    <i class="mdi mdi-file-pdf"></i> PDF
                                </a>
                                <a href="{{ url('/accounting/buku_besar/export?type=export&') . \Illuminate\Support\Arr::query(Request::all()) }}"
                                    target="_blank" class="btn btn-success ml-2">
                                    <i class="mdi mdi-file-excel"></i> Excel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-noborder b-radius">
                    <div class="card-header">
                        <h4>
                            Buku Besar Tahun {{ $year }}
                        </h4>
                        <p>Periode : {{ date('d', strtotime($start_date)) }}-{{ date('d', strtotime($end_date)) }}
                            {{ $bulan }} {{ $year }}</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="text-align:center">Kode Akun</th>
                                        <th style="text-align:center">Nama Akun</th>
                                        <th style="text-align:center">Tanggal</th>
                                        <th style="text-align:center">Ref</th>
                                        <th style="text-align:center">Saldo Awal</th>
                                        <th style="text-align:center">Debit</th>
                                        <th style="text-align:center">Kredit</th>
                                        <th style="text-align:center">Saldo Akhir</th>
                                        <th style="text-align:center">Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($buku_besars as $buku_besar)
                                        <tr>
                                            <td class="text-center">{{ $buku_besar['kode_akun'] }}</td>
                                            <td class="text-center">{{ $buku_besar['nama'] }}</td>
                                            <td class="text-center">{{ date('d-m-Y', strtotime($start_date)) }}</td>
                                            <td class="text-center">Saldo Awal</td>
                                            <td class="text-center">
                                                {{ number_format($buku_besar['saldo_awal'], 2, ',', '.') }}</td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center">
                                                {{ number_format($buku_besar['saldo_awal'], 2, ',', '.') }}</td>
                                            <td class="text-center"></td>
                                        </tr>
                                        @php
                                            $tdebit = 0;
                                            $tkredit = 0;
                                            $takhir = 0;
                                        @endphp
                                        @foreach ($buku_besar['jurnals'] as $key => $jurnal)
                                            @php
                                                $tdebit += $jurnal->debit;
                                                $tkredit += $jurnal->kredit;
                                            @endphp
                                            <tr>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center">{{ date('d-m-Y', strtotime($jurnal->tanggal)) }}
                                                </td>
                                                <td class="text-center">{{ $jurnal->bukti_transaksi }}</td>
                                                <td class="text-center"></td>
                                                <td class="text-center">{{ number_format($jurnal->debit, 2, ',', '.') }}
                                                </td>
                                                <td class="text-center">{{ number_format($jurnal->kredit, 2, ',', '.') }}
                                                </td>
                                                <td class="text-center">
                                                    {{ number_format($jurnal->saldo_akhir, 2, ',', '.') }}</td>
                                                <td class="text-center">{{ $jurnal->keterangan }}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center">Total</td>
                                            <td class="text-center">0</td>
                                            <td class="text-center">{{ number_format($tdebit, 2, ',', '.') }}</td>
                                            <td class="text-center">{{ number_format($tkredit, 2, ',', '.') }}</td>
                                            <td class="text-center">
                                                {{ number_format($buku_besar['jurnals']->last() == null ? 0 : $buku_besar['jurnals']->last()->saldo_akhir, 2, ',', '.') }}
                                            </td>
                                            <td class="text-center"></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

    <script>
        $(document).ready(function() {
            $('input[name=bulan]').change(function() {
                $('input[name=bulan2]').val($(this).val());
                $('input[name=bulan3]').val($(this).val());
            });
            $("#filter_akuns").select2();
            $("#filter_barangs").select2();
            $("#transaction_type").change(function() {
                $("#filter_akuns option").attr('disabled', true);
                $("#filter_barangs option").attr('disabled', true);
                if ($(this).val() == "all") {
                    $(".perusahaan_1").attr('disabled', false);
                    $(".perusahaan_2").attr('disabled', false);
                } else {
                    $(".perusahaan_" + $(this).val()).attr('disabled', false);
                }
            });
            $("#transaction_type").trigger('change');
        });

        $('').DataTable({
            dom: 'lBfrtip',
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            buttons: [
                'colvis',
                {
                    extend: 'print',
                    footer: true
                },
                {
                    extend: 'copyHtml5',
                    footer: true
                },
                {
                    extend: 'excelHtml5',
                    footer: true
                },
                {
                    extend: 'pdfHtml5',
                    footer: true
                }
            ],
        });
    </script>
@endsection
