@extends('layouts.master')
@section('content')
    <div class="container">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Master Pegawai</h1>
        </div>
        @if ($errors->any())
        @foreach ($errors->all() as $err)
            <div class="alert alert-danger">{{ $err }}</div>
        @endforeach
    @endif
        <form action="{{ url('doAddpegawai') }}" method="post">
            @csrf
            <div class="form-group">

                <label class="label" for="readonlyTextInput">ID Pegawai</label>
                <input name="id" id="readonlyTextInput" class="form-control" placeholder="ID Pegawai" readonly>

                <label class="label">Nama Pegawai</label>
                <input name="nama" class="form-control" placeholder="Masukkan Nama" required>

                <label class="label">NPWP</label>
                <input name="npwp" class="form-control" placeholder="Masukkan NPWP">

                <label class="label">Alamat</label>
                <input name="alamat" class="form-control" placeholder="Masukkan Alamat"  required>

                <label class="label">Provinsi</label>
                <input name="provinsi" class="form-control" placeholder="Masukkan Provinsi"  required>

                <label class="label">Kota</label>
                <input name="kota" class="form-control" placeholder="Masukkan Kota"  required>

                <label class="label">Kecamatan</label>
                <input name="kecamatan" class="form-control" placeholder="Masukkan Kecamatan"  required>

                <label class="label">Kelurahan</label>
                <input name="kelurahan" class="form-control" placeholder="Masukkan Kelurahan"  required>

                <label class="label">Kode Pos</label>
                <input name="kodepos" class="form-control" placeholder="Kode Pos"  required>

                <label class="label">No. Telp</label>
                <input name="notelp" type="number" class="form-control" placeholder="Masukkan No. Telp"  required>

                <label class="label">Fax</label>
                <input name="fax" type="number" class="form-control" placeholder="Masukkan No. Fax"  required>

                <label class="label">Email</label>
                <input name="email" type="email" class="form-control" placeholder="Masukkan Email"  required>

                <label class="label">Password</label>
                <input name="password" type="password" class="form-control" placeholder="Masukkan Password"  required>

                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
