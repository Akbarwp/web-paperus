@extends('layouts.master')
@section('content')
    <div class="row page-title-header">
        <div class="col-12">
            <div class="page-header d-flex justify-content-between align-items-center">
                <h1 class="h3 mb-0 text-gray-800 page-title">Type Akun</h1>
                <div class="d-flex justify-content-start">
                    <button class="btn btn-primary ml-2" id="button-add">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>

        </div>
    </div>

    <div class="row" id="form-add">
        <div class="col-12 grid-margin">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    <form action="{{ url('/accounting/akun/add') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="perusahaan">Perusahaan</label>
                            <select name="perusahaan" class="form-control form-control-lg" id="perusahaan" required>
                                <option value=""> -- Pilih Perusahaan -- </option>
                                @foreach (\App\Models\Akun::$perusahaans as $key => $perusahaan)
                                    <option value="{{ $key }}">{{ $perusahaan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tipe_akun">Type Akun</label>
                            <select name="tipe_akun" class="form-control form-control-lg" id="tipe_akun" required>
                                <option value=""> -- Pilih Type akun -- </option>
                                <option value="Aktiva Lancar">Aktiva Lancar</option>
                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                <option value="Kewajiban">Kewajiban</option>
                                <option value="Modal">Modal</option>
                                <option value="Pendapatan">Pendapatan</option>
                                <option value="HPP">HPP</option>
                                <option value="Beban">Beban</option>
                                <option value="Laba/Rugi">Laba/Rugi</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="kode_akun">Kode</label>
                            <input type="text" name="kode_akun" class="form-control form-control-lg" id="kode_akun"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama akun</label>
                            <input type="text" name="nama" class="form-control form-control-lg" id="nama"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="saldo_awal">Saldo Awal</label>
                            <input type="number" class="form-control form-control-lg" name="saldo_awal" id="saldo_awal"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="tanggal">Tanggal</label>
                            <input type="datetime-local" name="tanggal" class="form-control form-control-lg" id="tanggal"
                                required>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="button" class="btn btn-secondary mr-3" id="button-cancel">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- modal duplikat data --}}
    <div class="modal fade" id="modal-duplikat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                @php
                    $bulan = date('Y-m');
                    $akun = \App\Models\Akun::where('tanggal', 'like', $bulan . '%')->get();
                    $bulan_minus = date('Y-m', strtotime('-1 month', strtotime($bulan)));
                @endphp
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Duplicate Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @if ($akun->count() == 0)
                    <form action="{{ url('/accounting/akun/duplicate') }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <p>Data pada bulan sebelumnya ({{ Carbon\Carbon::parse($bulan_minus)->format('M-Y') }}) akan di
                                duplicate, apakah anda ingin
                                menduplicate
                                data?
                            </p>
                            <p>*Catatan saldo awal jadi 0</p>
                            <p>Meliputi:</p>
                            @foreach ($akun as $i => $item)
                                @if (\Carbon\Carbon::parse($item->tanggal)->format('Y-m') == $bulan_minus)
                                    {{-- <p>- {{ $item->nama }}</p> --}}
                                    <input type="text" class="form-control mb-1" value="- {{ $item->nama }}">

                                    <input type="hidden" name="id[]" value="{{ $item->id }}">
                                    <input type="hidden" name="kode_akun[]" value="{{ $item->kode_akun }}">
                                    <input type="hidden" name="tanggal[]" value="{{ $item->tanggal }}">
                                    <input type="hidden" name="nama[]" value="{{ $item->nama }}">
                                @endif
                            @endforeach
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="button-update">Duplicate</button>
                        </div>
                    </form>
                @else
                    <div class="modal-body">
                        <p>Data Bulan {{ Carbon\Carbon::parse($bulan_minus)->format('M Y') }} Sudah Ada !!</p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    <div class="mb-3">
                        <a href="#" class="btn btn-success" data-target="#modal-duplikat" data-toggle="modal">
                            DuplicateData
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table-akun">
                            <thead>
                                <tr>
                                    <th>Bulan</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Type</th>
                                    <th>Saldo Awal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($akun as $item)
                                    <tr>
                                        <td>{{ Carbon\Carbon::parse($item->tanggal)->format('Y/m') }}</td>
                                        <td>{{ $item->kode_akun }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->tipe_akun }}</td>
                                        <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                        <td>
                                            <a href="#" data-toggle="modal"
                                                data-target="#edit-akun{{ $item->id }}"
                                                class="btn btn-primary btn-sm">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="#" class="btn btn-danger btn-sm btn-delete"
                                                data-id="{{ $item->id }}" data-nama="{{ $item->nama }}">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal edit data table bank --}}
    @foreach ($akun as $item)
        <div class="modal fade" id="edit-akun{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit {{ $item->nama }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('/accounting/akun/update') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            <div class="form-group">
                                <label for="perusahaan">Perusahaan</label>
                                <select name="perusahaan" class="form-control form-control-lg" id="perusahaan" required>
                                    <option value=""> -- Pilih Perusahaan -- </option>
                                    @foreach (\App\Models\Akun::$perusahaans as $key => $perusahaan)
                                        <option value="{{ $key }}"
                                            @if ($item->perusahaan == $key) selected @endif>{{ $perusahaan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tipe_akun">Type Akun</label>
                                <select name="tipe_akun" class="form-control form-control-lg" id="tipe_akun" required>
                                    <option value=""> -- Pilih Type akun -- </option>
                                    <option value="Aktiva Lancar"
                                        {{ $item->tipe_akun == 'Aktiva Lancar' ? 'selected' : '' }}>Aktiva Lancar</option>
                                    <option value="Aktiva Tetap"
                                        {{ $item->tipe_akun == 'Aktiva Tetap' ? 'selected' : '' }}>Aktiva Tetap</option>
                                    <option value="Kewajiban" {{ $item->tipe_akun == 'Kewajiban' ? 'selected' : '' }}>
                                        Kewajiban
                                    </option>
                                    <option value="Modal" {{ $item->tipe_akun == 'Modal' ? 'selected' : '' }}>Modal
                                    </option>
                                    <option value="Pendapatan" {{ $item->tipe_akun == 'Pendapatan' ? 'selected' : '' }}>
                                        Pendapatan</option>
                                    <option value="Hpp" {{ $item->tipe_akun == 'Hpp' ? 'selected' : '' }}>HPP</option>
                                    <option value="Beban" {{ $item->tipe_akun == 'Beban' ? 'selected' : '' }}>Beban
                                    </option>
                                    <option value="Laba/Rugi" {{ $item->tipe_akun == 'Laba/Rugi' ? 'selected' : '' }}>
                                        Laba/Rugi
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kode_akun">Kode</label>
                                <input type="text" name="kode_akun" class="form-control form-control-lg"
                                    id="kode_akun" required value="{{ $item->kode_akun }}">
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama akun</label>
                                <input type="text" name="nama" value="{{ $item->nama }}"
                                    class="form-control form-control-lg" id="nama" required>
                            </div>
                            <div class="form-group">
                                <label for="saldo_awal">Saldo Awal</label>
                                <input type="number" value="{{ $item->saldo_awal }}"
                                    class="form-control form-control-lg" name="saldo_awal" id="saldo_awal" required>
                            </div>
                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input type="date" name="tanggal"
                                    value="{{ Carbon\Carbon::parse($item->tanggal)->format('Y-m-d') }}"
                                    class="form-control form-control-lg" id="tanggal">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

    <script type="text/javascript">
        // div form add hidden by default
        $('#form-add').hide();
    
        // show div form add when button add clicked
        $('#button-add').click(function() {
            $('#form-add').show();
            $('#button-add').hide();
        });
    
        // hide div form add when button cancel clicked
        $('#button-cancel').click(function() {
            $('#form-add').hide();
            $('#button-add').show();
        });
    
        //datatables bank_in
        $(document).ready(function () {
            $('#table-akun').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'colvis',
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            { 
                                extend: 'print',
                                footer: true 
                            },
                            { 
                                extend: 'copyHtml5',
                                exportOptions: {
                                    columns:[6,'visible'],
                                },
                                footer: true 
                            },
                            { 
                                extend: 'excelHtml5',
                                exportOptions: {
                                    columns: [6, ':visible' ]
                                },
                                footer: true 
                            },
                            { 
                                extend: 'pdfHtml5',
                                exportOptions: {
                                    columns: [6, ':visible' ]
                                }, 
                                footer: true 
                            },
                        ]
                    },
                ],
                
                // "language": {
                //     "lengthMenu": "Tampilkan _MENU_ data per halaman",
                //     "zeroRecords": "Tidak ada data",
                //     "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                //     "infoEmpty": "Tidak ada data yang ditampilkan",
                //     "infoFiltered": "(difilter dari _MAX_ total data)",
                //     "search": "Cari:",
                //     "paginate": {
                //         "first": "Awal",
                //         "last": "Akhir",
                //         "next": "Selanjutnya",
                //         "previous": "Sebelumnya"
                //     },
                // },
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });
        });
    
        @if ($message = Session::get('error'))
        swal(
            "",
            "{{ $message }}",
            "error"
        );
      @endif
    
        @if ($message = Session::get('create_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
      @endif
    
      @if ($message = Session::get('update_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
      @endif
    
      @if ($message = Session::get('delete_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
      @endif  
    
      @if ($message = Session::get('import_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
      @endif
    
      @if ($message = Session::get('update_failed'))
        swal(
            "",
            "{{ $message }}",
            "error"
        );
      @endif
    
      @if ($message = Session::get('supply_system_status'))
        swal(
            "",
            "{{ $message }}",
            "success"
        );
      @endif
    
      $(document).on('click', '.filter-btn', function(e){
        e.preventDefault();
        var data_filter = $(this).attr('data-filter');
        $.ajax({
          method: "GET",
          url: "{{ url('/customer/filter') }}/" + data_filter,
          success:function(data)
          {
            $('tbody').html(data);
          }
        });
      });
    
      $(document).on('click', '.btn-edit', function(){
        var data_edit = $(this).attr('data-edit');
        $.ajax({
          method: "GET",
          url: "{{ url('/customer/edit') }}/" + data_edit,
          success:function(response)
          {
            $('input[name=id]').val(response.customer.id);
            $('input[name=nama]').val(response.customer.nama);
            $('input[name=alamat]').val(response.customer.alamat);
            $('input[name=nama_bank]').val(response.customer.nama_bank);
            $('input[name=no_rekening]').val(response.customer.no_rekening);
            $('input[name=phone]').val(response.customer.phone);
            $('input[name=kota]').val(response.customer.kota);
            validator.resetForm();
          }
        });
      });
    
      $(document).on('click', '.btn-delete', function(e){
        e.preventDefault();
        var data_id = $(this).attr('data-id');
        var nama = $(this).attr('data-nama');
        swal({
          title: "Apa Anda Yakin?",
          text: "Data Akun "+nama+" akan terhapus, klik oke untuk melanjutkan",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            window.open("{{ url('/accounting/akun/delete') }}/" + data_id, "_self");
          }
        });
      });
    
      var validator = $("form[name='update_form']").validate({
            rules: {
                nama: "required",
                kota: "required"
            },
            messages: {
                nama: "Nama customer tidak boleh kosong",
                kota: "Kota customer tidak boleh kosong",
            },
            errorPlacement: function(error, element) {
                var name = element.attr("name");
                $("#" + name + "_error").html(error);
            },
            submitHandler: function(form) {
                form.submit();
            }
      });
    
      //format rupiah
        var rupiah = document.getElementById('rupiah');
        rupiah.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah.value = formatRupiah(this.value, 'Rp. ');
        });
        //fungsi format rupiah
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    </script>
@endsection
