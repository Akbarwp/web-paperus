@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Surat Perintah Kerja</h1>
        </div>
        
        <div class="container">
            <div class="form-group">
                <form action="{{ url('/doAddSPK') }}" method="post">
                    @csrf
                    <label class="label">No. SPK</label>
                    <input class="form-control" placeholder="No. SPK" name="no_spk" value="{{ $kode }}" readonly>

                    <label class="label">Pilih Filter</label>
                    <div class="form-control">

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="radioPenawaran" name="inlineRadioOptions1" value="option3" onchange="myFunction()">
                            <label class="form-check-label" for="radioPenawaran">Penawaran</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="radioBarangJadi" name="inlineRadioOptions1" value="option3" onchange="myFunction()">
                            <label class="form-check-label" for="radioBarangJadi">Stok Barang Jadi</label>
                        </div>
                    </div>

                    <div id="penawaran" style="display: none">
                        <label class="label" for="readonlyTextInput">No. Penawaran</label>
                        <input type="text" name="cekPenawaran" id="cekPenawaran" hidden>
                        {{-- livesearch input --}}
                        <select data-live-search="true" class="selectpicker form-control" id="id_penjualan1" name="id_penjualan1" onchange="nama_brand_change();">
                            <option selected>Pilih No. Penawaran</option>
                            @foreach ($penawaran as $prm)
                                <option value={{ $prm->id_penawaran }}>{{ $prm->id_penawaran }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="barangJadi" style="display: none">
                        <label class="label" for="readonlyTextInput">Kode Pembuatan</label>
                        <input type="text" name="cekPenjualan" id="cekPenjualan" hidden>
                        {{-- livesearch input --}}
                        <select data-live-search="true" class="selectpicker form-control" id="id_penjualan2" name="id_penjualan2">
                            <option selected>Pilih Kode Pembuatan</option>
                            @foreach ($penjualanBarangJadi as $prm)
                                <option value={{ $prm->kode_pembuatan }}>{{ $prm->kode_pembuatan }}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="exampleFormControlTextarea1" class="label">Nama Customer</label>
                    <input class="form-control" name="pic" id="pic" placeholder="Nama Customer">

                    <label for="exampleFormControlTextarea1" class="label">Jenis Box</label>
                    <input class="form-control" name="jenis_box" id="jenis_box" placeholder="Jenis Box">

                    <label for="exampleFormControlTextarea1" class="label">Total Produksi</label>
                    <input type="number" class="form-control" name="jum_produksi" id="jum_produksi" placeholder="Total Produksi">

                    <label for="exampleFormControlTextarea1" class="label">Link Desain</label>
                    <input class="form-control" name="link_desain" placeholder="Link Desain">

                    <label class="label" for="readonlyTextInput">Jenis Pisau</label>
                    {{-- livesearch input --}}
                    <select data-live-search="true" class="selectpicker form-control" id="pisau" name="pisau">
                        <option selected>Pilih Jenis Pisau</option>
                        @foreach ($pisau as $prm)
                            <option value={{ $prm->namabarang }}>{{ $prm->namabarang }}</option>
                        @endforeach
                    </select>

                    <label class="label" for="readonlyTextInput">Jenis Plat</label>
                    {{-- livesearch input --}}
                    <select data-live-search="true" class="selectpicker form-control" id="plat" name="plat">
                        <option selected>Pilih Jenis Plat</option>
                        @foreach ($plat as $prm)
                            <option value={{ $prm->namabarang }}>{{ $prm->namabarang }}</option>
                        @endforeach
                    </select>

                    {{-- <label for="exampleFormControlTextarea1" class="label">Jenis Pisau</label>
                    <input class="form-control" name="pisau" placeholder="Jenis Pisau">

                    <label for="exampleFormControlTextarea1" class="label">Jenis Plat</label>
                    <input class="form-control" name="plat" placeholder="Jenis Plat"> --}}

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <input type="hidden" name="temp">
                </form>
            </div>
        </div>
    </div>
    <br>

    {{-- Processing1 --}}
    <div class="card">
        <div class="card-body">
            <label class="label" style="font-weight: 700; font-size: 20px">Proses1</label>
            <br>
            <label class="label">Pilih Filter</label>
            <div class="form-control">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="radioPenawaranProses1" name="inlineRadioOptions2" value="option4" onchange="myFunctionProses1()">
                    <label class="form-check-label" for="radioPenawaranProses1">Penawaran</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="radioBarangJadiProses1" name="inlineRadioOptions2" value="option4" onchange="myFunctionProses1()">
                    <label class="form-check-label" for="radioBarangJadiProses1">Stok Barang Jadi</label>
                </div>
            </div>

            <div id="penawaranProses1" style="display: none">
                <label class="label" for="readonlyTextInput">No. SPK</label>
                {{-- livesearch input --}}
                <select data-live-search="true" class="selectpicker form-control" id="search_spk_penawaran" name="search_spk_penawaran">
                    <option selected>Pilih No. SPK Penawaran</option>
                    @foreach ($spk_penawaran as $prm)
                        <option value={{ $prm->no_spk }}>{{ $prm->no_spk }}</option>
                    @endforeach
                </select>
            </div>
            <div id="barangJadiProses1" style="display: none">
                <label class="label" for="readonlyTextInput">No. SPK</label>
                {{-- livesearch input --}}
                <select data-live-search="true" class="selectpicker form-control" id="search_spk_barang_jadi" name="search_spk_barang_jadi">
                    <option selected>Pilih No. SPK Barang Jadi</option>
                    @foreach ($spk_barang_jadi as $prm)
                        <option value={{ $prm->no_spk }}>{{ $prm->no_spk }}</option>
                    @endforeach
                </select>
            </div>

            {{-- <select data-live-search="true" class="selectpicker form-control" id="search_spk" name="search_spk" onchange="nama_brand_change_processing1();">
                <option selected>Pilih No. SPK</option>
                @foreach ($no_spk as $prm)
                    <option value={{ $prm->no_spk }}>{{ $prm->no_spk }}</option>
                @endforeach
            </select> --}}
            <table id="tabelProses" class="table table-bordered table-no-wrap table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Jenis Proses</th>
                        <th>Nama Vendor</th>
                        <th>Jumlah</th>
                        <th>Harga Satuan</th>
                        <th>Harga Total</th>
                        {{-- <th>Harga Satuan Sebelumnya</th>
                        <th>Harga Total Sebelumnya</th>
                        <th>Penerimaan</th> --}}
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody id="processing1">
                    
                </tbody>
            </table>
        </div>
    </div>
    <br>

    {{-- Processing2 --}}
    <div class="card">
        <div class="card-body">
            <label class="label" style="font-weight: 700; font-size: 20px">Proses2</label>
            <br>

            <label class="label">Pilih Filter</label>
            <div class="form-control">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="radioPenawaranProses2" name="inlineRadioOptions2" value="option4" onchange="myFunctionProses2()">
                    <label class="form-check-label" for="radioPenawaranProses2">Penawaran</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="radioBarangJadiProses2" name="inlineRadioOptions2" value="option4" onchange="myFunctionProses2()">
                    <label class="form-check-label" for="radioBarangJadiProses2">Stok Barang Jadi</label>
                </div>
            </div>

            <div id="penawaranProses2" style="display: none">
                <label class="label" for="readonlyTextInput">No. SPK</label>
                {{-- livesearch input --}}
                <select data-live-search="true" class="selectpicker form-control" id="search_spk_penawaran2" name="search_spk_penawaran2">
                    <option selected>Pilih No. SPK Penawaran</option>
                    @foreach ($spk_penawaran as $prm)
                        <option value={{ $prm->no_spk }}>{{ $prm->no_spk }}</option>
                    @endforeach
                </select>
            </div>
            <div id="barangJadiProses2" style="display: none">
                <label class="label" for="readonlyTextInput">No. SPK</label>
                {{-- livesearch input --}}
                <select data-live-search="true" class="selectpicker form-control" id="search_spk_barang_jadi2" name="search_spk_barang_jadi2">
                    <option selected>Pilih No. SPK Barang Jadi</option>
                    @foreach ($spk_barang_jadi as $prm)
                        <option value={{ $prm->no_spk }}>{{ $prm->no_spk }}</option>
                    @endforeach
                </select>
            </div>

            <table id="tabelProses2" class="table table-bordered table-no-wrap table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Jenis Proses</th>
                        <th>Nama Vendor</th>
                        <th>Jumlah</th>
                        <th>Harga Satuan</th>
                        <th>Harga Total</th>
                        {{-- <th>Harga Satuan Sebelumnya</th>
                        <th>Harga Total Sebelumnya</th>
                        <th>Penerimaan</th> --}}
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody id="processing2">
                    
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function myFunction() {
            var checkBox = document.getElementById("radioPenawaran");

            if (checkBox.checked == true) {
                // --Penawaran--
                document.getElementById("penawaran").style.display = "block";
                document.getElementById("barangJadi").style.display = "none";
                $("#cekPenawaran").val(1);
                $("#cekPenjualan").val(0);

            } else {
                // --Stock Jadi--
                document.getElementById("penawaran").style.display = "none";
                document.getElementById("barangJadi").style.display = "block";
                $("#cekPenjualan").val(1);
                $("#cekPenawaran").val(0);
            }
        }

        // Filter Proses1
        function myFunctionProses1() {
            var checkBox = document.getElementById("radioPenawaranProses1");

            if (checkBox.checked == true) {
                // --Penawaran--
                document.getElementById("penawaranProses1").style.display = "block";
                document.getElementById("barangJadiProses1").style.display = "none";

            } else {
                // --Stock Jadi--
                document.getElementById("penawaranProses1").style.display = "none";
                document.getElementById("barangJadiProses1").style.display = "block";
            }
        }
        // Filter Proses2
        function myFunctionProses2() {
            var checkBox = document.getElementById("radioPenawaranProses2");

            if (checkBox.checked == true) {
                // --Penawaran--
                document.getElementById("penawaranProses2").style.display = "block";
                document.getElementById("barangJadiProses2").style.display = "none";

            } else {
                // --Stock Jadi--
                document.getElementById("penawaranProses2").style.display = "none";
                document.getElementById("barangJadiProses2").style.display = "block";
            }
        }

        // Auto Fill
        $(document).ready(function() {
            $('#id_penjualan1').on('change', function() {
                const penawaran = $(this).val();

                if(penawaran) {
                    $.ajax({
                        url: '/cekPenawaranSPK/'+penawaran,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#pic').empty();
                                $('#jenis_box').empty();
                                $('#jum_produksi').empty();

                                $('#pic').val(data.pic);
                                $('#jenis_box').val(data.jenis_box);
                                $('#jum_produksi').val(data.jum_produksi);
                            }else{
                                $('#pic').empty();
                                $('#jenis_box').empty();
                                $('#jum_produksi').empty();
                            }
                        }
                    });
                }else{
                    $('#pic').empty();
                    $('#jenis_box').empty();
                    $('#jum_produksi').empty();
                }
            });
        });
        $(document).ready(function() {
            $('#id_penjualan2').on('change', function() {
                const kodePembuatan = $(this).val();

                if(kodePembuatan) {
                    $.ajax({
                        url: '/cekKodePembuatanSPK/'+kodePembuatan,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#pic').empty();
                                $('#jenis_box').empty();

                                $('#pic').val(data.nama_customer);
                                $('#jenis_box').val(data.jenis_box);
                            }else{
                                $('#pic').empty();
                                $('#jenis_box').empty();
                            }
                        }
                    });
                }else{
                    $('#pic').empty();
                    $('#jenis_box').empty();
                }
            });
        });

        // Processing 1
        // Penawaran
        $(document).ready(function() {
            $('#search_spk_penawaran').on('change', function() {
                const penawaran = $(this).val();
                if(penawaran) {
                    $.ajax({
                        url: '/cekProcessing1/'+penawaran,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#tabelProses').empty();
                                $('#tabelProses').append(`
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Jenis Proses</th>
                                        <th>Nama Vendor</th>
                                        <th>Jumlah</th>
                                        <th>Harga Satuan</th>
                                        <th>Harga Total</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="processing1">
                                    
                                </tbody>
                                `);
                                
                                $nomor = 0;
                                
                                $.each(data, function(key, proses1){
                                    $nomor += 1;
                                    if (proses1.status == 1) {
                                        $status = "Diterima";
                                    } else {
                                        $status = "Ditolak";
                                    }
                                    
                                    $('#tabelProses').append(`
                                        <tr>
                                            <td>` + $nomor + `</td>
                                            <td>` + proses1.proses + `</td>
                                            <td>`+ proses1.nama_brand +`</td>
                                            <td>`+ proses1.jumlah +`</td>
                                            <td>`+ proses1.harga_satuan +`</td>
                                            <td>`+ proses1.harga_total +`</td>
                                            <td>` + $status + `</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ url('editProcessing1/`+ proses1.id_proses1 +`') }}"><i class="fas fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    `);
                                });
                            }
                        }
                    });
                }
            });
        });

        // Penjualan Barang Jadi
        $(document).ready(function() {
            $('#search_spk_barang_jadi').on('change', function() {
                const barangJadi = $(this).val();
                if(barangJadi) {
                    $.ajax({
                        url: '/cekProcessingBarangJadi1/'+barangJadi,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#tabelProses').empty();
                                $('#tabelProses').append(`
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Jenis Proses</th>
                                        <th>Jumlah</th>
                                        <th>Harga Satuan</th>
                                        <th>Harga Total</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="processing1">
                                    
                                </tbody>
                                `);
                                
                                $nomor = 0;
                                $.each(data, function(key, proses1){
                                    $nomor += 1;
                                    if (proses1.status == 1) {
                                        $status = "Diterima";
                                    } else {
                                        $status = "Ditolak";
                                    }

                                    $('#tabelProses').append(`
                                        <tr>
                                            <td>` + $nomor + `</td>
                                            <td>` + proses1.proses + `</td>
                                            <td>`+ proses1.jumlah +`</td>
                                            <td>`+ proses1.harga_satuan +`</td>
                                            <td>`+ proses1.harga_total +`</td>
                                            <td>` + $status + `</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ url('editBarangJadi1/`+ proses1.id_proses_barang_jadi +`') }}"><i class="fas fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    `);
                                });
                            }
                        }
                    });
                }
            });
        });


        // Processing 2
        // Penawaran
        $(document).ready(function() {
            $('#search_spk_penawaran2').on('change', function() {
                const penawaran = $(this).val();
                if(penawaran) {
                    $.ajax({
                        url: '/cekProcessing2/'+penawaran,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#tabelProses2').empty();
                                $('#tabelProses2').append(`
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Jenis Proses</th>
                                        <th>Nama Vendor</th>
                                        <th>Jumlah</th>
                                        <th>Harga Satuan</th>
                                        <th>Harga Total</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="processing1">
                                    
                                </tbody>
                                `);
                                
                                $nomor = 0;
                                
                                $.each(data, function(key, proses1){
                                    $nomor += 1;
                                    if (proses1.status == 1) {
                                        $status = "Diterima";
                                    } else {
                                        $status = "Ditolak";
                                    }
                                    
                                    $('#tabelProses2').append(`
                                        <tr>
                                            <td>` + $nomor + `</td>
                                            <td>` + proses1.proses + `</td>
                                            <td>`+ proses1.nama_brand +`</td>
                                            <td>`+ proses1.jumlah +`</td>
                                            <td>`+ proses1.harga_satuan +`</td>
                                            <td>`+ proses1.harga_total +`</td>
                                            <td>` + $status + `</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ url('editProcessing2/`+ proses1.id_proses2 +`') }}"><i class="fas fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    `);
                                });
                            }
                        }
                    });
                }
            });
        });

        // Penjualan Barang Jadi
        $(document).ready(function() {
            $('#search_spk_barang_jadi2').on('change', function() {
                const barangJadi = $(this).val();
                if(barangJadi) {
                    $.ajax({
                        url: '/cekProcessingBarangJadi2/'+barangJadi,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#tabelProses2').empty();
                                $('#tabelProses2').append(`
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Jenis Proses</th>
                                        <th>Jumlah</th>
                                        <th>Harga Satuan</th>
                                        <th>Harga Total</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="processing1">
                                    
                                </tbody>
                                `);
                                
                                $nomor = 0;
                                $.each(data, function(key, proses1){
                                    $nomor += 1;
                                    if (proses1.status == 1) {
                                        $status = "Diterima";
                                    } else {
                                        $status = "Ditolak";
                                    }

                                    $('#tabelProses2').append(`
                                        <tr>
                                            <td>` + $nomor + `</td>
                                            <td>` + proses1.proses + `</td>
                                            <td>`+ proses1.jumlah +`</td>
                                            <td>`+ proses1.harga_satuan +`</td>
                                            <td>`+ proses1.harga_total +`</td>
                                            <td>` + $status + `</td>
                                            <td>
                                                <a class="btn btn-warning" href="{{ url('editBarangJadi2/`+ proses1.id_proses_barang_jadi +`') }}"><i class="fas fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    `);
                                });
                            }
                        }
                    });
                }
            });
        });
    </script>

    {{-- <script>
        var jArray = <?php echo json_encode($no_spk); ?>;
        var no_spk = "";
        var temp = 0;
        if (jArray.length == 0) {
            no_spk = "SP0";
        } else {
            for (var i = 0; i < jArray.length; i++) {
                no_spk = jArray[i]['no_spk'];
            }


        }
        temp = parseInt(no_spk.substring(no_spk.length, 2)) + 1;
        no_spk = "SP" + temp.toString().padStart(3, '0');
        document.getElementsByName("no_spk")[0].value = no_spk.toString();

        function nama_brand_change() {
            $.ajax({
                url: "autocomplete.php",
                method: "POST",
                data: {
                    query: $("[name='id_penawaran']").val(),
                    ctr: "PenawaranSPK"
                },
                success: function(data) {
                    var temp = data.split(",");
                    $("[name='pic']").val(temp[0]);
                    $("[name='jenis_box']").val(temp[1]);
                    $("[name='jum_produksi']").val(temp[2]);
                    $("[name='link_desain']").val(temp[3]);
                    $("[name='pisau']").val(temp[4]);
                    $("[name='plat']").val(temp[5]);
                }
            });
        }

        function nama_brand_change_processing1() {
            $("[name='temp']").val($("[name='search_spk']").val());
            console.log($("[name='temp']").val());
            $.ajax({
                url: "autocomplete.php",
                method: "POST",
                data: {
                    query: $("[name='search_spk']").val(),
                    ctr: "Processing1SPK"
                },
                success: function(data) {
                    var temp = data.split(",");
                    for (let i = 0; i < temp.length - 1; i++) {
                        console.log(temp[i]);
                        $("[name='proses[" + i + "]']").val(temp[i]);
                    }
                    $("[name='temp']").val(temp.length-1);
                    $("#processing1").html("");
                    $('#processing1').append(temp);
                }
            });
        }

        function harga_total_change($id) {
            var qty = parseInt($("[name='jumlah[" + $id + "]']").val());
            var harga_satuan = parseInt($("[name='harga_satuan[" + $id + "]']").val());
            var temp = harga_satuan * qty;
            $("[name='harga_total[" + $id + "]']").val(temp);
        }

        function btnAcc($id) {
            $.ajax({
                url: "autocomplete.php",
                method: "POST",
                data: {
                    query: [$id,$("[name='proses[" + $id + "]']").val(),$("[name='nama_vendor[" + $id + "]']").val(),$("[name='jumlah[" + $id + "]']").val(),$("[name='harga_satuan[" + $id + "]']").val(),$("[name='harga_total[" + $id + "]']").val(),
                    $("[name='harga_satuan_sebelumnya[" + $id + "]']").val(),$("[name='harga_total_sebelumnya[" + $id + "]']").val(),$("[name='search_spk']").val()],
                    ctr: "AccSPKProcess1"
                },
                success: function(data) {

                }
            });
        }
    </script> --}}
@endsection
