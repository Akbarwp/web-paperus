<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jurnal Harian Single {{now()}}</title>
    <style>
        .title {
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
</head>

<body>
    @php $sep_thsnd = $export_type == "export" ? "" : "."; @endphp
    @if ($export_type == "pdf")
    @php
        $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
        $month = date('m', strtotime($start_date));
        $year  = date('Y', strtotime($start_date));
    @endphp
    <h3 class="title"><center>CV. Anzac Food</center></h3>
    <h3 class="title"><center>Laporan Jurnal Harian per Akun</center></h3>
    <h4 class="title"><center>{{ date('d', strtotime($start_date)) }} s/d {{ date('d', strtotime($end_date)) }} {{$bulan}} {{ $year }}</center></h4>
    @endif
    @foreach($buku_besars as $buku_besar)
        <table class="" style="width: 100%;">
            <tr>
                <th style="text-align: left;" colspan="6">
                    Laporan {{$buku_besar['nama']}} : {{ date('d-m-Y', strtotime($start_date))}} - {{ date('d-m-Y', strtotime($end_date))}}
                </th>
            </tr>
            <tr>
                <td>Kode</td>
                <td style="text-align: left;" colspan="4">{{$buku_besar['kode_akun']}}</td>
                <td></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:center">Saldo Awal</td>
                <td data-format="#,##0" style="text-align:right">{{ number_format($buku_besar['saldo_awal'], 0, ",", $sep_thsnd)}}</td>
            </tr>
        </table>
        <table class="table table-bordered" border="1" align="center" style="width: 100%; border-collapse: collapse; margin-bottom: 20px;">
            <thead>
                <tr>
                    <th style="text-align:center; font-weight: bold;">No Bukti</th>
                    <th style="text-align:center; font-weight: bold;">Tgl Bukti</th>
                    <th style="text-align:center; font-weight: bold;">Keterangan</th>
                    <th style="text-align:center; font-weight: bold;" @if($sep_thsnd) colspan="2" @endif>Masuk</th>
                    <th style="text-align:center; font-weight: bold;" @if($sep_thsnd) colspan="2" @endif>Keluar</th>
                    <th style="text-align:center; font-weight: bold;" @if($sep_thsnd) colspan="2" @endif>Saldo</th>
                </tr>
            </thead>
            <tbody>
                @php $tdebit = 0; $tkredit= 0; $tsaldo_akhir = 0; @endphp
                @foreach($buku_besar['jurnals'] as $jurnal)
                    @php $tdebit += $jurnal->debit; $tkredit += $jurnal->kredit; $tsaldo_akhir = $jurnal->saldo_akhir; @endphp
                    <tr>
                        <td>{{ $jurnal->bukti_transaksi}}</td>
                        <td>{{ date('d-m-Y', strtotime($jurnal->tanggal))}}</td>
                        <td>{{ $jurnal->keterangan}}</td>
                        @if($sep_thsnd) <td>Rp.</td> @endif
                        <td data-format="#,##0">{{ number_format($jurnal->debit, 0, ",", $sep_thsnd)}}</td>
                        @if($sep_thsnd) <td>Rp.</td> @endif
                        <td data-format="#,##0">{{ number_format($jurnal->kredit, 0, ",", $sep_thsnd)}}</td>
                        @if($sep_thsnd) <td>Rp.</td> @endif
                        <td data-format="#,##0">{{ number_format($jurnal->saldo_akhir, 0, ",", $sep_thsnd)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td>Total</td>
                    @if($sep_thsnd) <td>Rp.</td> @endif
                    <td data-format="#,##0">{{number_format($tdebit, 0, ",", $sep_thsnd)}}</td>
                    @if($sep_thsnd) <td>Rp.</td> @endif
                    <td data-format="#,##0">{{number_format($tkredit, 0, ",", $sep_thsnd)}}</td>
                    @if($sep_thsnd) <td>Rp.</td> @endif
                    <td data-format="#,##0">{{number_format($tsaldo_akhir, 0, ",", $sep_thsnd)}}</td>
                </tr>
            </tbody>
        </table>
    @endforeach

</body>

</html>