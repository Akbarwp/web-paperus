@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Penjualan Stok Barang Jadi</h1>
        </div>

        <div class="card">
            <div class="card-header">
                <a href={{ url('tambahpenjualanbarangjadi') }}>
                    <button type="button" class="btn btn-primary my-auto">
                        Tambah Data
                    </button>
                </a>

            </div>
            <div class="card-body">

                <table id="tabelFormPenawaran" class="table table-bordered table-no-wrap table-responsive" style="width:100%">
                    <thead>
                        <tr>
                            <th>Kode Pembuatan</th>
                            <th>Customer</th>
                            <th>Barang Jadi</th>
                            <th>Quantity</th>
                            <th>Harga Satuan</th>
                            <th>Total Harga</th>
                            <th>Diskon</th>
                            <th>Potongan</th>
                            <th>Nett</th>
                            <th>Penerimaan</th>
                            <th>Aksi</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($penjualan as $prm)
                            <tr>
                                <td>{{ $prm->kode_pembuatan }}</td>
                                <td>{{ $prm->nama_customer }}</td>
                                <td>{{ $prm->jenis_box }}</td>
                                <td>{{ $prm->qty }}</td>
                                <td>{{ $prm->harga_satuan }}</td>
                                <td>{{ $prm->total_harga }}</td>
                                <td>{{ $prm->diskon }}</td>
                                <td>{{ $prm->potongan }}</td>
                                <td>{{ $prm->nett }}</td>
                                <td>
                                    <form method="post" action="{{ url('/accPenjualan') }}" style="display: inline-block;">
                                        @csrf
                                        <input type="text" name="kode_pembuatan" hidden value="{{ $prm->kode_pembuatan }}">
                                        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i></button>
                                    </form>
                                    <form method="post" action="{{ url('/declinePenjualan') }}" style="display: inline-block;">
                                        @csrf
                                        <input type="text" name="kode_pembuatan" hidden value="{{ $prm->kode_pembuatan }}">
                                        <button type="submit" class="btn btn-danger"><i class="fas fa-times"></i></button>
                                    </form>
                                </td>
                                <td>
                                    <div style="display: flex">
                                        {{-- <a href="{{ url('masterpenjualan/edit/' . $prm->kode_pembuatan) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a> --}}
                                        <form method="post" action="{{ url('masterpenjualan/delete/' . $prm->kode_pembuatan) }}">
                                            @csrf
                                            <button onclick="return confirm('Apakah yakin ingin dihapus?')" type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </div>
                                </td>
                                @if ($prm->status_penjualan == '1')
                                    <td>Diterima</td>
                                @else
                                    <td>Ditolak</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <script>
        
    </script>
    <!-- /.container-fluid -->
@endsection
