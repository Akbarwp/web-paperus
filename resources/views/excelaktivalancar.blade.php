<table>
    <thead>
        <tr>
            <th><b>110</b></th>
            <th colspan="6"><b>Aktiva Lancar</b></th>
        </tr>
        <tr>
            <th><b>Kode</b></th>
            <th><b>Nama</b></th>
            <th><b>Kategori</b></th>
            <th><b>Kategori lain</b></th>
            <th><b>Biaya</b></th>
            <th><b>Keterangan</b></th>
            <th><b>Tanggal Input</b></th>
            <th><b>Tanggal Update</b></th>
        </tr>
    </thead>
    <tbody>
        @php
        $kode = 12;
        $total_biaya = 0;
        @endphp
        @foreach ($aktiva_lancar as $item)
        <tr>
            <td>{{ $item->kode_nama_akun }}.{{$item->kode}}</td>
            <td>{{ $item->nama_produk }}</td>
            <td>{{ $item->jenis_aktiva }}</td>
            <td>{{ $item->jenis_lain }}</td>
            <td>Rp. {{ number_format($item->biaya, 0,',','.') }}</td>
            <td>{{ $item->keterangan }}</td>
            <td>{{ $item->created_at->format('d/m/Y')}}</td>
            <td>{{ $item->updated_at->format('d/m/Y')}}</td>
        </tr>
        @php
        $total_biaya += $item->biaya;
        @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4"><b>Total</b></td>
            <td><b>Rp. {{ number_format($total_biaya, 0,',','.') }}</b></td>
            <td></td>
        </tr>
    </tfoot>
</table>