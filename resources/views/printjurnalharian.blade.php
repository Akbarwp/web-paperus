<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Jurnal Harian</title>

</head>
<style>
    /* font size */
    * {
        font-size: 11px;
        /* font-family: Verdana, Arial, sans-serif; */
    }

    .table-bordered thead tr th,
    .table-bordered tbody tr th,
    .table-bordered tfoot tr th,
    .table-bordered thead tr td,
    .table-bordered tbody tr td,
    .table-bordered tfoot tr td {
        border: 0.5px solid rgb(160, 160, 160);
    }

    .table-bordered thead tr th,
    .table-bordered tbody tr td,
    .table-bordered tfoot tr td {
        padding: 2px;
    }
</style>
<body>
    <button id="prr" onclick="this.style.display='none'; print(); this.style.display='block';"> > PRINT < </button>
    @php
        $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
        $month = date('m', strtotime($start_date));
        $year  = date('Y', strtotime($start_date));
    @endphp
    <h1><center>CV. Anzac Food</center></h1>
    <h1><center>Laporan Jurnal Harian</center></h1>
    <h2><center>{{ date('d', strtotime($start_date)) }} s/d {{ date('d', strtotime($end_date)) }} {{$bulan}} {{ $year }}</center></h2>
    @php
        $total_debit = 0;
        $total_kredit = 0;
        foreach ($jurnal_harians as $jhs) {
            foreach ($jhs['debit'] as $jh) {
                $total_debit += $jh['nominal'];
            }
            foreach ($jhs['kredit'] as $jh) {
                $total_kredit += $jh['nominal'];
            }
        }
    @endphp
    <table class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>Saldo Awal</th>
                <th>Rp. {{number_format($saldo_awal_first,0,',','.')}}</th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>Rp. {{number_format($total_debit,0,',','.')}}</th>
                <th>Rp. {{number_format($total_kredit,0,',','.')}}</th>
            </tr>
            <tr>
                <th style="text-align:center">Tanggal</th>
                <th style="text-align:center">Kode Akun</th>
                <th style="text-align:center">Nama Akun</th>
                <th style="text-align:center">Keterangan</th>
                <th style="text-align:center">Ref.</th>
                <th style="text-align:center">Debit</th>
                <th style="text-align:center">Kredit</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($jurnal_harians as $jhs)
                @foreach ($jhs['debit'] as $jh)
                <tr>
                    <td class="text-center">{{\Carbon\Carbon::parse($jhs['tanggal'])->format('Y-m-d')}}</td>
                    <td>{{$jh['kode_akun']}}</td>
                    <td>{{$jh['nama_akun']}}</td>
                    <td>{{$jh['keterangan']}}</td>
                    <td>{{$jh['ref']??''}}</td>
                    <td>Rp. {{number_format($jh['nominal'],0,',','.')}}</td>
                    <td>Rp. 0</td>
                </tr>
                @endforeach
                @foreach ($jhs['kredit'] as $jh)
                <tr>
                    <td class="text-center">{{\Carbon\Carbon::parse($jhs['tanggal'])->format('Y-m-d')}}</td>
                    <td>{{$jh['kode_akun']}}</td>
                    <td>{{$jh['nama_akun']}}</td>
                    <td>{{$jh['keterangan']}}</td>
                    <td>{{$jh['ref']??''}}</td>
                    <td>Rp. 0</td>
                    <td>Rp. {{number_format($jh['nominal'],0,',','.')}}</td>
                </tr>
                @endforeach
            @endforeach
            <tr>
                <td class="text-center">Total</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Rp. {{number_format($total_debit,0,',','.')}}</td>
                <td>Rp. {{number_format($total_kredit,0,',','.')}}</td>
            </tr>
        </tbody>
    </table>
    <script>
        print(){
            window.onload = function() { 
                document.getElementById("prr").style.display='none';
                window.print(); 
            }
            window.onafterprint = window.close;
            document.getElementById("prr").style.display='block';
        };
    </script>
</body>

</html>