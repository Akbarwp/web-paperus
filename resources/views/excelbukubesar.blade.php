<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Buku Besar {{ now() }}</title>
</head>

<body>
    @php $sep_thsnd = $export_type == "export" ? "" : "."; @endphp
    @if ($export_type == 'pdf')
        @php
            $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
            $month = date('m', strtotime($start_date));
            $year = date('Y', strtotime($start_date));
        @endphp
        <h3 class="title">
            <center>CV. Anzac Food</center>
        </h3>
        <h3 class="title">
            <center>Laporan Buku Besar</center>
        </h3>
        <h4 class="title">
            <center>{{ date('d', strtotime($start_date)) }} s/d {{ date('d', strtotime($end_date)) }}
                {{ $bulan }} {{ $year }}</center>
        </h4>
    @endif
    @foreach ($buku_besars as $buku_besar)
        <div class="table-responsive">
            <h3>Nama akun: {{ $buku_besar['nama'] }}</h3>
            <h3>No akun: {{ $buku_besar['kode_akun'] }}</h3>
            <h3>Saldo Awal: {{ $buku_besar['saldo_awal'] }}</h3>
            <table class="table table-bordered" border="1" align="center"
                style="width: 100%;border-collapse: collapse;">
                <thead>
                    <tr>
                        <th style="text-align:center">Tanggal</th>
                        <th style="text-align:center">Keterangan</th>
                        <th style="text-align:center">Ref</th>
                        <th style="text-align:center">Debit</th>
                        <th style="text-align:center">Kredit</th>
                        <th style="text-align:center">Saldo Akhir</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($buku_besar['jurnals'] as $jurnal)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($jurnal->tanggal)->format('Y/m/d') }}</td>
                            <td><b>{{ $jurnal->keterangan }}</b></td>
                            <td><b>{{ $jurnal->bukti_transaksi }}</b></td>
                            <td data-format="#,##0" style="font-weight:bold;">
                                {{ number_format($jurnal->debit, 0, ',', $sep_thsnd) }}</td>
                            <td data-format="#,##0" style="font-weight:bold;">
                                {{ number_format($jurnal->kredit, 0, ',', $sep_thsnd) }}</td>
                            <td data-format="#,##0" style="font-weight:bold;">
                                {{ number_format($jurnal->saldo_akhir, 0, ',', $sep_thsnd) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <br>
    @endforeach
</body>

</html>
