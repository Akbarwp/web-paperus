@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Pengubahan Barang Jadi</h1>
        </div>

        <div class="form-group">
            <form method="post">
                @csrf
                <label class="label" for="readonlyTextInput">No. Produksi</label>
                <input id="readonlyTextInput" class="form-control" name="kode" placeholder="No. Produksi" value="{{ $barangJadi->id_produksi }}" readonly>
                
                {{-- <div id="penjualan" style="display: none"> --}}
                <div id="penjualan">
                    <label for="exampleFormControlTextarea1" class="label">Jenis Box</label>
                    <input class="form-control" placeholder="Jenis Box" name="jenis_box" value="{{ $barangJadi->jenis_box }}">

                    <label for="exampleFormControlTextarea1" class="label">Panjang</label>
                    <input class="form-control" placeholder="Masukkan panjang" name="panjang" value="{{ $barangJadi->panjang }}">

                    <label for="exampleFormControlTextarea1" class="label">Lebar</label>
                    <input class="form-control" placeholder="Masukkan lebar" name="lebar" value="{{ $barangJadi->lebar }}">

                    <label for="exampleFormControlTextarea1" class="label">Tinggi</label>
                    <input class="form-control" placeholder="Masukkan tinggi" name="tinggi" value="{{ $barangJadi->tinggi }}">

                    <label for="exampleFormControlTextarea1" class="label">Quantity</label>
                    <input type="number" class="form-control" placeholder="Masukkan Quantity" name="qty" value="{{ $barangJadi->qty }}">

                    <label for="exampleFormControlTextarea1" class="label">Jumlah Produksi</label>
                    <input type="number" class="form-control" placeholder="Masukkan Jumlah Produksi" name="jum_produksi" value="{{ $barangJadi->jum_produksi }}">

                    <br>
                    <div class="checkbox">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox1" value="Kertas" {{ in_array('Kertas', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox1">Kertas</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox2" value="Tinta" {{ in_array('Tinta', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox2">Tinta</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox3" value="Laminasi" {{ in_array('Laminasi', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox3">Laminasi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox4" value="Plong" {{ in_array('Plong', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox4">Plong</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox5" value="Hotprint" {{ in_array('Hotprint', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox5">Hotprint</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox6" value="Sortir" {{ in_array('Sortir', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox6">Sortir</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox7" value="Pembelian Dus" {{ in_array('Pembelian Dus', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox7">Pembelian Dus</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox8" value="Packing" {{ in_array('Packing', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox8">Packing</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox9" value="Emboss" {{ in_array('Emboss', $cb) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckbox9">Emboss</label>
                        </div>
                    </div>
                    <br>
                {{-- <input type="hidden" name="data"> --}}
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <script>

        
    </script>
@endsection
