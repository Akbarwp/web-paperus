<table>
    <thead>
        <tr>
            <th><b>1</b></th>
            <th colspan="7"><b>Aktiva Tetap</b></th>
        </tr>
        <tr>
            <th><b>Kode</b></th>
            <th><b>Kategori</b></th>
            <th><b>Kategori lain</b></th>
            <th><b>Nominal</b></th>
            <th><b>Durasi Aktiva</b></th>
            <th><b>Penyusutan</b></th>
            <th><b>Keterangan</b></th>
            <th><b>Tanggal Input</b></th>
            <th><b>Tanggal Update</b></th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 11;
        $total = 0;
        $total_pen = 0;
        @endphp
        @foreach ($aktiva_tetap as $item)
        <tr>
            <td>{{ $item->kode_nama_akun }}.{{$item->kode}}</td>
            <td>{{ $item->jenis_aktiva }}</td>
            <td>{{ $item->jenis_lain }}</td>
            <td>Rp. {{ number_format($item->nominal, 0,',','.') }}</td>
            <td>{{ $item->durasi_aktiva }} Bln</td>
            <td>Rp. {{ number_format($item->penyusutan, 0,',','.') }}</td>
            <td>{{ $item->keterangan }}</td>
            <td>{{ $item->created_at->format('d/m/Y')}}</td>
            <td>{{ $item->updated_at->format('d/m/Y')}}</td>
        </tr>
        @php
        $total += $item->nominal;
        $total_pen += $item->penyusutan;
        @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"><b>Total</b></td>
            <td><b>Rp. {{ number_format($total, 0,',','.') }}</b></td>
            <td></td>
            <td><b>Rp. {{ number_format($total_pen, 0,',','.') }}</b></td>
            <td colspan="3"></td>
        </tr>
    </tfoot>
</table>