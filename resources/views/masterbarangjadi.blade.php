@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Stok Barang Jadi</h1>
        </div>

        <div class="card">
            <div class="card-header">
                <a href={{ url('tambahbarangjadi') }}>
                    <button type="button" class="btn btn-primary my-auto">
                        Tambah Data
                    </button>
                </a>

            </div>
            <div class="card-body">
            
            <table id="tabelStokBarangJadi" class="table table-bordered table-no-wrap table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>No. Produksi</th>
                        <th>Jenis Box</th>
                        <th>Panjang</th>
                        <th>Lebar</th>
                        <th>Tinggi</th>
                        <th>Quantity</th>
                        <th>Jumlah Produksi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($barangJadi as $prm)
                        <tr>
                            <td>{{ $prm->id_produksi }}</td>
                            <td>{{ $prm->jenis_box }}</td>
                            <td>{{ $prm->panjang }}</td>
                            <td>{{ $prm->lebar }}</td>
                            <td>{{ $prm->tinggi }}</td>
                            <td>{{ $prm->qty }}</td>
                            <td>{{ $prm->jum_produksi }}</td>
                            <td>
                                {{-- <a href="{{ url('ubahbarangjadi/edit/' . $prm->id_produksi) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a> --}}
                                <form method="post" class="d-inline-block" action="{{ url('stokbarangjadi/delete/' . $prm->id_produksi) }}">
                                    @csrf
                                    <button type="submit" onclick="return confirm('Apakah yakin ingin dihapus?')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>

    </div>
    <!-- /.container-fluid -->
    @endsection
