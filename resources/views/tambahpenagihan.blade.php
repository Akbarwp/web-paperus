@extends('layouts.master')
@section('content')
<div class="container">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Form Penagihan / Invoicing</h1>
    </div>
    <div class="form-group">
        <form action="{{ url('/doAddpenagihan') }}" method="post">
            @csrf
            <label class="label">Pilih Filter</label>
            <div class="form-control">

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="radioPenawaran" name="inlineRadioOptions1" value="option3" onchange="myFunction()">
                    <label class="form-check-label" for="radioPenawaran">Penawaran</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="radioBarangJadi" name="inlineRadioOptions1" value="option3" onchange="myFunction()">
                    <label class="form-check-label" for="radioBarangJadi">Stok Barang Jadi</label>
                </div>
            </div>

            <div id="penawaran" style="display: none">
                <label class="label" for="readonlyTextInput">No. Penawaran</label>
                <input type="text" name="cekPenawaran" id="cekPenawaran" hidden>
                {{-- livesearch input --}}
                <select data-live-search="true" class="selectpicker form-control border border-secondary" id="id_penjualan1" name="id_penjualan1">
                    <option selected>Pilih No. Penawaran</option>
                    @foreach ($penawaran as $prm)
                        <option value={{ $prm->id_penawaran }}>{{ $prm->id_penawaran }}</option>
                    @endforeach
                </select>
            </div>
            <div id="barangJadi" style="display: none">
                <label class="label" for="readonlyTextInput">Kode Pembuatan</label>
                <input type="text" name="cekPenjualan" id="cekPenjualan" hidden>
                {{-- livesearch input --}}
                <select data-live-search="true" class="selectpicker form-control border border-secondary" id="id_penjualan2" name="id_penjualan2">
                    <option selected>Pilih Kode Pembuatan</option>
                    @foreach ($penjualanBarangJadi as $prm)
                        <option value={{ $prm->kode_pembuatan }}>{{ $prm->kode_pembuatan }}</option>
                    @endforeach
                </select>
            </div>

            <label for="exampleFormControlTextarea1" class="label">Customer</label>
            <input class="form-control" placeholder="Masukkan Customer" name="pic" id="pic">

            <label for="exampleFormControlTextarea1" class="label">Jenis Box</label>
            <input class="form-control" placeholder="Jenis Box" name="jenis_box" id="jenis_box">

            <label for="exampleFormControlTextarea1" class="label">Quantity</label>
            <input type="number" class="form-control" placeholder="Quantity" name="qty" id="qty">

            <label for="exampleFormControlTextarea1" class="label">Nominal</label>
            <input type="number" class="form-control" placeholder="Nominal" name="nominal" id="nominal">

            {{-- <label for="exampleFormControlTextarea1" class="label">Sudah Terbayar</label>
            <input type="number" class="form-control">

            <label for="exampleFormControlTextarea1" class="label">Sisa Hutang</label>
            <input type="number" class="form-control">

            <label for="exampleFormControlTextarea1" class="label">Jumlah Bayar</label>
            <input type="number" class="form-control"> --}}

            <br>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<script>
    function myFunction() {
        var checkBox = document.getElementById("radioPenawaran");

        if (checkBox.checked == true) {
            // --Penawaran--
            document.getElementById("penawaran").style.display = "block";
            document.getElementById("barangJadi").style.display = "none";
            $("#cekPenawaran").val(1);
            $("#cekPenjualan").val(0);

        } else {
            // --Stock Jadi--
            document.getElementById("penawaran").style.display = "none";
            document.getElementById("barangJadi").style.display = "block";
            $("#cekPenjualan").val(1);
            $("#cekPenawaran").val(0);
        }
    }

    $(document).ready(function() {
            $('#id_penjualan1').on('change', function() {
                const penawaran = $(this).val();

                if(penawaran) {
                    $.ajax({
                        url: '/cekPenawaranTagih/'+penawaran,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#pic').empty();
                                $('#jenis_box').empty();

                                $('#pic').val(data.pic);
                                $('#jenis_box').val(data.jenis_box);
                            }else{
                                $('#pic').empty();
                                $('#jenis_box').empty();
                            }
                        }
                    });
                }else{
                    $('#pic').empty();
                    $('#jenis_box').empty();
                }
            });
        });
        $(document).ready(function() {
            $('#id_penjualan2').on('change', function() {
                const kodePembuatan = $(this).val();

                if(kodePembuatan) {
                    $.ajax({
                        url: '/cekKodePembuatanTagih/'+kodePembuatan,
                        type: "GET",
                        data : {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success:function(data)
                        {
                            if(data){
                                $('#pic').empty();
                                $('#jenis_box').empty();
                                $('#qty').empty();
                                $('#nominal').empty();

                                $('#pic').val(data.nama_customer);
                                $('#jenis_box').val(data.jenis_box);
                                $('#qty').val(data.qty);
                                $('#nominal').val(data.nett);
                            }else{
                                $('#pic').empty();
                                $('#jenis_box').empty();
                                $('#qty').empty();
                                $('#nominal').empty();
                            }
                        }
                    });
                }else{
                    $('#pic').empty();
                    $('#jenis_box').empty();
                    $('#qty').empty();
                    $('#nominal').empty();
                }
            });
        });
</script>
@endsection
