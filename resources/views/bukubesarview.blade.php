@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-noborder b-radius">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                <h4>
                                    Buku Besar Periode {{ \Carbon\Carbon::parse($now)->format('F Y') }}
                                </h4>
                                <p id="bln"></p>
                            </div>
                            <div class="col-4">
                                <form action="{{ url('/accounting/buku_besar/view/filter') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col">
                                            <input type="month" name="bulan"
                                                value="{{ Carbon\Carbon::parse($now)->format('Y-m') }}" id="bulan"
                                                class="form-control form-control-lg" required>
                                        </div>
                                        @foreach ($akun as $a)
                                            <input type="hidden" name="kode_akun[]" value="{{ $a }}" />
                                        @endforeach
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-2">
                                <form action="{{ url('/accounting/buku_besar/pdf') }}" method="get" target="_blank">
                                    @csrf
                                    <input type="hidden" name="type" value="pdf">
                                    <input type="month" hidden name="bulan"
                                        value="{{ Carbon\Carbon::parse($now)->format('Y-m') }}" id="bulan3">
                                    <button type="submit" class="btn btn-danger" id="export">
                                        <i class="mdi mdi-file-pdf"></i> PDF
                                    </button>
                                    @foreach ($akun as $a)
                                        <input type="hidden" name="kode_akun[]" value="{{ $a }}" />
                                    @endforeach
                                </form>
                            </div>
                            <div class="col-2">
                                <form action="{{ url('/accounting/buku_besar/excel') }}" method="get" target="_blank">
                                    @csrf
                                    <input type="hidden" name="type" value="export">
                                    <input type="month" hidden name="bulan"
                                        value="{{ Carbon\Carbon::parse($now)->format('Y-m') }}" id="bulan2">
                                    <button type="submit" class="btn btn-success" id="export">
                                        <i class="mdi mdi-file-excel"></i> Excel
                                    </button>
                                    @foreach ($akun as $a)
                                        <input type="hidden" name="kode_akun[]" value="{{ $a }}" />
                                    @endforeach
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @foreach ($buku_besars as $buku_besar)
                            <div class="table-responsive">
                                <h6>Nama akun: {{ $buku_besar['nama'] }}</h6>
                                <h6>No akun: {{ $buku_besar['kode_akun'] }}</h6>
                                <table class="table table-bordered" id="table_buku_besar">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center">Tanggal</th>
                                            <th style="text-align:center">Keterangan</th>
                                            <th style="text-align:center">Ref</th>
                                            <th style="text-align:center">Saldo Awal</th>
                                            <th style="text-align:center">Debit</th>
                                            <th style="text-align:center">Kredit</th>
                                            <th style="text-align:center">Saldo Akhir</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($buku_besar['jurnals'] as $jurnal)
                                            <tr>
                                                <td>{{ \Carbon\Carbon::parse($jurnal->tanggal)->format('Y/m/d') }}</td>
                                                <td><b>{{ $jurnal->keterangan }}</b></td>
                                                <td><b>{{ $jurnal->bukti_transaksi }}</b></td>
                                                <td><b>Rp. {{ number_format($jurnal->saldo_awal, 0, ',', '.') }}</b></td>
                                                <td><b>Rp. {{ number_format($jurnal->debit, 0, ',', '.') }}</b></td>
                                                <td><b>Rp. {{ number_format($jurnal->kredit, 0, ',', '.') }}</b></td>
                                                <td><b>Rp. {{ number_format($jurnal->saldo_akhir, 0, ',', '.') }}</b></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    {{-- moment --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

    <script>
        $('#buku_besar').DataTable({
            dom: 'lBfrtip',
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            buttons: [
                'colvis',
                {
                    extend: 'print',
                    footer: true
                },
                {
                    extend: 'copyHtml5',
                    footer: true
                },
                {
                    extend: 'excelHtml5',
                    footer: true
                },
                {
                    extend: 'pdfHtml5',
                    footer: true
                }
            ],
        });

        @if($message = Session::get('error'))
        swal(
            "",
            "{{ $message }}",
            "error"
        );
        @endif
    </script>
@endsection
