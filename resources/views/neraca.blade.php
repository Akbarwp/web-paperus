@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-4">
                                <h4>
                                    Neraca Tahun {{ Carbon\Carbon::parse($now)->format('Y') }}
                                </h4>
                                <p>Periode : Bulan {{ Carbon\Carbon::parse($now)->format('F') }}</p>
                            </div>
                            <div class="col-6">
                                <form action="{{ url('/accounting/neraca/filter') }}" method="get">
                                    <div class="row">
                                        <div class="col">
                                            <select class="form-control form-control-lg" name="comparison">
                                                <option value="now" @if (request()->input('comparison') == 'now') selected @endif>
                                                    Sekarang</option>
                                                <option value="last_month"
                                                    @if (request()->input('comparison') == 'last_month') selected @endif>Bulan Lalu</option>
                                                <option value="current_year"
                                                    @if (request()->input('comparison') == 'current_year') selected @endif>Tahun Berjalan
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <input type="month" value="{{ Carbon\Carbon::parse($now)->format('Y-m') }}"
                                                name="bulan" id="bulan" class="form-control form-control-lg" required>
                                        </div>
                                        <div class="col">
                                            <select class="form-control form-control-lg" id="transaction_type"
                                                name="transaction_type">
                                                <option value="1" @if (request()->input('transaction_type') == '1') selected @endif>PPN
                                                </option>
                                                <option value="2" @if (request()->input('transaction_type') == '2') selected @endif>Non
                                                    PPN</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-1">
                                <form action="{{ url('/accounting/neraca/pdf') }}" method="get" target="_blank">
                                    @csrf
                                    <input type="hidden" name="comparison" value="{{ request()->input('comparison') }}">
                                    <input type="hidden" name="type" value="pdf">
                                    <input type="month" name="bulan"
                                        value="{{ Carbon\Carbon::parse($now)->format('Y-m') }}" hidden id="bulan2"
                                        class="form-control">
                                    <div class="form-group">
                                        <button class="btn btn-danger" type="submit" id="export">
                                            <i class="mdi mdi-file-pdf"></i> PDF
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-1">
                                <form action="{{ url('/accounting/neraca/excel') }}" method="get" target="_blank">
                                    @csrf
                                    <input type="hidden" name="comparison" value="{{ request()->input('comparison') }}">
                                    <input type="hidden" name="type" value="export">
                                    <input type="month" name="bulan"
                                        value="{{ Carbon\Carbon::parse($now)->format('Y-m') }}" hidden id="bulan3"
                                        class="form-control">
                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit" id="export">
                                            <i class="mdi mdi-file-excel"></i> Excel
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div id="data_neraca">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Bulan</th>
                                            @foreach ($comparisons as $comparison)
                                                <th>{{ $comparison->format('F') }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($neracas as $neraca => $tipes)
                                            @php $total_neraca = []; @endphp
                                            <tr>
                                                <td><b>{{ $neraca }}</b></td>
                                                @foreach ($comparisons as $comparison)
                                                    <td>&nbsp;</td>
                                                @endforeach
                                            </tr>
                                            @foreach ($tipes as $tipe => $akuns)
                                                @php $total_tipe = []; @endphp
                                                <tr>
                                                    <td><b>{{ $tipe }}</b></td>
                                                    @foreach ($comparisons as $comparison)
                                                        <td>&nbsp;</td>
                                                    @endforeach
                                                </tr>
                                                @foreach ($akuns as $key => $value)
                                                    @php
                                                        
                                                        foreach ($value['saldo_akhir'] as $idxSaldo => $saldo_akhir) {
                                                            if (($neraca == 'Aktiva' && $value['saldo_normal'] == 'kredit') || $value['kode_akun'] == \App\Akun::$prive) {
                                                                $total_tipe[$idxSaldo] = ($total_tipe[$idxSaldo] ?? 0) - $saldo_akhir;
                                                                $total_neraca[$idxSaldo] = ($total_neraca[$idxSaldo] ?? 0) - $saldo_akhir;
                                                            } else {
                                                                $total_tipe[$idxSaldo] = ($total_tipe[$idxSaldo] ?? 0) + $saldo_akhir;
                                                                $total_neraca[$idxSaldo] = ($total_neraca[$idxSaldo] ?? 0) + $saldo_akhir;
                                                            }
                                                        }
                                                        
                                                    @endphp
                                                    <tr>
                                                        <td>{{ $value['nama_akun'] }}</td>
                                                        @foreach ($value['saldo_akhir'] as $saldo)
                                                            <td>Rp. {{ number_format($saldo, 0, ',', '.') }}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td><b>Total {{ $tipe }}</b></td>
                                                    @foreach ($total_tipe as $total_t)
                                                        <td><b>Rp. {{ number_format($total_t, 0, ',', '.') }}</b></td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td><b>Total {{ $neraca }}</b></td>
                                                @foreach ($total_neraca as $total_n)
                                                    <td><b>Rp. {{ number_format($total_n, 0, ',', '.') }}</b></td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script>
        // $('#data_neraca').show();
        // $('#tetap').show();
        // $('#lancar').show();
        // $('#jenis').change(function() {
        //     if ($(this).val() == 'neraca') {
        //         $('#data_neraca').show();
        //         $('#tetap').hide();
        //         $('#lancar').hide();
        //     } else if ($(this).val() == 'tetap') {
        //         $('#data_neraca').hide();
        //         $('#tetap').show();
        //         $('#lancar').hide();
        //     } else if ($(this).val() == 'lancar') {
        //         $('#data_neraca').hide();
        //         $('#tetap').hide();
        //         $('#lancar').show();
        //     } else if ($(this).val() == '') {
        //         $('#data_neraca').show();
        //         $('#tetap').show();
        //         $('#lancar').show();
        //     }
        // });



        $(document).ready(function() {
            // $('input[name=bulan]').change(function() {
            //     $('input[name=bulan2]').val($(this).val());
            // });

            $('#neraca').DataTable({
                dom: 'lBfrtip',
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                buttons: [
                    'colvis',
                    {
                        extend: 'print',
                        footer: true
                    },
                    {
                        extend: 'copyHtml5',
                        footer: true
                    },
                    {
                        extend: 'excelHtml5',
                        footer: true
                    },
                    {
                        extend: 'pdf',
                        footer: true
                    }
                ],
            });
            $('#aktiva_tetap').DataTable({
                dom: 'lBfrtip',
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                buttons: [
                    'colvis',
                    {
                        extend: 'print',
                        footer: true
                    },
                    {
                        extend: 'copyHtml5',
                        footer: true
                    },
                    {
                        extend: 'excelHtml5',
                        footer: true
                    },
                    {
                        extend: 'pdfHtml5',
                        footer: true
                    }
                ],
            });
            $('#aktiva_lancar').DataTable({
                dom: 'lBfrtip',
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                buttons: [
                    'colvis',
                    {
                        extend: 'print',
                        footer: true
                    },
                    {
                        extend: 'copyHtml5',
                        footer: true
                    },
                    {
                        extend: 'excelHtml5',
                        footer: true
                    },
                    {
                        extend: 'pdfHtml5',
                        footer: true
                    }
                ],
            });
        });
    </script>
@endsection
