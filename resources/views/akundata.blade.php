@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                <h1 class="h3 mb-0 text-gray-800">
                                    Data Akun
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="data_akun">
                                <thead>
                                    <tr>
                                        <th>Bulan</th>
                                        <th>Kode Akun</th>
                                        <th>Nama</th>
                                        <th>Tipe Akun</th>
                                        <th>Saldo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $awal = 0;
                                    @endphp
                                    @foreach ($akun as $i => $item)
                                        {{-- @if ($i == 0)
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>100</th>
                                    <th>Harta</th>
                                    <th>Harta</th>
                                    <th></th>
                                </tr>
                                @endif --}}
                                        @if ($item->tipe_akun == 'Harta')
                                            <tr>
                                                <td>{{ $item->created_at->format('Y/m') }}</td>
                                                <td>{{ $item->kode_akun }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tipe_akun }}</td>
                                                <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif

                                        {{-- @if ($i == 1)
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>200</th>
                                    <th>Kewajiban</th>
                                    <th>Kewajiban</th>
                                    <th></th>
                                </tr>
                                @endif --}}
                                        @if ($item->tipe_akun == 'Kewajiban')
                                            <tr>
                                                <td>{{ $item->created_at->format('Y/m') }}</td>
                                                <td>{{ $item->kode_akun }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tipe_akun }}</td>
                                                <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif

                                        {{-- @if ($i == 2)
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>300</th>
                                    <th>Modal</th>
                                    <th>Modal</th>
                                    <th></th>
                                </tr>
                                @endif --}}
                                        @if ($item->tipe_akun == 'Modal')
                                            <tr>
                                                <td>{{ $item->created_at->format('Y/m') }}</td>
                                                <td>{{ $item->kode_akun }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tipe_akun }}</td>
                                                <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif

                                        {{-- @if ($i == 3)
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>400</th>
                                    <th>Pendapatan</th>
                                    <th>Pendapatan</th>
                                    <th></th>
                                </tr>
                                @endif --}}
                                        @if ($item->tipe_akun == 'Pendapatan')
                                            <tr>
                                                <td>{{ $item->created_at->format('Y/m') }}</td>
                                                <td>{{ $item->kode_akun }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tipe_akun }}</td>
                                                <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif

                                        {{-- @if ($i == 4)
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>500</th>
                                    <th>HPP</th>
                                    <th>HPP</th>
                                    <th></th>
                                </tr>
                                @endif --}}
                                        @if ($item->tipe_akun == 'HPP')
                                            <tr>
                                                <td>{{ $item->created_at->format('Y/m') }}</td>
                                                <td>{{ $item->kode_akun }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tipe_akun }}</td>
                                                <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif

                                        {{-- @if ($i == 5)
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>600</th>
                                    <th>Beban</th>
                                    <th>Beban</th>
                                    <th></th>
                                </tr>
                                @endif --}}
                                        @if ($item->tipe_akun == 'Beban')
                                            <tr>
                                                <td>{{ $item->created_at->format('Y/m') }}</td>
                                                <td>{{ $item->kode_akun }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tipe_akun }}</td>
                                                <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif

                                        {{-- @if ($i == 5)
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>700</th>
                                    <th>Laba/Rugi</th>
                                    <th>Laba/Rugi</th>
                                    <th></th>
                                </tr>
                                @endif --}}
                                        @if ($item->tipe_akun == 'Laba/Rugi')
                                            <tr>
                                                <td>{{ $item->created_at->format('Y/m') }}</td>
                                                <td>{{ $item->kode_akun }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->tipe_akun }}</td>
                                                <td>Rp. {{ number_format($item->saldo_awal, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script>
        $(document).ready(function() {
            $('#data_akun').DataTable({
                dom: 'lBfrtip',
                "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                buttons: [
                    'colvis',
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            { 
                                extend: 'print',
                                footer: true 
                            },
                            { 
                                extend: 'copyHtml5',
                                footer: true 
                            },
                            { 
                                extend: 'excelHtml5',
                                footer: true 
                            },
                            { 
                                extend: 'pdfHtml5',
                                footer: true 
                            },
                        ]
                    },
                ],
                processing: true,
            });
        });
    </script>
@endsection
