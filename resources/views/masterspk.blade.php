@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Master Surat Perintah Kerja</h1>
        </div>

        <div class="card">
            <div class="card-header">
                <a href={{ url('suratperintahkerja') }}>
                    <button type="button" class="btn btn-primary my-auto">
                        Tambah Data
                    </button>
                </a>
            </div>
            <div class="card-body">
            <table id="tabelPenagihan" class="table table-bordered table-no-wrap table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>No. SPK</th>
                        <th>ID Penjualan</th>
                        <th>Customer</th>
                        <th>Jenis Box</th>
                        <th>Jumlah Produksi</th>
                        <th>Link Desain</th>
                        <th>Pisau</th>
                        <th>Plat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($no_spk as $prm)
                        <tr>
                            <td>{{ $prm->no_spk }}</td>
                            <td>{{ $prm->id_penjualan }}</td>
                            <td>{{ $prm->pic }}</td>
                            <td>{{ $prm->jenis_box }}</td>
                            <td>{{ $prm->jum_produksi }}</td>
                            <td>{{ $prm->link_desain }}</td>
                            <td>{{ $prm->pisau }}</td>
                            <td>{{ $prm->plat }}</td>
                            <td>
                                {{-- <button type="button" class="btn btn-warning"><i class="fas fa-edit"></i></button> --}}
                                <form method="post" class="d-inline-block" action="{{ url('formSPK/delete/' . $prm->no_spk) }}">
                                    @csrf
                                    <button onclick="return confirm('Apakah yakin ingin dihapus?')" type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>

    </div>
    <!-- /.container-fluid -->
    @endsection