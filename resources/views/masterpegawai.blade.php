@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Master Pegawai</h1>
        </div>

        @if (session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        <div class="card">
            <div class="card-header">
                <a href={{ url('tambahpegawai') }}>
                    <button type="button" class="btn btn-primary my-auto">
                        Tambah Data
                    </button>
                </a>

                <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel">
                    Import Excel
                </button>

                <!-- Import Excel -->
                <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form method="post" action="/pegawai/import_excel" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                                </div>
                                <div class="modal-body">
                                    <label>Pilih file excel</label>
                                    <div class="form-group">
                                        <input type="file" name="file" required="required">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Import</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="card-body">
            <form action="">
                @csrf

            </form>
            <table id="tabelMasterPegawai" class="table table-bordered table-no-wrap table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>ID Pegawai</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>No. Telp</th>
                        <th>Email</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($arrPegawai as $prm)
                    <tr>
                        <td>{{$prm->id_pegawai}}</td>
                        <td>{{$prm->nama_pegawai}}</td>
                        <td>{{$prm->alamat_pegawai}}</td>
                        <td>{{$prm->notelp_pegawai}}</td>
                        <td>{{$prm->email_pegawai}}</td>
                        <td>
                            <div style="display: flex;">
                                {{-- <form action="">
                                    @csrf
                                    <button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i></button>
                                </form> --}}
                                <form  action="{{ url('masterpegawai/delete/' . $prm->id_pegawai) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>


                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
