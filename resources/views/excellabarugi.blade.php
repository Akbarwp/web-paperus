<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laba Rugi</title>
</head>

<body>
    @php $sep_thsnd = $export_type == "export" ? "" : "."; @endphp

    @php
    $thn = $years;
    $namabln = $bln;
    $bln = $month;
    $fbulan = Carbon\Carbon::parse($thn.'-'.$month)->format('Y-m');
    @endphp
    @if ($export_type == "pdf")
    @php
        $bulan = \Carbon\Carbon::parse($now)->translatedFormat('F');
        $month = date('m', strtotime($now));
        $year  = date('Y', strtotime($now));
    @endphp
    <h3 class="title"><center>CV. Anzac Food</center></h3>
    <h3 class="title"><center>Laporan Laba Rugi</center></h3>
    <h4 class="title"><center>{{ date('01', strtotime($now)) }} s/d {{ date('t', strtotime($now)) }} {{$bulan}} {{ $year }}</center></h4>
    @endif

    @foreach($labarugis as $dataLabarugi)
    @php $labarugi = $dataLabarugi['labarugi']; @endphp
    <table style="width: 100%">
        @if(count($comparisons) > 1)
        <thead>
            <tr>
                <th colspan="6" style="text-align: center;">{{ $dataLabarugi['now']->translatedFormat('F') }}</th>
            </tr>
        </thead>
        @endif
        <tbody>
            <tr>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
                <th style="text-align: left; border-bottom: 1px double black;">&nbsp;</th>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            {{-- Pendapatan --}}
            <tr>
                <td colspan="6" style="text-align: left;">PENDAPATAN</td>
            </tr>
            @foreach (($labarugi['Pendapatan'] ?? []) as $akun)
                <tr>
                    <td colspan="2" style="text-align: left;">{{$akun['nama_akun']}}</td>
                    @if($akun['saldo_normal'] == 'kredit')
                    <td></td>
                    <td></td>
                    @endif
                    <td style="text-align: right;">Rp.</td>
                    <td style="text-align: right;" data-format="#,##0">{{number_format($akun['saldo_'.$akun['saldo_normal']],0,',',$sep_thsnd)}}</td>
                    @if($akun['saldo_normal'] == 'debit')
                    <td></td>
                    <td></td>
                    @endif
                </tr>
            @endforeach
            <tr>
                <td colspan="2" style="text-align: left;">Pendapatan Bersih</td>
                <td></td>
                <td></td>
                <td style="text-align: right;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black" data-format="#,##0">{{number_format(collect($labarugi['Pendapatan'])->sum('saldo_total'),0,',',$sep_thsnd)}}</td>
            </tr>
            {{-- ========== --}}

            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>

            {{-- Beban --}}
            <tr>
                <td colspan="6" style="text-align: left;">BIAYA OPERASIONAL</td>
            </tr>
            @foreach (($labarugi['Beban'] ?? []) as $akun)
                <tr>
                    <td>&nbsp;</td>
                    <td style="text-align: left;">{{$akun['nama_akun']}}</td>
                    @if($akun['saldo_normal'] == 'kredit')
                    <td></td>
                    <td></td>
                    @endif
                    <td style="text-align: right;">Rp.</td>
                    <td style="text-align: right;" data-format="#,##0">{{number_format($akun['saldo_'.$akun['saldo_normal']],0,',',$sep_thsnd)}}</td>
                    @if($akun['saldo_normal'] == 'debit')
                    <td></td>
                    <td></td>
                    @endif
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="border-top: 1px solid black;">&nbsp;</td>
                <td style="text-align: right;">Rp.</td>
                <td style="text-align: right;" data-format="#,##0">{{number_format(collect($labarugi['Beban'] ?? [])->sum('saldo_total'),0,',',$sep_thsnd)}}</td>
            </tr>
            {{-- ===== --}}


            <tr>
                <th colspan="2" style="text-align: left;">LABA SEBELUM PAJAK</th>
                <th></th>
                <th></th>
                <td style="text-align: right;">Rp.</td>
                <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px double black;" data-format="#,##0">
                    {{number_format(collect($labarugi['Pendapatan'] ?? [])->sum('saldo_total') - collect($labarugi['Beban'] ?? [])->sum('saldo_total'), 0,',',$sep_thsnd)}}
                </td>
            </tr>
        </tbody>
    </table>
    @endforeach

</body>

</html>