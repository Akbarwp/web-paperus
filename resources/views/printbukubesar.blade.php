<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Buku Besar</title>
</head>
<style>
    /* font size */
    * {
        font-size: 11px;
        /* font-family: Verdana, Arial, sans-serif; */
    }

    .table-bordered thead tr th,
    .table-bordered tbody tr th,
    .table-bordered tfoot tr th,
    .table-bordered thead tr td,
    .table-bordered tbody tr td,
    .table-bordered tfoot tr td {
        border: 0.5px solid rgb(160, 160, 160);
    }

    .table-bordered thead tr th,
    .table-bordered tbody tr td,
    .table-bordered tfoot tr td {
        padding: 2px;
    }
</style>

<body>
    <button id="prr" onclick="this.style.display='none'; print(); this.style.display='block';"> > PRINT < </button>
            @php
                $bulan = \Carbon\Carbon::parse($start_date)->translatedFormat('F');
                $month = date('m', strtotime($start_date));
                $year = date('Y', strtotime($start_date));
            @endphp
            <h1>
                <center>CV. Anzac Food</center>
            </h1>
            <h1>
                <center>Laporan Buku Besar</center>
            </h1>
            <h2>
                <center>{{ date('d', strtotime($start_date)) }} s/d {{ date('d', strtotime($end_date)) }}
                    {{ $bulan }} {{ $year }}</center>
            </h2>
            @foreach ($buku_besars as $buku_besar)
                <table class="table table-bordered" style="width: 100%;">
                    <h3>Nama akun: {{ $buku_besar['nama'] }}</h3>
                    <h3>No akun: {{ $buku_besar['kode_akun'] }}</h3>
                    <h3>Saldo Awal: {{ $buku_besar['saldo_awal'] }}</h3>
                    <thead>
                        <tr>
                            <th style="text-align:center">Tanggal</th>
                            <th style="text-align:center">Keterangan</th>
                            <th style="text-align:center">Ref</th>
                            <th style="text-align:center">Debet</th>
                            <th style="text-align:center">Kredit</th>
                            <th style="text-align:center">Akhir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($buku_besar['jurnals'] as $jurnal)
                            <tr>
                                <td style="text-align:center">{{ date('d-m-Y', strtotime($jurnal->tanggal)) }}</td>
                                <td style="text-align:center">{{ $jurnal->bukti_transaksi }}</td>
                                <td style="text-align:left">{{ $jurnal->keterangan }}</td>
                                <td style="text-align:right">{{ number_format($jurnal->debit, 2, ',', '.') }}</td>
                                <td style="text-align:right">{{ number_format($jurnal->kredit, 2, ',', '.') }}</td>
                                <td style="text-align:right">{{ number_format($jurnal->saldo_akhir, 2, ',', '.') }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endforeach
            <script>
                print() {
                    window.onload = function() {
                        document.getElementById("prr").style.display = 'none';
                        window.print();
                    }
                    window.onafterprint = window.close;
                    document.getElementById("prr").style.display = 'block';
                };
            </script>
</body>

</html>
