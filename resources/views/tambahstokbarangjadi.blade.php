@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Pembuatan Barang Jadi</h1>
        </div>

        <div class="form-group">
            <form action="{{ url('/doAddPembuatanBarangJadi') }}" method="post">
                @csrf
                <label class="label" for="readonlyTextInput">No. Produksi</label>
                <input id="readonlyTextInput" class="form-control" name="kode" placeholder="No. Produksi" value="{{ $kode }}" readonly>

                
                {{-- <div id="penjualan" style="display: none"> --}}
                <div id="penjualan">
                    <label for="exampleFormControlTextarea1" class="label">Jenis Box</label>
                    <input class="form-control" placeholder="Jenis Box" name="jenis_box">

                    <label for="exampleFormControlTextarea1" class="label">Panjang</label>
                    <input class="form-control" placeholder="Masukkan panjang" name="panjang">

                    <label for="exampleFormControlTextarea1" class="label">Lebar</label>
                    <input class="form-control" placeholder="Masukkan lebar" name="lebar">

                    <label for="exampleFormControlTextarea1" class="label">Tinggi</label>
                    <input class="form-control" placeholder="Masukkan tinggi" name="tinggi">

                    <label for="exampleFormControlTextarea1" class="label">Quantity</label>
                    <input type="number" class="form-control" placeholder="Masukkan Quantity" name="qty">

                    <label for="exampleFormControlTextarea1" class="label">Jumlah Produksi</label>
                    <input type="number" class="form-control" placeholder="Masukkan Jumlah Produksi" name="jum_produksi">

                    <br>
                    <div class="checkbox">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" value="Kertas" id="inlineCheckbox1">
                            <label class="form-check-label" for="inlineCheckbox1">Kertas</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox2" value="Tinta">
                            <label class="form-check-label" for="inlineCheckbox2">Tinta</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox3" value="Laminasi">
                            <label class="form-check-label" for="inlineCheckbox3">Laminasi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox4" value="Plong">
                            <label class="form-check-label" for="inlineCheckbox4">Plong</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox5" value="Hotprint">
                            <label class="form-check-label" for="inlineCheckbox5">Hotprint</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox6" value="Sortir">
                            <label class="form-check-label" for="inlineCheckbox6">Sortir</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox7" value="Pembelian Dus">
                            <label class="form-check-label" for="inlineCheckbox7">Pembelian Dus</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox8" value="Packing">
                            <label class="form-check-label" for="inlineCheckbox8">Packing</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb[]" id="inlineCheckbox9" value="Emboss">
                            <label class="form-check-label" for="inlineCheckbox9">Emboss</label>
                        </div>
                        
                        <button onclick="showCheckbox()" type="button" class="btn btn-info">Tambah</button>
                        <input type="hidden" name="proses2" value="0">
                    </div>
                    <div class="checkbox" id="myCheckbox" style="display: none;">
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox10"
                                value="Kertas">
                            <label class="form-check-label" for="inlineCheckbox10">Kertas</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox11"
                                value="Tinta">
                            <label class="form-check-label" for="inlineCheckbox11">Tinta</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox12"
                                value="Laminasi">
                            <label class="form-check-label" for="inlineCheckbox12">Laminasi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox13"
                                value="Plong">
                            <label class="form-check-label" for="inlineCheckbox13">Plong</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox14"
                                value="Hotprint">
                            <label class="form-check-label" for="inlineCheckbox14">Hotprint</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox15"
                                value="Sortir">
                            <label class="form-check-label" for="inlineCheckbox15">Sortir</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox16"
                                value="Pembelian Dus">
                            <label class="form-check-label" for="inlineCheckbox16">Pembelian Dus</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox17"
                                value="Packing">
                            <label class="form-check-label" for="inlineCheckbox17">Packing</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="cb2[]" id="inlineCheckbox18"
                                value="Emboss">
                            <label class="form-check-label" for="inlineCheckbox18">Emboss</label>
                        </div>
                    </div>
                    <br>
                {{-- <input type="hidden" name="data"> --}}
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <script>

        function showCheckbox() {
            var x = document.getElementById("myCheckbox");

            if (x.style.display == "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
            if ($("[name='proses2']").val() == "0") {
                $("[name='proses2']").val("1");
            } else {
                $("[name='proses2']").val("0");
            }
            console.log($("[name='proses2']").val());
        }
    </script>
@endsection
