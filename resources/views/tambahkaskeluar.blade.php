@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if (session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                <h4 class="h3 mb-0 text-gray-800">
                                    Kas Keluar
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('/accounting/kasKeluar/process') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="perusahaan">Perusahaan</label>
                                <select name="perusahaan" class="form-control form-control-lg" id="perusahaan" required>
                                    <option value=""> -- Pilih Perusahaan -- </option>
                                    @foreach (\App\Models\Akun::$perusahaans as $key => $perusahaan)
                                        <option value="{{ $key }}">{{ $perusahaan }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="bukti_transaksi">Bukti Transaksi</label>
                                <input type="text" name="bukti_transaksi" value="{{ $bukti_transaksi }}"
                                    class="form-control form-control-lg" id="bukti_transaksi" required>
                            </div>

                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input type="date" step="1" name="tanggal" class="form-control form-control-lg"
                                    id="tanggal" required>
                            </div>

                            <br>
                            <hr>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Kode Akun</th>
                                        <th>Keterangan</th>
                                        <th>Saldo</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body">
                                    <tr>
                                        <td><select name='kode_akun[]' id="0"
                                                class='form-control form-control-sm select2' style="width: 100%;"
                                                id='kode_akun' required>
                                                @foreach ($akun as $item)
                                                    <option value='{{ $item->id }}'>
                                                        {{ $item->kode_akun . ' ' . $item->nama }}</option>
                                                @endforeach
                                            </select></td>
                                        <td><input class='form-control form-control-sm' type='text'
                                                name='keterangan[]' /></td>
                                        <td>Rp. <input class='form-control form-control-sm' type='text' name='saldo[]'
                                                style="width: 90%; font-weight:bold;" data-type="thousand" /></td>
                                        <td><button type="button" class="btn btn-danger mr-3"
                                                id="button-hapus">Hapus</button></td>
                                    </tr>
                                </tbody>
                                <table>

                                    <br>

                                    <div class="form-group">
                                        <button type="button" class="btn btn-success mr-3"
                                            id="button-tambahkan">Tambahkan</button>
                                    </div>

                                    <div class="d-flex justify-content-end">
                                        <button type="button" class="btn btn-secondary mr-3"
                                            id="button-cancel">Cancel</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script>
        var i = 1;
        $("#button-tambahkan").click(function() {
            $("#table-body").append("<tr><td><select name='kode_akun[]' id='" + i + "' class='form-control form-control-sm select2' id='kode_akun' required>@foreach ($akun as $item)<option value='{{ $item->id}}'>{{ $item->kode_akun.' '.$item->nama }}</option>@endforeach</select></td><td><input class='form-control form-control-sm' type='text' name='keterangan[]' /></td><td>Rp. <input class='form-control form-control-sm' type='text' name='saldo[]' data-type='thousand' style='font-weight: bold; width: 90%;' /></td><td><button type='button' class='btn btn-danger mr-3' id='button-hapus'>Hapus</button></td></tr>")
            $(".select2").select2();
            i++;
        });

        $("#table-body").on('click', '#button-hapus', function() {
            $(this).closest('tr').remove();
        });

        $(".select2").select2();

        function numberWithCommas(x) {
            if (x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }
            return x;
        }

        $(document).on("input", 'input[data-type="thousand"]', function() {
            $(this).val(numberWithCommas($(this).val()));
        });
    </script>
@endsection
