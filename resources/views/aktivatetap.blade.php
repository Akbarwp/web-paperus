@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="row page-title-header">
        <div class="col-12">
            <div class="page-header d-flex justify-content-between align-items-center">
                <h4 class="page-title h3 mb-0 text-gray-800">Aktiva Tetap</h4>
                <div class="d-flex justify-content-start">
                    <button class="btn btn-primary btn-new ml-2" id="button-add">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="form-add">
        <div class="col-12 grid-margin">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    <form action="{{ url('/accounting/aktiva_tetap/add') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="kode">Kode Akun Bank</label>
                            <select name="kode" class="form-control form-control-lg" id="kode" required>
                                @foreach ($akun as $item)
                                    <option value="{{ $item->id }}">{{ $item->kode_akun }} {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nama">Tanggal</label>
                            <input type="date" class="form-control form-control-lg" name="tanggal" id="tanggal"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="jenis">Kategori</label>
                            <select name="jenis_aktiva" id="jenis_aktiva" class="form-control form-control-lg" required>
                                <option value="" hidden> -- Pilih Jenis Aktiva -- </option>
                                <option value="Kendaraan">Kendaraan</option>
                                <option value="Pembelian aset">Pembelian aset</option>
                                <option value="Rumah">Rumah</option>
                                <option value="Tanah">Tanah</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                        <div class="form-group" id="lain-lain">
                            <input type="text" class="form-control form-control-lg" name="jenis_lain"
                                placeholder="Jenis Lain-lain">
                        </div>
                        <div class="form-group">
                            <label for="metode_bayar">Metode Bayar</label>
                            <select name="metode_bayar" class="form-control form-control-lg" required>
                                <option value="" hidden> -- Pilih Metode Bayar -- </option>
                                <option value="kas">Kas</option>
                                <option value="bank">Bank</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nominal">Nominal</label>
                            <input type="text" class="form-control form-control-lg" name="nominal" id="nominal"
                                placeholder="Nominal" required>
                        </div>
                        <div class="form-group">
                            <label for="durasi">Durasi</label>
                            <select name="durasi_aktiva" class="form-control" id="durasi" required>
                                <option value="12">12 Bulan (1 Thn)</option>
                                <option value="24">24 Bulan (2 Thn)</option>
                                <option value="36">36 Bulan (3 Thn)</option>
                                <option value="48">48 Bulan (4 Thn)</option>
                                <option value="60">60 Bulan (5 Thn)</option>
                                <option value="72">72 Bulan (6 Thn)</option>
                                <option value="84">84 Bulan (7 Thn)</option>
                                <option value="96">96 Bulan (8 Thn)</option>
                                <option value="108">108 Bulan (9 Thn)</option>
                                <option value="120">120 Bulan (10 Thn)</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="penyusutan">Penyusutan</label>
                            <input type="number" class="form-control form-control-lg" name="penyusutan" id="penyusutan"
                                placeholder="0" readonly required>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control form-control-lg" name="keterangan" id="keterangan" rows="3" required></textarea>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="button" id="button-cancel" class="btn btn-secondary mr-2">Cancel</button>
                            <button type="submit" class="btn btn-primary btn-save">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- div card table --}}
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    {{-- <div class="row mb-3">
                        <div class="col-6">
                            <input type="text" class="form-control form-control-lg h-100" name="search" placeholder="Cari">
                        </div>
                    </div> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table-aktiva-lancar">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Tanggal</th>
                                    <th>Jenis</th>
                                    <th>Metode Bayar</th>
                                    <th>Nominal</th>
                                    <th>Durasi Aktiva</th>
                                    <th>Penyusutan</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($aktiva_tetap as $item)
                                    <tr>
                                        <td>{{ \App\Models\Akun::find($item->kode)->kode_akun }}</td>
                                        <td>{{ Carbon\Carbon::parse($item->tanggal)->format('Y/m/d') }}</td>
                                        @if ($item->jenis_aktiva != 'Lain-lain')
                                            <td>{{ $item->jenis_aktiva }}</td>
                                        @else
                                            <td>{{ $item->jenis_lain }}</td>
                                        @endif
                                        <td>{{ $item->metode_bayar }}</td>
                                        <td>Rp. {{ number_format($item->nominal, 2, ',', '.') }}</td>
                                        <td>{{ $item->durasi_aktiva }} Bln</td>
                                        <td>Rp. {{ number_format($item->penyusutan, 2, ',', '.') }}</td>
                                        <td>{{ $item->keterangan }}</td>
                                        <td>
                                            <button type="button" data-target="#edit-aktiva-tetap"
                                                data-id="{{ $item->id }}" data-toggle="modal"
                                                class="btn btn-icons btn-info btn-edit">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <a href="#" data-id="{{ $item->id }}"
                                                data-nama="{{ $item->jenis_aktiva }}"
                                                class="btn btn-icons btn-danger btn-delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal edit data aktiva Tetap --}}
    <div class="modal fade" id="edit-aktiva-tetap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Aktiva Tetap</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/accounting/aktiva_tetap/update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id2">
                        <div class="form-group" hidden>
                            <label for="kode">Kode Akun Bank</label>
                            <select name="kode2" class="form-control form-control-lg" id="kode2">
                                @foreach ($akun as $i)
                                    <option value="{{ $i->id }}">{{ $i->kode_akun . ' ' . $i->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tanggal">Tanggal</label>
                            <input type="date" name="tanggal2" class="form-control form-control-lg" id="tanggal2">
                        </div>
                        <div class="form-group">
                            <label for="jenis">Kategori</label>
                            <select name="jenis_aktiva2" id="jenis_aktiva" class="form-control form-control-lg">
                                <option value="" hidden> -- Pilih Jenis Aktiva -- </option>
                                <option value="Kendaraan">Kendaraan</option>
                                <option value="Pembelian aset">Pembelian aset</option>
                                <option value="Rumah">Rumah</option>
                                <option value="Tanah">Tanah</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                        <div class="form-group" id="lain-lain">
                            <input type="text" class="form-control form-control-lg" name="jenis_lain2"
                                id="jenis_lain" placeholder="Jenis Lain">
                        </div>
                        <div class="form-group">
                            <label for="metode_bayar">Metode Bayar</label>
                            <select name="metode_bayar2" id="metode_bayar" class="form-control form-control-lg" required>
                                <option value="" hidden> -- Pilih Metode Bayar -- </option>
                                <option value="kas">Kas</option>
                                <option value="bank">Bank</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nominal">Nominal</label>
                            <input type="number" class="form-control form-control-lg" name="nominal2" id="nominal2"
                                placeholder="Nominal">
                        </div>
                        <div class="form-group">
                            <label for="durasi">Durasi Aktiva</label>
                            <select name="durasi_aktiva2" id="durasi_aktiva2" class="form-control form-control-lg">
                                <option value="" hidden> -- Pilih Durasi Aktiva -- </option>
                                <option value="12">12 Bulan (1 Thn)</option>
                                <option value="24">24 Bulan (2 Thn)</option>
                                <option value="36">36 Bulan (3 Thn)</option>
                                <option value="48">48 Bulan (4 Thn)</option>
                                <option value="60">60 Bulan (5 Thn)</option>
                                <option value="72">72 Bulan (6 Thn)</option>
                                <option value="84">84 Bulan (7 Thn)</option>
                                <option value="96">96 Bulan (8 Thn)</option>
                                <option value="108">108 Bulan (9 Thn)</option>
                                <option value="120">120 Bulan (10 Thn)</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="penyusutan">Penyusutan</label>
                            <input type="number" class="form-control form-control-lg" name="penyusutan2"
                                id="penyusutan2" placeholder="Penyusutan" readonly>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control form-control-lg" name="keterangan2" id="keterangan" placeholder="Keterangan"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <script src="{{ asset('js/manage_customer/customer/script.js') }}"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript">
        $('#form-add').hide();

        $('#button-add').click(function() {
            $('#form-add').show();
            $('#button-add').hide();
        });

        $('#button-cancel').click(function() {
            $('#form-add').hide();
            $('#button-add').show();
        });

        $(document).ready(function () {
            $('#table-aktiva-lancar').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'colvis',
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            'copy',
                            'excel',
                            'pdf',
                            'print'
                        ]
                    },
                ],
                'lengthMenu': [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
            });
        });

        $('#jenis_aktiva').on('change', function() {
            var jenis_aktiva = $(this).val();
            if (jenis_aktiva == 'Lain-lain') {
                $('#lain-lain').show();
            } else {
                $('#lain-lain').hide();
            }
        });

        $('#lain-lain').hide();

        //get value nominal
        $('#nominal').on('keyup', function() {
            var biaya = $(this).val();
            var durasi = $('#durasi').val();
            var total = parseInt(biaya) / parseInt(durasi);

            $('#penyusutan').val(total.toFixed(0));
        });

        //get value durasi
        $('#durasi').on('change', function() {
            var durasi = $(this).val();
            var biaya = $('#nominal').val();
            var total = parseInt(biaya) / parseInt(durasi);
            $('#penyusutan').val(total.toFixed(0));
        });

        //get value nominal2
        $('#nominal2').on('keyup', function() {
            var biaya = $(this).val();
            var durasi = $('#durasi_aktiva2').val();
            var total = parseInt(biaya) / parseInt(durasi);

            $('#penyusutan2').val(total.toFixed(0));
        });

        //get value durasi2
        $('#durasi_aktiva2').on('change', function() {
            var durasi = $(this).val();
            var biaya = $('#nominal2').val();
            var total = parseInt(biaya) / parseInt(durasi);
            $('#penyusutan2').val(total.toFixed(0));
        });

        $(function(){
            $("#edit-data-tetap").on("show.bs.modal", function(event){
                
                var durasi = $('#durasi_aktiva2').val();
                var biaya = $('#nominal2').val();
                var total = parseInt(biaya) / parseInt(durasi);
                $('#penyusutan2').val(total.toFixed(0));
            });
        });

        @if ($message = Session::get('create_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
        @endif

        @if ($message = Session::get('update_success'))
            swal(
                "Berhasil!",
                "{{ $message }}",
                "success"
            );
        @endif

        @if ($message = Session::get('delete_success'))
            swal(
                "Berhasil!",
                "{{ $message }}",
                "success"
            );
        @endif  

        @if ($message = Session::get('import_success'))
            swal(
                "Berhasil!",
                "{{ $message }}",
                "success"
            );
        @endif

        @if ($message = Session::get('update_failed'))
            swal(
                "",
                "{{ $message }}",
                "error"
            );
        @endif

        @if ($message = Session::get('supply_system_status'))
            swal(
                "",
                "{{ $message }}",
                "success"
            );
        @endif

        $(document).on('click', '.filter-btn', function(e){
            e.preventDefault();
            var data_filter = $(this).attr('data-filter');
            $.ajax({
            method: "GET",
            url: "{{ url('/customer/filter') }}/" + data_filter,
            success:function(data)
            {
                $('tbody').html(data);
            }
            });
        });

        $(document).on('click', '.btn-edit', function(){
            var data_id = $(this).attr('data-id');
            $.ajax({
            method: "GET",
            url: "{{ url('/accounting/aktiva_tetap/edit') }}/" + data_id,
            success:function(response)
            {
                $('input[name=id2]').val(response.aktiva_tetap.id);
                $('select[name=kode2]').val(response.aktiva_tetap.kode);
                $('input[name=tanggal2]').val(response.aktiva_tetap.tanggal);
                $('input[name=kode_nama_akun2]').val(response.aktiva_tetap.kode_nama_akun);
                $('input[name=kode2]').val(response.aktiva_tetap.kode);
                $('select[name=jenis_aktiva2]').val(response.aktiva_tetap.jenis_aktiva);
                $('input[name=jenis_lain2]').val(response.aktiva_tetap.jenis_lain);
                $('select[name=metode_bayar2]').val(response.aktiva_tetap.metode_bayar);
                $('input[name=nominal2]').val(response.aktiva_tetap.nominal);
                $('select[name=durasi_aktiva2]').val(response.aktiva_tetap.durasi_aktiva);
                $('input[name=penyusutan2]').val(response.aktiva_tetap.penyusutan);
                $('textarea[name=keterangan2]').val(response.aktiva_tetap.keterangan);
                validator.resetForm();
            }
            });
        });

        $(document).on('click', '.btn-delete', function(e){
            e.preventDefault();
            var data_id = $(this).attr('data-id');
            var nama = $(this).attr('data-nama');
            swal({
            title: "Apa Anda Yakin?",
            text: "Data Aktiva "+nama+" akan terhapus, klik oke untuk melanjutkan",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                window.open("{{ url('/accounting/aktiva_tetap/delete') }}/" + data_id, "_self");
            }
            });
        });

        var validator = $("form[name='update_form']").validate({
                rules: {
                    nama: "required",
                    kota: "required"
                },
                messages: {
                    nama: "Nama customer tidak boleh kosong",
                    kota: "Kota customer tidak boleh kosong",
                },
                errorPlacement: function(error, element) {
                    var name = element.attr("name");
                    $("#" + name + "_error").html(error);
                },
                submitHandler: function(form) {
                    form.submit();
                }
        });

        //format rupiah
            var rupiah = document.getElementById('rupiah');
            rupiah.addEventListener('keyup', function(e){
                // tambahkan 'Rp.' pada saat form di ketik
                // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
                rupiah.value = formatRupiah(this.value);
            });
            //fungsi format rupiah
            function formatRupiah(angka, prefix){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
            }
    </script>
@endsection
