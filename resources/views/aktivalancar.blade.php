@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="row page-title-header">
        <div class="col-12">
            <div class="page-header d-flex justify-content-between align-items-center">
                <h4 class="page-title h3 mb-0 text-gray-800">Aktiva Lancar</h4>
                <div class="d-flex justify-content-start">
                    <button class="btn btn-primary ml-2" id="button-add">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="form-add">
        <div class="col-12 grid-margin">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    <form action="{{ url('/accounting/aktiva_lancar/add') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="kode">Kode Akun Bank</label>
                            <select name="kode" class="form-control form-control-lg" id="kode_akun" required>
                                @foreach ($akun as $i)
                                    <option value="{{ $i->id }}">{{ $i->kode_akun . ' ' . $i->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nama">Tanggal</label>
                            <input type="date" class="form-control form-control-lg" name="tanggal" id="tanggal"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama Produk</label>
                            <input type="text" class="form-control form-control-lg" name="nama_produk" id="nama"
                                placeholder="Nama" required>
                        </div>
                        <div class="form-group">
                            <label for="jenis">Kategori</label>
                            <select name="jenis_aktiva" id="jenis_aktiva" class="form-control form-control-lg" required>
                                <option value="" hidden> -- Pilih Jenis Aktiva -- </option>
                                <option value="Biaya Bulanan">Biaya bulanan</option>
                                <option value="Pemeliharaan">Pemeliharaan</option>
                                <option value="Servis">Servis</option>
                                <option value="Pajak">Pajak</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                        <div class="form-group" id="lain-lain">
                            <input type="text" class="form-control form-control-lg" name="jenis_lain"
                                placeholder="Jenis Lain-lain">
                        </div>
                        <div class="form-group">
                            <label for="metode_bayar">Metode Bayar</label>
                            <select name="metode_bayar" id="metode_bayar" class="form-control form-control-lg" required>
                                <option value="" hidden> -- Pilih Metode Bayar -- </option>
                                <option value="kas">Kas</option>
                                <option value="bank">Bank</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="biaya">Biaya</label>
                            <input type="number" class="form-control form-control-lg" name="biaya" id="biaya"
                                placeholder="Biaya" required>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control form-control-lg" name="keterangan" id="keterangan" rows="3" required></textarea>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="button" id="button-cancel" class="btn btn-secondary mr-2">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card card-noborder b-radius">
                <div class="card-body">
                    {{-- <div class="row mb-3">
                        <div class="col-6">
                            <input type="text" class="form-control form-control-lg h-100" name="search" placeholder="Cari">
                        </div>
                    </div> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table-aktiva-lancar">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <th>Metode Bayar</th>
                                    <th>Biaya</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($aktiva_lancar as $item)
                                    <tr>
                                        <td>{{ \App\Models\Akun::find($item->kode)->kode_akun }}</td>
                                        <td>{{ Carbon\Carbon::parse($item->tanggal)->format('Y/m/d') }}</td>
                                        <td>{{ $item->nama_produk }}</td>
                                        @if ($item->jenis_aktiva != 'Lain-lain')
                                            <td>{{ $item->jenis_aktiva }}</td>
                                        @else
                                            <td>{{ $item->jenis_lain }}</td>
                                        @endif
                                        <td>{{ $item->metode_bayar }}</td>
                                        <td>Rp. {{ number_format($item->biaya, 2, ',', '.') }}</td>
                                        <td>{{ $item->keterangan }}</td>
                                        <td>
                                            <a href="#" data-target="#edit-aktiva-lancar{{ $item->id }}"
                                                data-toggle="modal" class="btn btn-icons btn-info btn-edit">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="#" data-id="{{ $item->id }}"
                                                data-nama="{{ $item->nama_produk }}"
                                                class="btn btn-icons btn-danger btn-delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal edit data aktiva lancar --}}
    @foreach ($aktiva_lancar as $i => $item)
        <div class="modal fade" id="edit-aktiva-lancar{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Aktiva Lancar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ url('/accounting/aktiva_lancar/update') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            <div class="form-group">
                                <label for="kode">Kode Akun Bank</label>
                                <select name="kode" class="form-control form-control-lg" id="kode_akun" required>
                                    @foreach ($akun as $i)
                                        @if ($i->id == $item->kode)
                                            <option value="{{ $i->id }}" selected>{{ $i->kode_akun . ' ' . $i->nama }}
                                            </option>
                                        @else
                                            <option value="{{ $i->id }}">{{ $i->kode_akun . ' ' . $i->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tanggal">Tanggal
                                    ({{ Carbon\Carbon::parse($item->tanggal)->format('Y/m/d') }})</label>
                                <input type="date" name="tanggal" class="form-control form-control-lg"
                                    id="tanggal">
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama Produk</label>
                                <input type="text" class="form-control form-control-lg" name="nama_produk"
                                    id="nama" placeholder="Nama" value="{{ $item->nama_produk }}">
                            </div>
                            <div class="form-group">
                                <label for="jenis">Jenis Kategori</label>
                                <select name="jenis_aktiva" id="jenis_aktiva{{ $i }}"
                                    class="form-control form-control-lg">
                                    <option value="{{ $item->jenis_aktiva }}" selected hidden>{{ $item->jenis_aktiva }}
                                    </option>
                                    <option value="Biaya Bulanan">Biaya bulanan</option>
                                    <option value="Pemeliharaan">Pemeliharaan</option>
                                    <option value="Servis">Servis</option>
                                    <option value="Pajak">Pajak</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                            </div>
                            <div class="form-group" id="lain-lain{{ $i }}">
                                <input type="text" class="form-control form-control-lg"
                                    id="jenis-lain{{ $i }}" name="jenis_lain" placeholder="Jenis Lain-lain"
                                    value="{{ $item->jenis_lain }}">
                            </div>
                            <div class="form-group">
                                <label for="metode_bayar">Metode Bayar</label>
                                <select name="metode_bayar" id="metode_bayar" class="form-control form-control-lg"
                                    required>
                                    <option value="" hidden> -- Pilih Metode Bayar -- </option>
                                    <option value="kas" @if ($item->metode_bayar == 'kas') selected @endif>Kas</option>
                                    <option value="bank" @if ($item->metode_bayar == 'bank') selected @endif>Bank</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="biaya">Biaya</label>
                                <input type="number" class="form-control form-control-lg" name="biaya" id="biaya"
                                    placeholder="Biaya" value="{{ $item->biaya }}">
                            </div>
                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <textarea class="form-control form-control-lg" name="keterangan" id="keterangan" rows="3">{{ $item->keterangan }}</textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <!-- /.container-fluid -->

    <script src="{{ asset('js/manage_customer/customer/script.js') }}"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript">
        $('#form-add').hide();

        $('#button-add').click(function() {
            $('#form-add').show();
            $('#button-add').hide();
        });

        $('#button-cancel').click(function() {
            $('#form-add').hide();
            $('#button-add').show();
        });

        $(document).ready(function() {
            $('#table-aktiva-lancar').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    'colvis',
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            'copy',
                            'excel',
                            'pdf',
                            'print'
                        ]
                    },
                ],
                'lengthMenu': [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                'order': [
                    [0, 'asc']
                ],
                'deferLoading': 57,
            });
        });


        $('#jenis_aktiva').on('change', function() {
            var jenis_aktiva = $(this).val();
            if (jenis_aktiva == 'Lain-lain') {
                $('#lain-lain').show();
            } else {
                $('#lain-lain').hide();
                $('#jenis-lain').val('');
            }
        });
        $('#lain-lain').hide();

        @if($message = Session::get('create_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
        @endif

        @if($message = Session::get('update_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
        @endif

        @if($message = Session::get('delete_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
        @endif

        @if($message = Session::get('import_success'))
        swal(
            "Berhasil!",
            "{{ $message }}",
            "success"
        );
        @endif

        @if($message = Session::get('update_failed'))
        swal(
            "",
            "{{ $message }}",
            "error"
        );
        @endif

        @if($message = Session::get('supply_system_status'))
        swal(
            "",
            "{{ $message }}",
            "success"
        );
        @endif

        $(document).on('click', '.filter-btn', function(e) {
            e.preventDefault();
            var data_filter = $(this).attr('data-filter');
            $.ajax({
                method: "GET",
                url: "{{ url('/customer/filter') }}/" + data_filter,
                success: function(data) {
                    $('tbody').html(data);
                }
            });
        });

        $(document).on('click', '.btn-edit', function() {
            var data_edit = $(this).attr('data-edit');
            $.ajax({
                method: "GET",
                url: "{{ url('/customer/edit') }}/" + data_edit,
                success: function(response) {
                    $('input[name=id]').val(response.customer.id);
                    $('input[name=nama]').val(response.customer.nama);
                    $('input[name=alamat]').val(response.customer.alamat);
                    $('input[name=nama_bank]').val(response.customer.nama_bank);
                    $('input[name=no_rekening]').val(response.customer.no_rekening);
                    $('input[name=phone]').val(response.customer.phone);
                    $('input[name=kota]').val(response.customer.kota);
                    validator.resetForm();
                }
            });
        });

        $(document).on('click', '.btn-delete', function(e) {
            e.preventDefault();
            var data_id = $(this).attr('data-id');
            var nama = $(this).attr('data-nama');
            swal({
                    title: "Apa Anda Yakin?",
                    text: "Data Aktiva " + nama + " akan terhapus, klik oke untuk melanjutkan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.open("{{ url('/accounting/aktiva_lancar/delete') }}/" + data_id, "_self");
                    }
                });
        });

        var validator = $("form[name='update_form']").validate({
            rules: {
                nama: "required",
                kota: "required"
            },
            messages: {
                nama: "Nama customer tidak boleh kosong",
                kota: "Kota customer tidak boleh kosong",
            },
            errorPlacement: function(error, element) {
                var name = element.attr("name");
                $("#" + name + "_error").html(error);
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        //format rupiah
        var rupiah = document.getElementById('rupiah');
        rupiah.addEventListener('keyup', function(e) {
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah.value = formatRupiah(this.value, 'Rp. ');
        });
        //fungsi format rupiah
        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    </script>
@endsection
