@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if (session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                <h1 class="h3 mb-0 text-gray-800">
                                    Jurnal Umum
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('/accounting/jurnalUmum') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="bukti_transaksi">Bukti Transaksi</label>
                                <input type="text" name="bukti_transaksi" value="{{ $bukti_transaksi }}"
                                    class="form-control form-control-lg" id="bukti_transaksi" required>
                            </div>

                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input type="date" step="1" name="tanggal" class="form-control form-control-lg"
                                    id="tanggal" required>
                            </div>

                            <br>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Kode Akun</th>
                                        <th>Keterangan</th>
                                        <th>Debit</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body">
                                    <tr>
                                        <td><select name='kode_akun_debit[]' class='form-control form-control-sm'
                                                id='kode_akun' required>
                                                @foreach ($akun as $item)
                                                    <option value='{{ $item->id }}'>
                                                        {{ $item->kode_akun . ' ' . $item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input class='form-control form-control-sm' type='text'
                                                name='keterangan_debit[]' /></td>
                                        <td><input class='form-control form-control-sm' type='number'
                                                name='saldo_debit[]' /></td>
                                    </tr>
                                </tbody>

                                <table>
                                    <br>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-danger mr-3" id="button-hapus">Hapus</button>
                                        <button type="button" class="btn btn-success mr-3"
                                            id="button-tambahkan">Tambahkan</button>
                                    </div>

                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Kode Akun</th>
                                                <th>Keterangan</th>
                                                <th>Kredit</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table-body2">
                                            <tr>
                                                <td><select name='kode_akun_kredit[]' class='form-control form-control-sm'
                                                        id='kode_akun' required>
                                                        @foreach ($akun as $item)
                                                            <option value='{{ $item->id }}'>
                                                                {{ $item->kode_akun . ' ' . $item->nama }}</option>
                                                        @endforeach
                                                    </select></td>
                                                <td><input class='form-control form-control-sm' type='text'
                                                        name='keterangan_kredit[]' /></td>
                                                <td><input class='form-control form-control-sm' type='number'
                                                        name='saldo_kredit[]' /></td>
                                            </tr>
                                        </tbody>
                                        <table>
                                            <br>
                                            <div class="form-group">
                                                <button type="button" class="btn btn-danger mr-3"
                                                    id="button-hapus2">Hapus</button>
                                                <button type="button" class="btn btn-success mr-3"
                                                    id="button-tambahkan2">Tambahkan</button>
                                            </div>

                                            <div class="d-flex justify-content-end">
                                                <button type="button" class="btn btn-secondary mr-3"
                                                    id="button-cancel">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </table>
                                    </table>
                                </table>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script>
        $("#button-tambahkan").click(function() {
            $("#table-body").append("<tr><td><select name='kode_akun_debit[]' class='form-control form-control-sm' id='kode_akun' required>@foreach ($akun as $item)<option value='{{ $item->id}}'>{{ $item->kode_akun.' '.$item->nama }}</option>@endforeach</select></td><td><input class='form-control form-control-sm' type='text' name='keterangan_debit[]' /></td><td><input class='form-control form-control-sm' type='number' name='saldo_debit[]' /></td></tr>")
        });

        $("#button-hapus").click(function() {
            $("#table-body").children().last().remove();
        });

        $("#button-tambahkan2").click(function() {
            $("#table-body2").append("<tr><td><select name='kode_akun_kredit[]' class='form-control form-control-sm' id='kode_akun' required>@foreach ($akun as $item)<option value='{{ $item->id}}'>{{ $item->kode_akun.' '.$item->nama }}</option>@endforeach</select></td><td><input class='form-control form-control-sm' type='text' name='keterangan_kredit[]' /></td><td><input class='form-control form-control-sm' type='number' name='saldo_kredit[]' /></td></tr>")
        });

        $("#button-hapus2").click(function() {
            $("#table-body2").children().last().remove();
        });
    </script>
@endsection
