@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="h3 mb-0 text-gray-800">
                                    Bank Masuk
                                </h4>
                                {{ $startDate->format('Y-m-d') }} - {{ $endDate->format('Y-m-d') }}
                            </div>
                            <div class="col-md-6 justify-content-end">
                                <form action="{{ url('/accounting/bankMasuk') }}" method="get">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <input type="date" name="start_date"
                                                    value="{{ request()->input('start_date') }}" class="form-control">
                                            </div>
                                        </div>
                                        _
                                        <div class="col">
                                            <div class="form-group">
                                                <input type="date" name="end_date"
                                                    value="{{ request()->input('end_date') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <select name="jenis" class="form-control form-control-lg" id="jenis">
                                                    <option value="all"
                                                        @if ($transaction_type == 'all') selected @endif>Semua</option>
                                                    <option value="1"
                                                        @if ($transaction_type == 1) selected @endif>CV Anzac Food
                                                    </option>
                                                    <option value="2"
                                                        @if ($transaction_type == 2) selected @endif>Sumber Mulia
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-1 d-flex justify-content-end">
                                <a href="{{ url('/accounting/bankMasuk/add') }}"
                                    class="btn btn-primary ml-2" id="button-add">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kas_in">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>No Transaksi</th>
                                        <th>Keterangan</th>
                                        <th>Total</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total_jumah = 0;
                                    @endphp
                                    @foreach ($bank_in as $item)
                                        <tr>
                                            <td>{{ date('Y-m-d', strtotime($item->tanggal)) }}</td>
                                            <td>{{ $item->bukti_transaksi }}</td>
                                            <td>{{ $item->keterangan }}</td>
                                            <td>Rp. {{ number_format($item->debit, 0, ',', '.') }}</td>
                                            <td>
                                                <a class='btn btn-info btn-detail mr-4 btn-view-detail'
                                                    data-toggle="modal" data-target="#view-detail"
                                                    data-view="{{ $item->id }}"><i class="fa fa-eye"></i></a>
                                                <a class='btn btn-primary btn-detail mr-4'
                                                    href="{{ url('/accounting/bankMasuk/edit/' . $item->id) }}"><i
                                                        class="fas fa-edit"></i></a>
                                                <a class='btn btn-danger btn-detail mr-4'
                                                    data-delete="{{ $item->id }}" href="#"><i
                                                        class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        @php
                                            $total_jumah += $item->debit;
                                        @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>&nbsp;</td>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th class="text-right">Total</th>
                                        <th>Rp. {{ number_format($total_jumah, 0, ',', '.') }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="view-detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">View</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>Perusahaan</td>
                            <td>:</td>
                            <td id="vd-perusahaan"></td>
                        </tr>
                        <tr>
                            <td>Bukti Transaksi</td>
                            <td>:</td>
                            <td id="vd-bukti"></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td id="vd-tanggal"></td>
                        </tr>
                    </table>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Kode Akun</th>
                                    <th>Keterangan</th>
                                    <th>Saldo</th>
                                </tr>
                            </thead>
                            <tbody id="vd-jurnals"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script>
        $(document).ready(function() {
            $('#kas_in').DataTable({
                dom: 'lBfrtip',
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                buttons: [
                    'colvis',
                    {
                        extend: 'collection',
                        text: 'Export',
                        buttons: [{
                                extend: 'print',
                                footer: true
                            },
                            {
                                extend: 'copyHtml5',
                                footer: true
                            },
                            {
                                extend: 'excelHtml5',
                                footer: true
                            },
                            {
                                extend: 'pdfHtml5',
                                footer: true
                            }
                        ]
                    },
                ],
                processing: true,
            });
        });

        $(document).on('click', '.btn-delete', function(e) {
            e.preventDefault();
            var data_delete = $(this).attr('data-delete');
            swal({
                    title: "Apa Anda Yakin?",
                    text: "Data bank masuk akan terhapus, klik oke untuk melanjutkan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.open("{{ url('/accounting/bank_in/delete') }}/" + data_delete, "_self");
                    }
                });
        });
        
        function numberWithCommas(x) {
            if (x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                return parts[0];
            }
            return x;
        }

        $(document).on('click', '.btn-view-detail', function() {
            var data_view = $(this).attr('data-view');
            $.ajax({
                method: "GET",
                url: "{{ url('/accounting/view_detail_jurnal') }}/" + data_view,
                success:function(response)
                {
                    var jurnal = response.jurnal;
                    var jurnals = response.jurnals;
                    $("#vd-perusahaan").html(jurnal.kode_akun.substr(jurnal.kode_akun.length - 1) == "A"
                        ? "CV.Anzac Food" : "Sumber Mulia");
                    $("#vd-bukti").html(jurnal.bukti_transaksi);
                    $("#vd-tanggal").html(jurnal.tanggal);
                    var total_jurnals = 0;
                    var body_jurnals = '';
                    jurnals.forEach(element => {
                        body_jurnals += `<tr>`;
                        body_jurnals += `<td>`+element.kode_akun.kode_akun + ' ' + element.kode_akun.nama +`</td>`;
                        body_jurnals += `<td>`+element.keterangan+`</td>`;
                        body_jurnals += `<td>Rp. `+numberWithCommas(element.kredit)+`</td>`;
                        body_jurnals += `</tr>`;
                        total_jurnals += parseFloat(element.kredit);
                    });
                    body_jurnals += `<tr><td colspan="2"><b>Total</b></td><td><b>Rp. `+numberWithCommas(total_jurnals)+`</td></tr>`;
                    $("#vd-jurnals").html(body_jurnals);
                }
            });
        })
    </script>
@endsection
