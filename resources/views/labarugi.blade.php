@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-noborder b-radius">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-4">
                                @php
                                    $thn = $years;
                                    $namabln = $bln;
                                    $bln = $month;
                                    $fbulan = Carbon\Carbon::parse($thn . '-' . $month)->format('Y-m');
                                @endphp
                                <h4>
                                    Laba Rugi {{ $thn }}
                                </h4>
                                @if (isset($bln))
                                    <p>Periode : Bulan {{ $namabln }} Tanggal 01-31</p>
                                @endif
                            </div>
                            <div class="col-6">
                                <form action="{{ url('/accounting/laba_rugi/filter') }}" method="get">
                                    @csrf
                                    <div class="row">
                                        <div class="col">
                                            <select class="form-control form-control-lg" name="comparison">
                                                <option value="now" @if (request()->input('comparison') == 'now') selected @endif>
                                                    Sekarang</option>
                                                <option value="last_month"
                                                    @if (request()->input('comparison') == 'last_month') selected @endif>Bulan Lalu</option>
                                                <option value="current_year"
                                                    @if (request()->input('comparison') == 'current_year') selected @endif>Tahun Berjalan
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <input type="month" value="{{ $fbulan }}" name="bulan" id="bulan"
                                                class="form-control form-control-lg" required>
                                        </div>
                                        <div class="col">
                                            <select class="form-control form-control-lg" id="transaction_type"
                                                name="transaction_type">
                                                <option value="1" @if (request()->input('transaction_type') == '1') selected @endif>PPN
                                                </option>
                                                <option value="2" @if (request()->input('transaction_type') == '2') selected @endif>Non
                                                    PPN</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-1">
                                <form action="{{ url('/accounting/laba_rugi/pdf') }}" target="_blank" method="get">
                                    @csrf
                                    <input type="hidden" name="comparison" value="{{ request()->input('comparison') }}">
                                    <input type="hidden" name="type" value="pdf">
                                    <input type="month" name="bulan" hidden id="bulan3" value="{{ $fbulan }}"
                                        class="form-control">
                                    <div class="form-group">
                                        <button class="btn btn-danger" id="export" type="submit">
                                            <i class="mdi mdi-file-pdf"></i> PDF
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-1">
                                <form action="{{ url('/accounting/laba_rugi/excel') }}" target="_blank" method="get">
                                    @csrf
                                    <input type="hidden" name="comparison" value="{{ request()->input('comparison') }}">
                                    <input type="hidden" name="type" value="export">
                                    <input type="month" name="bulan" hidden id="bulan3" value="{{ $fbulan }}"
                                        class="form-control">
                                    <div class="form-group">
                                        <button class="btn btn-success" id="export" type="submit">
                                            <i class="mdi mdi-file-excel"></i> Excel
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="text-align:center" colspan="2">Bulan</th>
                                        @foreach ($comparisons as $comparison)
                                            <th style="text-align:center" colspan="3">{{ $comparison->format('F') }}
                                            </th>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th style="text-align:center">Kode</th>
                                        <th style="text-align:center">Keterangan</th>
                                        @foreach ($comparisons as $comparison)
                                            <th style="text-align:center">Debit</th>
                                            <th style="text-align:center">Kredit</th>
                                            <th style="text-align:center">Saldo</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($labarugis[0]['labarugi'] as $key => $lr)
                                        <tr>
                                            <td></td>
                                            <th colspan="4">{{ $key }}</th>
                                        </tr>
                                        @foreach ($lr as $keyAkun => $akun)
                                            <tr>
                                                <td>{{ $akun['kode_akun'] }}</td>
                                                <td>{{ $akun['nama_akun'] }}</td>
                                                @foreach ($comparisons as $idxSaldo => $comparison)
                                                    @php $akun_comparison = $labarugis[$idxSaldo]['labarugi'][$key][$keyAkun]; @endphp
                                                    <td>{{ $akun['saldo_normal'] == 'debit' ? 'Rp. ' . number_format($akun_comparison['saldo_debit'] ?? 0, 0, ',', '.') : '' }}
                                                    </td>
                                                    <td>{{ $akun['saldo_normal'] == 'kredit' ? 'Rp. ' . number_format($akun_comparison['saldo_kredit'] ?? 0, 0, ',', '.') : '' }}
                                                    </td>
                                                    <td></td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <th></th>
                                            <th>Total {{ $key }}</th>
                                            @foreach ($comparisons as $idxSaldo => $comparison)
                                                @php $akun_comparison = $labarugis[$idxSaldo]['labarugi'][$key]; @endphp
                                                <th></th>
                                                <th></th>
                                                <th>{{ number_format(collect($akun_comparison)->sum('saldo_total'), 0, ',', '.') }}
                                                </th>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <th></th>
                                        <th>Laba/Rugi</th>
                                        @foreach ($comparisons as $idxSaldo => $comparison)
                                            @php $akun_comparison = $labarugis[$idxSaldo]['labarugi']; @endphp
                                            <th></th>
                                            <th></th>
                                            <th>Rp.
                                                {{ number_format(collect($akun_comparison['Pendapatan'] ?? [])->sum('saldo_total') - collect($akun_comparison['Beban'] ?? [])->sum('saldo_total'), 0, ',', '.') }}
                                            </th>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script>
        $('#laba_rugi').DataTable({
            dom: 'lBfrtip',
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            buttons: [
                'colvis',
                { 
                    extend: 'print',
                    footer: true 
                },
                { 
                    extend: 'copyHtml5',
                    footer: true 
                },
                { 
                    extend: 'excelHtml5',
                    footer: true 
                },
                { 
                    extend: 'pdfHtml5',
                    footer: true 
                }
            ],
        });
        
    </script>
@endsection
