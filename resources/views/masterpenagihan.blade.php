@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Form Penagihan / Invoicing</h1>
        </div>

        <div class="card">
            <div class="card-header">
                <a href={{ url('tambahpenagihan') }}>
                    <button type="button" class="btn btn-primary my-auto">
                        Tambah Data
                    </button>
                </a>
            </div>
            <div class="card-body">
            <table id="tabelPenagihan" class="table table-bordered table-no-wrap table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>ID Penjualan</th>
                        <th>Customer</th>
                        <th>Jenis Box</th>
                        <th>Quantity</th>
                        <th>Nominal</th>
                        <th>Penerimaan</th>
                        <th>Aksi</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($penagihan as $prm)
                        <tr>
                            <td>{{ $prm->id_penjualan }}</td>
                            <td>{{ $prm->pic }}</td>
                            <td>{{ $prm->jenis_box }}</td>
                            <td>{{ $prm->qty }}</td>
                            <td>{{ $prm->nominal }}</td>
                            <td>
                                <form method="post" action="{{ url('/accPenagihan') }}" style="display: inline-block;">
                                    @csrf
                                    <input type="text" name="id_penagihan" hidden value="{{ $prm->id_penagihan }}">
                                    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i></button>
                                </form>
                                <form method="post" action="{{ url('/declinePenagihan') }}" style="display: inline-block;">
                                    @csrf
                                    <input type="text" name="id_penagihan" hidden value="{{ $prm->id_penagihan }}">
                                    <button type="submit" class="btn btn-danger"><i class="fas fa-times"></i></button>
                                </form>
                            </td>
                            <td>
                                {{-- <button type="button" class="btn btn-warning"><i class="fas fa-edit"></i></button> --}}
                                <form method="post" class="d-inline-block" action="{{ url('masterpenagihan/delete/' . $prm->id_penagihan) }}">
                                    @csrf
                                    <button onclick="return confirm('Apakah yakin ingin dihapus?')" type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                            @if ($prm->status_penagihan == '1')
                                <td>Diterima</td>
                            @else
                                <td>Ditolak</td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>

    </div>
    <!-- /.container-fluid -->
    @endsection