<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BoxController;
use App\Http\Controllers\SPKController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DesainController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\PenawaranController;
use App\Http\Controllers\BarangJadiController;
use App\Http\Controllers\PembayaranController;
use App\Http\Controllers\PengirimanController;
use App\Http\Controllers\stokbarangController;
use App\Http\Controllers\controllerMasukKeluarStok;
use App\Http\Controllers\GeneralLedgerController;
use App\Http\Controllers\PembelianBarangcontroller;
use App\Http\Controllers\PenagihanController;
use App\Http\Controllers\PenjualanStokJadiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', [LoginController::class, "login"])->name('login');
Route::post('/login', [LoginController::class, "authenticate"]);
Route::get('/logout', [LoginController::class, "logout"]);

Route::get('/', function () {
    return view('login');
})->name('login');

Route::group([
    'middleware' => 'auth'

], function() {
    
    Route::get('/dashboard', function () {
        return view('layouts.master');
    });

    Route::get('/masterpegawai', [PegawaiController::class, "show"]);
    Route::post('/doAddpegawai', [PegawaiController::class, "doAdd"]);
    Route::post('/masterpegawai/delete/{id}', [PegawaiController::class, "delete"]);
    Route::post('/pegawai/import_excel', [PegawaiController::class, "importExcel"]);
    
    Route::get('/masterbox', [BoxController::class, "show"]);
    Route::post('/doAddbox', [BoxController::class, "doAdd"]);
    Route::post('/masterbox/delete/{id}', [BoxController::class, "delete"]);
    Route::post('/box/import_excel', [BoxController::class, "importExcel"]);
    
    Route::get('/mastercustomer', [CustomerController::class, "show"]);
    Route::post('/doAddcustomer', [CustomerController::class, "doAdd"]);
    Route::post('/mastercustomer/delete/{id}', [CustomerController::class, "delete"]);
    Route::post('/customer/import_excel', [CustomerController::class, "importExcel"]);
    
    Route::get('/mastersupplier', [SupplierController::class, "show"]);
    Route::post('/doAddsupplier', [SupplierController::class, "doAdd"]);
    Route::post('/mastersupplier/delete/{id}', [SupplierController::class, "delete"]);
    Route::post('/supplier/import_excel', [SupplierController::class, "importExcel"]);
    
    Route::get('/mastervendor', [VendorController::class, "show"]);
    Route::post('/doAddvendor', [VendorController::class, "doAdd"]);
    Route::post('/mastervendor/delete/{id}', [VendorController::class, "delete"]);
    Route::post('/vendor/import_excel', [VendorController::class, "importExcel"]);
    
    
    Route::get('/masterpenawaran', [PenawaranController::class, "show"]);
    Route::get('/tambahpenawaran', [PenawaranController::class, "showBrand"]);
    Route::post('/masterpenawaran/delete/{id}', [PenawaranController::class, "delete"]);
    Route::post('/doAddpenawaran', [PenawaranController::class, "doAddpenawaran"]);
    Route::post('/accPenawaran', [PenawaranController::class, "accPenawaran"]);
    Route::post('/declinePenawaran', [PenawaranController::class, "declinePenawaran"]);
    

    Route::get('/stokbarangjadi', [BarangJadiController::class, 'index']);
    Route::get('/masterbarangjadi', [BarangJadiController::class, 'index2']);
    Route::get('/tambahbarangjadi', [BarangJadiController::class, "tambahBarangJadi"]);
    Route::post('/doAddPembuatanBarangJadi', [BarangJadiController::class, "doAddPembuatanBarangJadi"]);
    Route::post('/stokbarangjadi/delete/{id}', [BarangJadiController::class, "delete"]);
    Route::get('/ubahbarangjadi/edit/{id}', [BarangJadiController::class, "ubahBarangJadi"]);
    Route::post('/ubahbarangjadi/edit/{id}', [BarangJadiController::class, "doEditPembuatanBarangJadi"]);
    

    Route::get('/masterpenjualanstokjadi', [PenjualanStokJadiController::class, "show"]);
    Route::get('/tambahpenjualanbarangjadi', [PenjualanStokJadiController::class, "create"]);
    Route::post('/doAddpenjualanbarangjadi', [PenjualanStokJadiController::class, "doAddpenjualanbarangjadi"]);
    Route::get('/masterpenjualan/edit/{id}', [PenjualanStokJadiController::class, "edit"]);
    Route::post('/masterpenjualan/edit/{id}', [PenjualanStokJadiController::class, "doEditpenjualanbarangjadi"]);
    Route::post('/masterpenjualan/delete/{id}', [PenjualanStokJadiController::class, "delete"]);
    Route::post('/accPenjualan', [PenjualanStokJadiController::class, "accPenjualan"]);
    Route::post('/declinePenjualan', [PenjualanStokJadiController::class, "declinePenjualan"]);
    

    Route::get('/formdp', [PembayaranController::class, "show"]);
    Route::get('/tambahdp', [PembayaranController::class, "showBrand"]);
    Route::get('/formdp/edit/{id}', [PembayaranController::class, "edit"]);
    Route::post('/formdp/edit/{id}', [PembayaranController::class, "doEditpembayaran"]);
    Route::post('/formdp/delete/{id}', [PembayaranController::class, "delete"]);
    Route::post('/doAddpembayaran', [PembayaranController::class, "doAddpembayaran"]);
    Route::post('/accPembayaran', [PembayaranController::class, "AccPembayaran"]);
    Route::post('/declinePembayaran', [PembayaranController::class, "declinePembayaran"]);
    Route::get('cekPenawaranDP/{id_penawaran}', [SPKController::class, 'cekPenawaran']);
    
    // Route::get('/formdesain', [DesainController::class, "show"]);
    // Route::get('/tambahdesain', [DesainController::class, "showBrand"]);
    // Route::post('/formdesain/delete/{id}', [DesainController::class, "delete"]);
    // Route::post('/doAddDesain', [DesainController::class, "doAddDesain"]);
    // Route::post('/accDesain', [DesainController::class, "AccDesain"]);
    // Route::post('/declineDesain', [DesainController::class, "declineDesain"]);
    
    Route::get('/mastersuratperintahkerja', [SPKController::class, "index"]);
    Route::get('/suratperintahkerja', [SPKController::class, "create"]);
    Route::post('/doAddSPK', [SPKController::class, "doAddSPK"]);
    Route::post('/formSPK/delete/{id}', [SPKController::class, "delete"]);
    Route::get('cekKodePembuatanSPK/{kode_pembuatan}', [SPKController::class, 'cekKodePembuatan']);
    Route::get('cekPenawaranSPK/{id_penawaran}', [SPKController::class, 'cekPenawaran']);

    Route::get('cekProcessing1/{no_spk}', [SPKController::class, 'cekProcessing1']);
    Route::get('cekProcessingBarangJadi1/{no_spk}', [SPKController::class, 'cekProcessingBarangJadi1']);
    Route::get('cekProcessing2/{no_spk}', [SPKController::class, 'cekProcessing2']);
    Route::get('cekProcessingBarangJadi2/{no_spk}', [SPKController::class, 'cekProcessingBarangJadi2']);
    
    Route::get('editProcessing1/{id_proses1}', [SPKController::class, 'editProcessing1']);
    Route::post('editProcessing1/{id_proses1}', [SPKController::class, 'simpanProcessing1']);
    Route::get('editBarangJadi1/{id_proses_barang_jadi}', [SPKController::class, 'editBarangJadi1']);
    Route::post('editBarangJadi1/{id_proses_barang_jadi}', [SPKController::class, 'simpanBarangJadi1']);

    Route::get('editProcessing2/{id_proses2}', [SPKController::class, 'editProcessing2']);
    Route::post('editProcessing2/{id_proses2}', [SPKController::class, 'simpanProcessing2']);
    Route::get('editBarangJadi2/{id_proses_barang_jadi}', [SPKController::class, 'editBarangJadi2']);
    Route::post('editBarangJadi2/{id_proses_barang_jadi}', [SPKController::class, 'simpanBarangJadi2']);
    

    Route::get('/suratjalan', [PengirimanController::class, "show"]);
    Route::get('/tambahpengiriman', [PengirimanController::class, "showSPK"]);
    Route::post('/tambahPengiriman', [PengirimanController::class, "doAddPengiriman"]);
    Route::get('/suratjalan/edit/{id}', [PengirimanController::class, "edit"]);
    Route::post('/suratjalan/edit/{id}', [PengirimanController::class, "doEditPengiriman"]);
    Route::post('/suratjalan/delete/{id}', [PengirimanController::class, "delete"]);
    Route::post('/accPengiriman', [PengirimanController::class, "accPengiriman"]);
    Route::post('/declinePengiriman', [PengirimanController::class, "declinePengiriman"]);


    Route::get('/penagihan', [PenagihanController::class, 'index']);
    Route::get('/tambahpenagihan', [PenagihanController::class, "create"]);
    Route::post('/doAddpenagihan', [PenagihanController::class, "doAddpenagihan"]);
    Route::post('/masterpenagihan/delete/{id}', [PenagihanController::class, "delete"]);
    Route::post('/accPenagihan', [PenagihanController::class, "accPenagihan"]);
    Route::post('/declinePenagihan', [PenagihanController::class, "declinePenagihan"]);
    Route::get('cekKodePembuatanTagih/{kode_pembuatan}', [PenagihanController::class, 'cekKodePembuatan']);
    Route::get('cekPenawaranTagih/{id_penawaran}', [PenagihanController::class, 'cekPenawaran']);
    
    
    Route::get('/mastersubcon', function () {
        return view('mastersubcon');
    });
    Route::get('/masterstok', function () {
        return view('masterstok');
    });
    Route::get('/kartustok', function () {
        return view('kartustok');
    });
    Route::get('/formprocess', function () {
        return view('formprocess');
    });
    Route::get('/formpo', function () {
        return view('formpo');
    });
    
    
    Route::get('/history', function () {
        return view('history');
    });
    
    
    Route::get('/home', function () {
        return view('home');
    });
    // Form Tambah Data
    Route::get('/tambahpegawai', function () {
        return view('tambahpegawai');
    });
    Route::get('/tambahcustomer', function () {
        return view('tambahcustomer');
    });
    Route::get('/tambahsupplier', function () {
        return view('tambahsupplier');
    });
    Route::get('/tambahbox', function () {
        return view('tambahbox');
    });
    Route::get('/tambahvendor', function () {
        return view('tambahvendor');
    });
    
    
    Route::get('/tambahpembelian', [PembelianBarangcontroller::class, "show"]);
    // Route::get('/tambahpembelian', function () {
    //     return view('tambahpembelian');
    // });
    Route::post('/doPembelianbarang', [PembelianBarangcontroller::class, "doAdd"]);
    Route::get('/pembelianbarang', [PembelianBarangcontroller::class, "showpembelian"]);
    Route::post('/pembelianbarang/delete/{id}', [PembelianBarangcontroller::class, "delete"]);
    // Route::get('/pembelianbarang', function () {
    //     return view('pembelianbarang');
    // });
    Route::get('/arusbarang', [PembelianBarangcontroller::class, "showarus"]);
    // Route::get('/arusbarang', function () {
    //     return view('arusbarang');
    // });
    
    Route::get('/barangmasuk', [controllerMasukKeluarStok::class, "showmasuk"]);
    Route::post('/dobarangmasuk', [controllerMasukKeluarStok::class, "doaddmasuk"]);
    Route::get('/barangkeluar', [controllerMasukKeluarStok::class, "showkeluar"]);
    Route::post('/dobarangkeluar', [controllerMasukKeluarStok::class, "doaddkeluar"]);
    // Route::get('/barangmasuk', function () {
    //     return view('barangmasuk');
    // });
    // Route::get('/barangkeluar', function () {
    //     return view('barangkeluar');
    // });
    
    
    Route::get('/stokbarang', [stokbarangController::class, "showstok"]);
    // Route::get('/stokbarang', function () {
    //     return view('stokbarang');
    // });
    
    Route::get('/laporanlogin', function () {
        return view('laporanlogin');
    });
    Route::get('/laporankeuangan', function () {
        return view('laporankeuangan');
    });
    Route::get('/laporantransaksi', function () {
        return view('laporantransaksi');
    });


    // General Ledger
    Route::get('accounting/jurnalUmum', [GeneralLedgerController::class, 'viewJurnalUmum']);
    Route::post('accounting/jurnalUmum', [GeneralLedgerController::class, 'addJurnalUmum']);


    Route::get('accounting/akun', [GeneralLedgerController::class, 'viewAkun']);
    Route::post('accounting/akun/add', [GeneralLedgerController::class, 'storeAkun']);
    Route::post('accounting/akun/update', [GeneralLedgerController::class, 'updateAkun']);
    Route::get('accounting/akun/delete/{id}', [GeneralLedgerController::class, 'deleteAkun']);
    Route::post('accounting/akun/duplicate', [GeneralLedgerController::class, 'duplicateAkun']);
    Route::get('accounting/akun/data', [GeneralLedgerController::class, 'dataAkun']);


    Route::get('/accounting/aktiva_lancar', [GeneralLedgerController::class, 'viewAktivaLancar']);
    Route::post('/accounting/aktiva_lancar/add', [GeneralLedgerController::class, 'addAktivaLancar']);
    Route::get('/accounting/aktiva_lancar/delete/{id}', [GeneralLedgerController::class, 'deleteAktivaLancar']);
    Route::post('/accounting/aktiva_lancar/update', [GeneralLedgerController::class, 'updateAktivaLancar']);

    Route::get('/accounting/aktiva_tetap', [GeneralLedgerController::class, 'viewAktivaTetap']);
    Route::post('/accounting/aktiva_tetap/add', [GeneralLedgerController::class, 'addAktivaTetap']);
    Route::get('/accounting/aktiva_tetap/delete/{id}', [GeneralLedgerController::class, 'deleteAktivaTetap']);
    Route::post('/accounting/aktiva_tetap/update', [GeneralLedgerController::class, 'updateAktivaTetap']);
    Route::get('/accounting/aktiva_tetap/edit/{id}', [GeneralLedgerController::class, 'editAktivaTetap']);

    Route::get('/accounting/laporan_aktiva', [GeneralLedgerController::class, 'viewLaporanAktiva']);
    Route::post('/accounting/laporan_aktiva/printexcel', [GeneralLedgerController::class, 'excelPrintLaporanAktiva']);


    Route::get('/accounting/bankMasuk', [GeneralLedgerController::class, 'viewBankIn']);
    Route::get('/accounting/bankKeluar', [GeneralLedgerController::class, 'viewBankOut']);
    Route::get('/accounting/bankMasuk/add', [GeneralLedgerController::class, 'addBankIn']);
    Route::get('/accounting/bankKeluar/add', [GeneralLedgerController::class, 'addBankOut']);
    Route::get('/accounting/bankMasuk/edit/{id}', [GeneralLedgerController::class, 'viewEditBankIn']);
    Route::get('/accounting/bankKeluar/edit/{id}', [GeneralLedgerController::class, 'viewEditBankOut']);
    Route::post('/accounting/bankMasuk/process', [GeneralLedgerController::class, 'processBankIn']);
    Route::post('/accounting/bankKeluar/process', [GeneralLedgerController::class, 'processBankOut']);
    Route::post('/accounting/bankMasuk/process_edit', [GeneralLedgerController::class, 'processEditBankIn']);
    Route::post('/accounting/bankKeluar/process_edit', [GeneralLedgerController::class, 'processEditBankOut']);
    Route::get('/accounting/bankMasuk/delete/{id}', [GeneralLedgerController::class, 'deleteBankIn']);
    Route::get('/accounting/bankKeluar/delete/{id}', [GeneralLedgerController::class, 'deleteBankOut']);


    Route::get('/accounting/view_detail_jurnal/{id}', [GeneralLedgerController::class, 'viewDetailJurnal']);


    Route::get('/accounting/kasMasuk', [GeneralLedgerController::class, 'viewKasIn']);
    Route::get('/accounting/kasKeluar', [GeneralLedgerController::class, 'viewKasOut']);
    Route::get('/accounting/kasMasuk/add', [GeneralLedgerController::class, 'addKasIn']);
    Route::get('/accounting/kasKeluar/add', [GeneralLedgerController::class, 'addKasOut']);
    Route::get('/accounting/kasMasuk/edit/{id}', [GeneralLedgerController::class, 'viewEditKasIn']);
    Route::get('/accounting/kasKeluar/edit/{id}', [GeneralLedgerController::class, 'viewEditKasOut']);
    Route::post('/accounting/kasMasuk/process', [GeneralLedgerController::class, 'processKasIn']);
    Route::post('/accounting/kasKeluar/process', [GeneralLedgerController::class, 'processKasOut']);
    Route::post('/accounting/kasMasuk/process_edit', [GeneralLedgerController::class, 'processEditKasIn']);
    Route::post('/accounting/kasKeluar/process_edit', [GeneralLedgerController::class, 'processEditKasOut']);
    Route::get('/accounting/kasMasuk/delete/{id}', [GeneralLedgerController::class, 'deleteKasIn']);
    Route::get('/accounting/kasKeluar/delete/{id}', [GeneralLedgerController::class, 'deleteKasOut']);



    Route::get('/accounting/buku_besar', [GeneralLedgerController::class, 'viewBukuBesar']);
    Route::post('/accounting/buku_besar/view', [GeneralLedgerController::class, 'viewAkunBukuBesar']);
    Route::post('/accounting/buku_besar/view/filter', [GeneralLedgerController::class, 'viewAkunBukuBesar']);
    Route::get('/accounting/buku_besar/filter', [GeneralLedgerController::class, 'viewBukuBesar']);
    Route::get('/accounting/buku_besar/export', [GeneralLedgerController::class, 'viewBukuBesar']);
    Route::get('/accounting/buku_besar/pdf', [GeneralLedgerController::class, 'viewBukuBesar']);
    Route::get('/accounting/buku_besar/tahun', [GeneralLedgerController::class, 'viewBukuBesar']);


    Route::get('/accounting/laba_rugi', [GeneralLedgerController::class, 'viewLabaRugi']);
    Route::get('/accounting/laba_rugi/filter', [GeneralLedgerController::class, 'viewLabaRugi']);
    Route::get('/accounting/laba_rugi/excel', [GeneralLedgerController::class, 'viewLabaRugi']);
    Route::get('/accounting/laba_rugi/pdf', [GeneralLedgerController::class, 'viewLabaRugi']);


    Route::get('/accounting/neraca', [GeneralLedgerController::class, 'viewNeraca']);
    Route::get('/accounting/neraca/excel', [GeneralLedgerController::class, 'viewNeraca']);
    Route::get('/accounting/neraca/pdf', [GeneralLedgerController::class, 'viewNeraca']);
    Route::get('/accounting/neraca/filter/', [GeneralLedgerController::class, 'viewNeraca']);


    Route::get('/accounting/jurnal_harian', [GeneralLedgerController::class, 'viewJurnalHarian']);
    Route::get('/accounting/jurnal_harian/filter', [GeneralLedgerController::class, 'viewJurnalHarian']);
    Route::get('/accounting/jurnal_harian/export', [GeneralLedgerController::class, 'viewJurnalHarian']);
    Route::get('/accounting/jurnal_harian/pdf', [GeneralLedgerController::class, 'viewJurnalHarian']);
    Route::get('/accounting/jurnal_harian/single', [GeneralLedgerController::class, 'viewJurnalHarianSingle']);
});

