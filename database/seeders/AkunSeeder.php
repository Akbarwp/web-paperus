<?php

namespace Database\Seeders;

use App\Models\Akun;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $akun = [
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1100,
                'nama' => "Aktiva Lancar",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1110,
                'nama' => "Kas",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1120,
                'nama' => "BCA Anzac Food",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1130,
                'nama' => "Piutang Dagang",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1140,
                'nama' => "Cadangan Kerugian",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1150,
                'nama' => "Piutang Karyawan",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1160,
                'nama' => "Persediaan Barang",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1170,
                'nama' => "Sewa di Bayar Dimuka",
            ],
            [
                'tipe_akun' => "Aktiva Lancar",
                'kode_akun' => 1180,
                'nama' => "Uang Muka Pembelian",
            ],
            [
                'tipe_akun' => "Aktiva Tetap",
                'kode_akun' => 1200,
                'nama' => "Aktiva Tetap",
            ],
            [
                'tipe_akun' => "Aktiva Tetap",
                'kode_akun' => 1220,
                'nama' => "Kendaraan",
            ],
            [
                'tipe_akun' => "Aktiva Tetap",
                'kode_akun' => 1221,
                'nama' => "Akumulasi Penyusutan Kendaraan",
            ],
            [
                'tipe_akun' => "Aktiva Tetap",
                'kode_akun' => 1230,
                'nama' => "Peralatan Toko",
            ],
            [
                'tipe_akun' => "Aktiva Tetap",
                'kode_akun' => 1231,
                'nama' => "Akumulasi penyusutan Peralatan Toko",
            ],
            [
                'tipe_akun' => "Kewajiban",
                'kode_akun' => 2110,
                'nama' => "Hutang Dagang",
            ],
            [
                'tipe_akun' => "Kewajiban",
                'kode_akun' => 2120,
                'nama' => "Hutang Bank",
            ],
            [
                'tipe_akun' => "Kewajiban",
                'kode_akun' => 2130,
                'nama' => "Hutang Gaji",
            ],
            [
                'tipe_akun' => "Kewajiban",
                'kode_akun' => 2140,
                'nama' => "Hutang Lain-lain",
            ],
            [
                'tipe_akun' => "Modal",
                'kode_akun' => 3100,
                'nama' => "Modal",
            ],
            [
                'tipe_akun' => "Modal",
                'kode_akun' => 3200,
                'nama' => "Prive",
            ],
            [
                'tipe_akun' => "Pendapatan",
                'kode_akun' => 4110,
                'nama' => "Penjualan",
            ],
            [
                'tipe_akun' => "Pendapatan",
                'kode_akun' => 4120,
                'nama' => "Retur Penjualan",
            ],
            [
                'tipe_akun' => "Pendapatan",
                'kode_akun' => 4130,
                'nama' => "Potongan Penjualan",
            ],
            [
                'tipe_akun' => "Pendapatan",
                'kode_akun' => 4140,
                'nama' => "Pendapatan Lain-lain",
            ],
            [
                'tipe_akun' => "Pendapatan",
                'kode_akun' => 4150,
                'nama' => "Pembelian",
            ],
            [
                'tipe_akun' => "Pendapatan",
                'kode_akun' => 4160,
                'nama' => "Retur Pembelian",
            ],
            [
                'tipe_akun' => "Pendapatan",
                'kode_akun' => 4170,
                'nama' => "Potongan Pembelian",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5100,
                'nama' => "Biaya Angkut Pembelian",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5200,
                'nama' => "BBiaya Perlengkapan",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5310,
                'nama' => "Biaya Gaji Karyawan",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5320,
                'nama' => "Biaya Sewa",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5330,
                'nama' => "Biaya Asuransi Kendaraan",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5340,
                'nama' => "Biaya Servis Kendaraan",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5350,
                'nama' => "Biaya BBM, Parkir, dan Retribusi",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5360,
                'nama' => "Biaya Listrik dan Telepon",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5370,
                'nama' => "Biaya Alat Tulis Kantor",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5380,
                'nama' => "Biaya Penyusutan Kendaraan",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5390,
                'nama' => "Biaya Lain-lain",
            ],
            [
                'tipe_akun' => "Biaya",
                'kode_akun' => 5400,
                'nama' => "HPP",
            ]
        ];

        // $akuns = [];

        // foreach ($akun as $key => $value) {
        //     $kode_akun = $value["kode_akun"];
        //     $value["perusahaan"] = 1;
        //     $value["kode_akun"] = $kode_akun . "A";
        //     $value["tanggal"] = 2022-01-01;
        //     $value["saldo_awal"] = 0;
        //     $value["created_at"] = now();
        //     $value["updated_at"] = now();

        //     $akuns[] = $value;

        //     $value["perusahaan"] = 2;
        //     $value["kode_akun"] = $kode_akun;
        //     $akuns[] = $value;
        // }

        // Akun::insert($akuns);

        foreach ($akun as $key => $item) {
            Akun::create([
                'tipe_akun' => $item['tipe_akun'],
                'kode_akun' => $item['kode_akun'],
                'nama' => $item['nama'],
                'tanggal' => Carbon::now(),
            ]);
        }
    }
}
