<?php

namespace App\Imports;

use App\Models\Pegawai;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PegawaiImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        $pegawai = Pegawai::withTrashed()->get();
        $ctr = 1;
        foreach($pegawai as $p){
            $ctr = intval(substr($p->id_pegawai, 2)) + 1;
        }
        if($ctr<10){
            $kode = "P00{$ctr}";
        }else if($ctr<100){
            $kode = "P0{$ctr}";
        }else{
            $kode = "P{$ctr}";
        }

        $data = Pegawai::where('email_pegawai', $row[11])->first();
        if (empty($data)) {
            return new Pegawai([
                'id_pegawai' => $kode,
                'nama_pegawai' => $row[1],
                'npwp_pegawai' => $row[2],
                'alamat_pegawai' => $row[3],
                'provinsi_pegawai' => $row[4],
                'kota_pegawai' => $row[5],
                'kecamatan_pegawai' => $row[6],
                'kelurahan_pegawai' => $row[7],
                'kodepos_pegawai' => $row[8],
                'notelp_pegawai' => $row[9],
                'fax_pegawai' => $row[10],
                'email_pegawai' => $row[11],
                'password'  => Hash::make($row[12]),
            ]);
        }
    }
}
