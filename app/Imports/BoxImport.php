<?php

namespace App\Imports;

use App\Models\Box;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BoxImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        $box = Box::withTrashed()->get();
        $ctr = 1;
        foreach($box as $b){
            $ctr = intval(substr($b->id_box, 2)) + 1;
        }
        if($ctr<10){
            $kode = "B00{$ctr}";
        }else if($ctr<100){
            $kode = "B0{$ctr}";
        }else{
            $kode = "B{$ctr}";
        }

        $data = Box::where('nama_box', $row[1])->first();
        if (empty($data)) {
            return new Box([
                'id_box' => $kode,
                'nama_box' => $row[1],
                'tipe_box' => $row[2],
                'panjang_box' => $row[3],
                'lebar_box' => $row[4],
                'tinggi_box' => $row[5],
                'keterangan' => $row[6],
            ]);
        }
    }
}
