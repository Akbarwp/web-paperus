<?php

namespace App\Imports;

use App\Models\Supplier;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class SupplierImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        $supplier = Supplier::withTrashed()->get();
        $ctr = 1;
        foreach($supplier as $s){
            $ctr = intval(substr($s->id_supplier, 2)) + 1;
        }
        if($ctr<10){
            $kode = "S00{$ctr}";
        }else if($ctr<100){
            $kode = "S0{$ctr}";
        }else{
            $kode = "S{$ctr}";
        }

        $data = Supplier::where('email_supplier', $row[11])->first();
        if (empty($data)) {
            return new Supplier([
                'id_supplier' => $kode,
                'nama_supplier' => $row[1],
                'npwp_supplier' => $row[2],
                'alamat_supplier' => $row[3],
                'provinsi_supplier' => $row[4],
                'kota_supplier' => $row[5],
                'kecamatan_supplier' => $row[6],
                'kelurahan_supplier' => $row[7],
                'kodepos_supplier' => $row[8],
                'notelp_supplier' => $row[9],
                'email_supplier' => $row[10],
                'item' => $row[11],
                'nama_bank' => $row[12],
                'no_rekening' => $row[13],
            ]);
        }
    }
}
