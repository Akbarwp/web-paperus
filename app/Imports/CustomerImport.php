<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class CustomerImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        $customer = Customer::withTrashed()->get();
        $ctr = 1;
        foreach($customer as $c){
            $ctr = intval(substr($c->id_customer, 2)) + 1;
        }
        if($ctr<10){
            $kode = "CU00{$ctr}";
        }else if($ctr<100){
            $kode = "CU0{$ctr}";
        }else{
            $kode = "CU{$ctr}";
        }

        $data = Customer::where('email_customer', $row[11])->first();
        if (empty($data)) {
            return new Customer([
                'id_customer' => $kode,
                'nama_customer' => $row[1],
                'npwp_customer' => $row[2],
                'alamat_customer' => $row[3],
                'provinsi_customer' => $row[4],
                'kota_customer' => $row[5],
                'kecamatan_customer' => $row[6],
                'kelurahan_customer' => $row[7],
                'kodepos_customer' => $row[8],
                'notelp_customer' => $row[9],
                'nofax_customer' => $row[10],
                'email_customer' => $row[11],
                'batasan_hutang' => $row[12],
                'hutang_sekarang' => $row[13],
                'hutang_tersedia' => $row[14],
                'no_rekening' => $row[15],
                'metode_pembayaran' => $row[16],
            ]);
        }
    }
}
