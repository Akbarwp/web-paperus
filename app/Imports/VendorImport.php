<?php

namespace App\Imports;

use App\Models\Vendor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class VendorImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }
    
    public function model(array $row)
    {
        $vendor = Vendor::withTrashed()->get();
        $ctr = 1;
        foreach($vendor as $v){
            $ctr = intval(substr($v->id_vendor, 2)) + 1;
        }
        if($ctr<10){
            $kode = "V00{$ctr}";
        }else if($ctr<100){
            $kode = "V0{$ctr}";
        }else{
            $kode = "V{$ctr}";
        }

        $data = Vendor::where('email_vendor', $row[8])->first();
        if (empty($data)) {
            return new Vendor([
                'id_vendor' => $kode,
                'nama_vendor' => $row[1],
                'jenis_item' => $row[2],
                'kategori_vendor' => $row[3],
                'alamat_vendor' => $row[4],
                'kota_vendor' => $row[5],
                'kecamatan_vendor' => $row[6],
                'kelurahan_vendor' => $row[7],
                'email_vendor' => $row[8],
                'nama_bank' => $row[9],
                'no_akun' => $row[10],
                'no_rekening' => $row[11],
                'batasan_hutang' => $row[12],
                'hutang_sekarang' => $row[13],
                'hutang_tersedia' => $row[14],
                'sisa_hutang' => $row[15],
                'metode_pembayaran' => $row[16],
            ]);
        }
    }
}
