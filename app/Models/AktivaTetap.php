<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AktivaTetap extends Model
{
    protected $table = 'aktiva_tetap';
    protected $fillable = ['jenis_aktiva','tanggal', 'jenis_lain', 'nominal', 'durasi_aktiva', 'penyusutan', 'keterangan'];

    public function akun()
    {
        return $this->belongsTo(Akun::class, 'kode');
    }
}
