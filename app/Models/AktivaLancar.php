<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AktivaLancar extends Model
{
    protected $table = 'aktiva_lancar';
    protected $fillable = [
        'kode',
        'tanggal',
        'nama_produk', 
        'jenis_aktiva', 
        'jenis_lain', 
        'biaya', 
        'keterangan',
        'nama_tipe_akun',
        'kode_nama_akun',
    ];

    public function akun()
    {
        return $this->belongsTo(Akun::class, 'kode');
    }
}
