<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcessingBarangJadi2 extends Model
{
    use HasFactory;
    protected $table = "processing_barang_jadi2";
    protected $primaryKey = "id_proses_barang_jadi";
    public $timestamps = false;
    public $autoincrement = true;
    protected $fillable = [
        'id_produksi',
        'proses',
        'status'
    ];
}
