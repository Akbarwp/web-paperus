<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PenjualanStokJadi extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "penjualan_stok_jadi";
    protected $primaryKey = "kode_pembuatan";
    public $timestamps = true;
    protected $fillable = [
        'kode_pembuatan',
        'id_produksi',
        'id_customer',
        'qty',
        'harga_satuan',
        'total_harga',
        'diskon',
        'potongan',
        'nett',
        'status_penjualan',
    ];
    protected $keyType = 'string';
}
