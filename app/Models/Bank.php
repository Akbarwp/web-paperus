<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'bank';
    protected $fillable = ['nama_bank','tanggal','bank_lain','nama_akun','nomor_rekening','nominal','keterangan','jenis'];

    public function akun()
    {
        return $this->belongsTo(Akun::class, 'kode');
    }
}
