<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BarangJadi extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "barang_jadi";
    protected $primaryKey = "id_produksi";
    public $timestamps = true;
    protected $fillable = [
        'id_produksi',
        'jenis_box',
        'panjang',
        'lebar',
        'tinggi',
        'qty',
        'jum_produksi',
    ];
    protected $keyType = 'string';
}
