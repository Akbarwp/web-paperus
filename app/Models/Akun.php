<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
    protected $table = 'akun';
    protected $fillable = [
        'perusahaan',
        'tipe_akun', 
        'kode_akun', 
        'nama', 
        'tanggal', 
        'saldo_awal', 
        'created_at', 
        'updated_at'];

    public static $perusahaans = ['1' => 'CV.Anzac Food', '2' => 'Paperus'];
    public static $perusahaan_ppn = '1';
    public static $perusahaan_prefix = 'A';

    public static $aktiva_lancar = 1100;
    public static $kas = 1110;
    public static $bank = 1120;
    public static $piutang_dagang = 1130;
    public static $cadangan_kerugian = 1140;
    public static $piutang_karyawan = 1150;
    public static $persediaan_barang = 1160;
    public static $sewa_dbm = 1170;
    public static $uang_muka_pembelian = 1180;
    public static $aktiva_tetap = 1200;
    public static $kendaraan = 1220;
    public static $akumulasi_penyusutan_kendaraan = 1221;
    public static $peralatan_toko = 1230;
    public static $akumulasi_penyusutan_peralatan_toko = 1231;

    public static $modal = 3100;
    public static $prive = 3200;

    public static $penjualan = 4110;
    public static $retur_penjualan = 4120;
    public static $potongan_penjualan = 4130;
    public static $pendapatan_lain_lain = 4140;
    public static $pembelian = 4150;
    public static $retur_pembelian = 4160;
    public static $potongan_pembelian = 4170;

    public static $biaya_perlengkapan = 5200;
    public static $biaya_penyusutan_kendaraan = 5380;
    public static $hpp = 5400;

    public static function cekSaldoNormal($akun) 
    {
        $tipe_akun_kredit = ['Modal', 'Kewajiban', 'Pendapatan'];
        $akun_kredit = [
            self::$akumulasi_penyusutan_kendaraan, 
            self::$akumulasi_penyusutan_peralatan_toko, 
            self::$pembelian,
            self::$retur_pembelian,
        ];
        $akun_debit = [self::$prive, self::$potongan_penjualan, self::$retur_penjualan];

        // Prefix Kode Akun
        $kode_akun = $akun->kode_akun;
        if ($akun->perusahaan == self::$perusahaan_ppn && substr($kode_akun,-1) == self::$perusahaan_prefix) {
            $kode_akun = substr($kode_akun,0,-1);;
        }

        $saldo_normal = "debit";
        if ($akun && $akun->tipe_akun) {
            $saldo_normal = in_array($akun->tipe_akun, $tipe_akun_kredit) ? 'kredit' : 'debit';
        }
        if ($akun && $kode_akun) {
            $saldo_normal = in_array($kode_akun, $akun_kredit) ? 'kredit' : $saldo_normal;
            $saldo_normal = in_array($kode_akun, $akun_debit) ? 'debit' : $saldo_normal;
        }

        return $saldo_normal;
    }

    public static function checkAkunPerusahaan($akun)
    {
        $transaction_type = $akun->perusahaan;
        return [
            'transaction_type' => $transaction_type,
            'perusahaan_prefix' => $transaction_type == \App\Models\Akun::$perusahaan_ppn ? \App\Models\Akun::$perusahaan_prefix : "",
            'transaction_query_table' => $transaction_type == \App\Models\Akun::$perusahaan_ppn ? "transactions" : "transaction_non_ppn as transactions",
            'status_pajak' => $transaction_type == \App\Models\Akun::$perusahaan_ppn ? 'ppn' : 'non_ppn',
        ];
    }

    public function aktiva_lancar()
    {
        return $this->hasMany(AktivaLancar::class, 'kode_nama_akun', 'kode_akun');  
    }
}
