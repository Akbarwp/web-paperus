<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JurnalUmum extends Model
{
    public $timestamps = false;
    protected $table = "jurnal_umum";

    protected $fillable = [
        "bukti_transaksi",
        "tanggal",
        "kode_akun",
        "id_kode_akun",
        "keterangan",
        "debit",
        "kredit",
    ];

    public function kodeAkun()
    {
        return $this->belongsTo(Akun::class, 'id_kode_akun', 'id');
    }
}
