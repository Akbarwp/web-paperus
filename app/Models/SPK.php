<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SPK extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "master_spk";
    protected $primaryKey = "no_spk";
    public $timestamps = false;
    protected $fillable = [
        'no_spk',
        'id_penjualan',
        'pic',
        'jenis_box',
        'jum_produksi',
        'link_desain',
        'pisau',
        'plat'
    ];
    protected $keyType = 'string';


}
