<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportAktivaLancar implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $aktiva_lancar = \App\Models\AktivaLancar::orderBy('created_at', 'desc')->get();
        return view('excelaktivalancar',compact('aktiva_lancar'));
    }
}
