<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportAktivaTetap implements FromView,ShouldAutoSize
{
    public function view(): View
    {
        $aktiva_tetap = \App\Models\AktivaTetap::orderBy('created_at', 'desc')->get();
        return view('excelaktivatetap',compact('aktiva_tetap'));
    }
}
