<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportAktiva implements FromView,ShouldAutoSize
{
    public function view(): View
    {
        $aktiva_lancar = \App\Models\AktivaLancar::orderBy('created_at', 'desc')->get();
        $aktiva_tetap = \App\Models\AktivaTetap::orderBy('created_at', 'desc')->get();
        return view('excelaktiva',compact('aktiva_tetap','aktiva_lancar'));
    }
    
}
