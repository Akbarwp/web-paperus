<?php

namespace App\Http\Controllers;

use App\Models\BarangJadi;
use App\Models\Customer;
use App\Models\SPK;
use App\Models\Vendor;
use Illuminate\Http\Request;
use App\Models\PenjualanStokJadi;

class PenjualanStokJadiController extends Controller
{
    public function show()
    {
        $penjualan = PenjualanStokJadi::query()
            ->join('master_customer as c', 'c.id_customer', 'penjualan_stok_jadi.id_customer')
            ->join('barang_jadi as b', 'b.id_produksi', 'penjualan_stok_jadi.id_produksi')
            ->select('penjualan_stok_jadi.*', 'c.nama_customer', 'b.jenis_box')
            ->get();

        return view('masterpenjualanstokjadi', compact('penjualan'));
    }

    public function create()
    {
        $barangJadi = BarangJadi::all();
        
        $penjualan = PenjualanStokJadi::get();
        $ctr = 1;
        foreach ($penjualan as $p) {
            $ctr = intval(substr($p->kode_pembuatan, 2)) + 1;
        }
        if ($ctr < 10) {
            $kode = "PR00{$ctr}";
        } else if ($ctr < 100) {
            $kode = "PR0{$ctr}";
        } else {
            $kode = "PR{$ctr}";
        }

        $customer = Customer::all();

        return view('tambahpenjualanbarangjadi', compact('barangJadi', 'kode', 'customer'));
    }

    public function edit(Request $request)
    {
        $penjualan = PenjualanStokJadi::find($request->id);
        $barangJadi = BarangJadi::all();
        $customer = Customer::all();

        return view('ubahpenjualanbarangjadi', compact('penjualan', 'barangJadi', 'customer'));
    }

    public function doAddpenjualanbarangjadi(Request $request)
    {
        $produk = BarangJadi::where('id_produksi', $request->id_produksi)->first();
        if ( $request->qty > $produk->qty) {
            return redirect('tambahpenjualanbarangjadi')->with('error', 'Quantity barang jadi tidak mencukupi');
        }

        PenjualanStokJadi::create([
            'kode_pembuatan' => $request->kode_pembuatan,
            'qty' => $request->qty,
            'harga_satuan' => $request->harga_satuan,
            'total_harga' => $request->total_harga,
            'diskon' => $request->diskon,
            'potongan' => $request->potongan,
            'nett' => $request->nett,
            'id_produksi' => $request->id_produksi,
            'id_customer' => $request->id_customer,
            'status_penjualan' => 0,
        ]);

        // BarangJadi::where('id_produksi', $request->id_produksi)->update([
        //     'qty' => $produk->qty - $request->qty,
        // ]);

        return redirect("/masterpenjualanstokjadi");
    }

    public function doEditpenjualanbarangjadi(Request $request)
    {
        $produk = BarangJadi::where('id_produksi', $request->id_produksi)->first();
        if ( $request->qty > $produk->qty) {
            return redirect('tambahpenjualanbarangjadi')->with('error', 'Quantity barang jadi tidak mencukupi');
        }

        PenjualanStokJadi::find($request->kode_pembuatan)->update([
            'qty' => $request->qty,
            'harga_satuan' => $request->harga_satuan,
            'total_harga' => $request->total_harga,
            'diskon' => $request->diskon,
            'potongan' => $request->potongan,
            'nett' => $request->nett,
            'id_produksi' => $request->id_produksi,
            'id_customer' => $request->id_customer,
        ]);

        // BarangJadi::where('id_produksi', $request->id_produksi)->update([
        //     'qty' => $produk->qty - $request->qty,
        // ]);

        return redirect("/masterpenjualanstokjadi");
    }

    public function accPenjualan(Request $request)
    {
        PenjualanStokJadi::find($request->kode_pembuatan)->update([
            'status_penjualan' => 1,
        ]);
        return back();
    }

    public function declinePenjualan(Request $request)
    {
        PenjualanStokJadi::find($request->kode_pembuatan)->update([
            'status_penjualan' => 0,
        ]);
        return back();
    }

    public function delete(Request $request , $id)
    {
        $penjualan = PenjualanStokJadi::withTrashed()->find($id);
        if($penjualan->trashed()){
            $result = $penjualan->restore();
        }else{
            $result = $penjualan->delete();
        }
        if ($result) {
            return redirect('/masterpenjualanstokjadi');
        } else {
            return redirect('/masterpenjualanstokjadi');
        }
    }
}
