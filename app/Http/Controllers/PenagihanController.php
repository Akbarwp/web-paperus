<?php

namespace App\Http\Controllers;

use App\Models\Penagihan;
use App\Models\Penawaran;
use Illuminate\Http\Request;
use App\Models\PenjualanStokJadi;

class PenagihanController extends Controller
{
    public function index()
    {
        $penagihan = Penagihan::orderBy('created_at', 'asc')->get();

        return view('masterpenagihan', compact('penagihan'));
    }

    public function create()
    {
        $penawaran = Penawaran::where('status_penawaran', '1')->get();
        $penjualanBarangJadi = PenjualanStokJadi::where('status_penjualan', '1')->get();
        
        return view('tambahpenagihan', compact('penawaran', 'penjualanBarangJadi'));
    }

    public function cekPenawaran(Request $req)
    {
        $penawaran = Penawaran::where('id_penawaran', $req->id_penawaran)->first();
        return response()->json($penawaran);
    }
    public function cekKodePembuatan(Request $req)
    {
        $kodePembuatan = PenjualanStokJadi::query()
            ->join('master_customer as c', 'c.id_customer', 'penjualan_stok_jadi.id_customer')
            ->join('barang_jadi as b', 'b.id_produksi', 'penjualan_stok_jadi.id_produksi')
            ->where('kode_pembuatan', $req->kode_pembuatan)
            ->select('penjualan_stok_jadi.*', 'c.nama_customer', 'b.jenis_box')
            ->first();

        return response()->json($kodePembuatan);
    }

    public function doAddpenagihan(Request $request)
    {
        if ($request->cekPenawaran == 1) {
            $id_penjualan = $request->id_penjualan1;
        } elseif ($request->cekPenjualan == 1) {
            $id_penjualan = $request->id_penjualan2;
        }
        Penagihan::create([
            'id_penjualan' => $id_penjualan,
            'pic' => $request->pic,
            'jenis_box' => $request->jenis_box,
            'qty' => $request->qty,
            'nominal' => $request->nominal,
            'status_penagihan' => 0,
        ]);

        return redirect('/penagihan');
    }

    public function accPenagihan(Request $request)
    {
        Penagihan::find($request->id_penagihan)->update([
            'status_penagihan' => 1,
        ]);
        return back();
    }

    public function declinePenagihan(Request $request)
    {
        Penagihan::find($request->id_penagihan)->update([
            'status_penagihan' => 0,
        ]);
        return back();
    }

    public function delete(Request $request , $id)
    {
        $penagihan = Penagihan::withTrashed()->find($id);
        if($penagihan->trashed()){
            $result = $penagihan->restore();
        }else{
            $result = $penagihan->delete();
        }
        if ($result) {
            return redirect('/penagihan');
        } else {
            return redirect('/penagihan');
        }
    }
}
