<?php

namespace App\Http\Controllers;

use App\Models\SPK;
use App\Models\Desain;
use App\Models\Vendor;
use App\Models\Penawaran;
use App\Models\Processing1;
use App\Models\Processing2;
use Illuminate\Http\Request;
use App\Models\masukkeluarstok;
use App\Models\SPK_Processing1;
use App\Models\PenjualanStokJadi;
use App\Models\ProcessingBarangJadi1;
use App\Models\ProcessingBarangJadi2;

class SPKController extends Controller
{
    public function index()
    {
        $no_spk = SPK::all();

        return view('masterspk', compact('no_spk'));
    }

    public function create(Request $request)
    {
        $desain = Desain::where('status_desain', '1')->get();
        $vendor = Vendor::all();
        $no_spk = SPK::all();
        $penawaran = Penawaran::where('status_penawaran', '1')->get();
        $penjualanBarangJadi = PenjualanStokJadi::where('status_penjualan', '1')->get();
        $pisau = masukkeluarstok::where('jenisbarang', 'Pisau')->get();
        $plat = masukkeluarstok::where('jenisbarang', 'Plat')->get();

        $spk_penawaran = SPK::where('id_penjualan', 'NOT LIKE', '%PR%')->get();
        $spk_barang_jadi = SPK::where('id_penjualan', 'LIKE', '%PR%')->get();

        $ctr = 1;
        foreach ($no_spk as $p) {
            $ctr = intval(substr($p->no_spk, 2)) + 1;
        }
        if ($ctr < 10) {
            $kode = "SP00{$ctr}";
        } else if ($ctr < 100) {
            $kode = "SP0{$ctr}";
        } else {
            $kode = "SP{$ctr}";
        }
        // $arr = [];
        // $arr2 = [];
        // $data = json_decode($request->temp);
        // $spk = SPK::join('processing1', 'processing1.id_penawaran', '=', 'master_spk.id_penawaran')->get();
        // $spk = SPK::join('processing1', 'processing1.id_penawaran', '=', 'master_spk.id_penawaran')->get();
        // for ($i=0; $i < count($spk); $i++) {
        //     $bool = false;

        //     for ($j=0; $j < count($arr); $j++) {
        //         if($spk[$i]->proses==$arr[$j]){
        //             $bool = true;
        //         }
        //     }
        //     if(!$bool){
        //         array_push($arr,$spk[$i]->proses);
        //         array_push($arr2,$spk[$i]);
        //     }
        // }
        // $proces1 = Processing1::paginate(5);
        $proces1 = Processing1::all();
        // if($data == null){
        // }else{
        //     $proces1 = Processing1::where('id_penawaran', $data)->get();
        // }
        $proces2 = Processing2::paginate(5);
        return view('suratperintahkerja', compact('desain','no_spk', 'proces1','proces2','vendor', 'penjualanBarangJadi', 'penawaran', 'pisau', 'plat', 'kode', 'spk_penawaran', 'spk_barang_jadi'));
    }

    public function cekPenawaran(Request $req)
    {
        $penawaran = Penawaran::where('id_penawaran', $req->id_penawaran)->first();
        return response()->json($penawaran);
    }
    public function cekKodePembuatan(Request $req)
    {
        $kodePembuatan = PenjualanStokJadi::query()
            ->join('master_customer as c', 'c.id_customer', 'penjualan_stok_jadi.id_customer')
            ->join('barang_jadi as b', 'b.id_produksi', 'penjualan_stok_jadi.id_produksi')
            ->where('kode_pembuatan', $req->kode_pembuatan)
            ->select('penjualan_stok_jadi.*', 'c.nama_customer', 'b.jenis_box')
            ->first();

        return response()->json($kodePembuatan);
    }

    // Proses1
    // Penawaran
    public function cekProcessing1(Request $req)
    {
        $spk = SPK::where('no_spk', $req->no_spk)->first();
        $processing1 = Processing1::where('id_penawaran', $spk->id_penjualan)->get();

        return response()->json($processing1);
    }

    public function editProcessing1(Request $req)
    {
        $processing1 = Processing1::where('id_proses1', $req->id_proses1)->first();
        $vendor = Vendor::all();
        $cekEditSPK = 1;
        return view('ubahProcessing1', compact(['processing1', 'vendor', 'cekEditSPK']));
    }
    public function simpanProcessing1(Request $req)
    {
        Processing1::where('id_proses1', $req->id_proses1)->update([
            'nama_brand' => $req->nama_brand,
            'jumlah' => $req->jumlah,
            'harga_satuan' => $req->harga_satuan,
            'harga_total' => $req->jumlah * $req->harga_satuan,
            'status' => $req->status,
        ]);
        return redirect('/suratperintahkerja');
    }

    // Penjualan Barang Jadi
    public function cekProcessingBarangJadi1(Request $req)
    {
        $spk = SPK::where('no_spk', $req->no_spk)->first();
        $penjualanBarangJadi = PenjualanStokJadi::where('kode_pembuatan', $spk->id_penjualan)->first();
        $processing1 = ProcessingBarangJadi1::where('id_produksi', $penjualanBarangJadi->id_produksi)->get();

        return response()->json($processing1);
    }

    public function editBarangJadi1(Request $req)
    {
        $processing1 = ProcessingBarangJadi1::where('id_proses_barang_jadi', $req->id_proses_barang_jadi)->first();
        $cekEditSPK = 0;
        return view('ubahProcessing1', compact(['processing1', 'cekEditSPK']));
    }
    public function simpanBarangJadi1(Request $req)
    {
        ProcessingBarangJadi1::where('id_proses_barang_jadi', $req->id_proses_barang_jadi)->update([
            'jumlah' => $req->jumlah,
            'harga_satuan' => $req->harga_satuan,
            'harga_total' => $req->jumlah * $req->harga_satuan,
            'status' => $req->status,
        ]);
        return redirect('/suratperintahkerja');
    }

    // Proses2
    // Penawaran
    public function cekProcessing2(Request $req)
    {
        $spk = SPK::where('no_spk', $req->no_spk)->first();
        $processing1 = Processing2::where('id_penawaran', $spk->id_penjualan)->get();

        return response()->json($processing1);
    }

    public function editProcessing2(Request $req)
    {
        $processing2 = Processing2::where('id_proses2', $req->id_proses2)->first();
        $vendor = Vendor::all();
        $cekEditSPK = 1;
        return view('ubahProcessing2', compact(['processing2', 'vendor', 'cekEditSPK']));
    }
    public function simpanProcessing2(Request $req)
    {
        Processing2::where('id_proses2', $req->id_proses2)->update([
            'nama_brand' => $req->nama_brand,
            'jumlah' => $req->jumlah,
            'harga_satuan' => $req->harga_satuan,
            'harga_total' => $req->jumlah * $req->harga_satuan,
            'status' => $req->status,
        ]);
        return redirect('/suratperintahkerja');
    }

    // Penjualan Barang Jadi
    public function cekProcessingBarangJadi2(Request $req)
    {
        $spk = SPK::where('no_spk', $req->no_spk)->first();
        $penjualanBarangJadi = PenjualanStokJadi::where('kode_pembuatan', $spk->id_penjualan)->first();
        $processing2 = ProcessingBarangJadi2::where('id_produksi', $penjualanBarangJadi->id_produksi)->get();

        return response()->json($processing2);
    }

    public function editBarangJadi2(Request $req)
    {
        $processing2 = ProcessingBarangJadi2::where('id_proses_barang_jadi', $req->id_proses_barang_jadi)->first();
        $cekEditSPK = 0;
        return view('ubahProcessing2', compact(['processing2', 'cekEditSPK']));
    }
    public function simpanBarangJadi2(Request $req)
    {
        ProcessingBarangJadi2::where('id_proses_barang_jadi', $req->id_proses_barang_jadi)->update([
            'jumlah' => $req->jumlah,
            'harga_satuan' => $req->harga_satuan,
            'harga_total' => $req->jumlah * $req->harga_satuan,
            'status' => $req->status,
        ]);
        return redirect('/suratperintahkerja');
    }


    public function doAddSPK(Request $req)
    {
        if ($req->cekPenawaran == 1) {
            $id_penjualan = $req->id_penjualan1;
        } elseif ($req->cekPenjualan == 1) {
            $id_penjualan = $req->id_penjualan2;
        }
        SPK::create([
            'no_spk' => $req->no_spk,
            'id_penjualan'=>$id_penjualan,
            'pic'=>$req->pic,
            'jenis_box'=>$req->jenis_box,
            'jum_produksi'=>$req->jum_produksi,
            'link_desain'=>$req->link_desain,
            'pisau'=>$req->pisau,
            'plat'=>$req->plat
        ]);

        // if ($req->cekPenawaran == 1) {
        //     $spk_proces = Processing1::where('id_penawaran', $req->id_penawaran)->get();
        //     foreach ($spk_proces as $prm){
        //         SPK_Processing1::create([
        //             'id_proses' => $prm->id_proses1,
        //             'jenis_proses' => $prm->proses,
        //             'nama_vendor' => $prm->nama_brand,
        //             'jumlah' => '0',
        //             'harga_satuan' => '0',
        //             'harga_total' => '0',
        //             'harga_satuan_sebelumnya' => '0',
        //             'harga_total_sebelumnya' => '0',
        //             'status' => '1',
        //             'no_spk' => $req->no_spk
        //         ]);
        //     }

        // } elseif ($req->cekPenjualan == 1) {
            
        // }

        return redirect("/mastersuratperintahkerja");
    }

    public function delete(Request $request , $id)
    {
        $spk = SPK::withTrashed()->find($id);
        if($spk->trashed()){
            $result = $spk->restore();
        }else{
            $result = $spk->delete();
        }
        if ($result) {
            return redirect('/mastersuratperintahkerja');
        } else {
            return redirect('/mastersuratperintahkerja');
        }
    }
}
