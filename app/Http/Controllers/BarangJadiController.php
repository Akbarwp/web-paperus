<?php

namespace App\Http\Controllers;

use App\Models\SPK;
use App\Models\Vendor;
use App\Models\BarangJadi;
use Illuminate\Http\Request;
use App\Models\ProcessingBarangJadi1;
use App\Models\ProcessingBarangJadi2;

class BarangJadiController extends Controller
{
    public function index()
    {
        $barangJadi = BarangJadi::all();
        return view('stokbarangjadi', compact('barangJadi'));
    }

    public function index2()
    {
        $barangJadi = BarangJadi::all();
        return view('masterbarangjadi', compact('barangJadi'));
    }

    public function tambahBarangJadi()
    {
        $arrBrand = Vendor::all();
        $spk = SPK::all();

        $produksi = BarangJadi::get();
        $ctr = 1;
        foreach ($produksi as $p) {
            $ctr = intval(substr($p->id_produksi, 2)) + 1;
        }
        if ($ctr < 10) {
            $kode = "B00{$ctr}";
        } else if ($ctr < 100) {
            $kode = "B0{$ctr}";
        } else {
            $kode = "B{$ctr}";
        }

        return view('tambahstokbarangjadi', compact('arrBrand','spk', 'kode'));
    }

    public function ubahBarangJadi(Request $request)
    {
        $barangJadi = BarangJadi::find($request->id);
        $checkbox = ProcessingBarangJadi1::where('id_produksi', $request->id)->get('proses');
        $cb = [];
        foreach ($checkbox as $item) {
            $cb[] = $item->proses;
        }
        return view('ubahstokbarangjadi', compact('barangJadi', 'cb'));
    }
    
    public function doAddPembuatanBarangJadi(Request $request)
    {
        $produksi = BarangJadi::get();
        $ctr = 1;
        foreach ($produksi as $p) {
            $ctr = intval(substr($p->id_produksi, 2)) + 1;
        }
        if ($ctr < 10) {
            $kode = "B00{$ctr}";
        } else if ($ctr < 100) {
            $kode = "B0{$ctr}";
        } else {
            $kode = "B{$ctr}";
        }
        
        BarangJadi::create([
            'id_produksi' => $request->kode,
            'jenis_box' => $request->jenis_box,
            'panjang' => $request->panjang,
            'lebar' => $request->lebar,
            'tinggi' => $request->tinggi,
            'qty' => $request->qty,
            'jum_produksi' => $request->jum_produksi,
        ]);

        $checked = $request->cb;
        for ($i = 0; $i < count($checked); $i++) {

            ProcessingBarangJadi1::create([
                'id_produksi' => $request->kode,
                'proses' => $checked[$i],
                'status' => 0,
            ]);
        }

        if ($request->proses2 == "1") {
            $checked2 = $request->cb2;
            for ($i = 0; $i < count($checked2); $i++) {

                ProcessingBarangJadi2::create([
                    'id_produksi' => $request->kode,
                    'proses' => $checked2[$i],
                    'status' => 0,
                ]);
            }
        }

        return redirect("/masterbarangjadi");
    }

    public function doEditPembuatanBarangJadi(Request $request)
    {
        BarangJadi::find($request->kode)->update([
            'jenis_box' => $request->jenis_box,
            'panjang' => $request->panjang,
            'lebar' => $request->lebar,
            'tinggi' => $request->tinggi,
            'qty' => $request->qty,
            'jum_produksi' => $request->jum_produksi,
        ]);

        ProcessingBarangJadi1::where('id_produksi', $request->kode)->delete();

        $checked = $request->cb;
        for ($i = 0; $i < count($checked); $i++) {

            ProcessingBarangJadi1::create([
                'id_produksi' => $request->kode,
                'proses' => $checked[$i],
                'status' => 1,
            ]);
        }
        return redirect("/stokbarangjadi");
    }

    public function delete(Request $request , $id)
    {
        $barangJadi = BarangJadi::withTrashed()->find($id);
        if($barangJadi->trashed()){
            $result = $barangJadi->restore();
        }else{
            $result = $barangJadi->delete();
        }
        if ($result) {
            return redirect('/stokbarangjadi');
        } else {
            return redirect('/stokbarangjadi');
        }
    }
}
