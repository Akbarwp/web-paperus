<?php

namespace App\Http\Controllers;

use PDF;
use stdClass;
use Carbon\Carbon;
use App\Models\Akun;
use App\Models\Bank;
use App\Models\BarangJadi;
use App\Models\JurnalUmum;
use App\Models\AktivaTetap;
use App\Models\AktivaLancar;
use Illuminate\Http\Request;
use App\Exports\ExportAktiva;
use App\Exports\ExportNeraca;
use App\Exports\ExportLabaRugi;
use App\Exports\ExportBukuBesar;
use App\Exports\ExportAktivaTetap;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Exports\ExportAktivaLancar;
use App\Exports\ExportJurnalHarian;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportJurnalHarianSingle;

class GeneralLedgerController extends Controller
{
    // Jurnal Umum
    public function viewJurnalUmum()
    {
        $now = Carbon::now();
        $year = $now->format('Y');
        $month = $now->format('m');

        $like_str = 'JU/' . $year . '/' . $month . '/%';

        $akun = Akun::select(['id', 'kode_akun', 'nama'])
        ->orderBy('kode_akun', 'asc')->get();

        $bukti_transaksi = JurnalUmum::select('bukti_transaksi')
        ->where('bukti_transaksi', 'LIKE', $like_str)
        ->distinct()
        ->orderByDesc('bukti_transaksi')
        ->first();

        $carbon = Carbon::now();

        if ($bukti_transaksi == null) {
            $bukti_transaksi = 'JU/' . $year . '/' . $month . '/' . str_pad("1", 4, '0', STR_PAD_LEFT);
        } else {
            $bukti_transaksi = $bukti_transaksi->bukti_transaksi;
            $increment = (int)substr($bukti_transaksi, 12, 4);
            $increment += 1;

            $bukti_transaksi = 'JU/' . $year . '/' . $month  . '/' . str_pad($increment, 4, '0', STR_PAD_LEFT);
        }

        return view('jurnalumum', compact('akun', 'bukti_transaksi'));
    }

    public function addJurnalUmum(Request $request)
    {
        $bukti_transaksi = $request->input('bukti_transaksi');
        $tanggal = $request->input('tanggal');

        $kode_akun_debit = $request->input("kode_akun_debit");
        $keterangan_debit = $request->input('keterangan_debit');
        $saldo_debit = $request->input("saldo_debit");

        $kode_akun_kredit = $request->input("kode_akun_kredit");
        $keterangan_kredit = $request->input('keterangan_kredit');
        $saldo_kredit = $request->input("saldo_kredit");

        $count_debit = count($kode_akun_debit);
        $count_kredit = count($kode_akun_kredit);

        $debit = 0;
        $kredit = 0;

        for ($i = 0; $i < $count_debit; $i++) {
            for ($j = 0; $j < $count_kredit; $j++) {
                if ($kode_akun_debit[$i] == $kode_akun_kredit[$j]) {
                    return redirect('accounting/jurnalUmum')->with("error", "Akun tidak boleh sama");
                }
            }
        }

        for ($i = 0; $i < $count_debit; $i++) {
            if ($kode_akun_debit[$i] == null || $keterangan_debit[$i] == null || $saldo_debit[$i] == null) {
                return redirect('accounting/jurnalUmum')->with("error", "Detail Akun Debit tidak boleh kosong");
            } else {
                $debit += $saldo_debit[$i];
            }
        }

        for ($i = 0; $i < $count_kredit; $i++) {
            if ($kode_akun_kredit[$i] == null || $keterangan_kredit[$i] == null || $saldo_kredit[$i] == null) {
                return redirect('accounting/jurnalUmum')->with("error", "Detail Akun Kredit tidak boleh kosong");
            } else {
                $kredit += $saldo_kredit[$i];
            }
        }

        if ($debit != $kredit) {
            return redirect('accounting/jurnalUmum')->with("error", "Saldo tidak balance");
        }

        DB::beginTransaction();
        try {
            for ($i = 0; $i < $count_debit; $i++) {
                $akun = Akun::find($kode_akun_debit[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun->kode_akun,
                    "id_kode_akun" => $kode_akun_debit[$i],
                    "keterangan" => $keterangan_debit[$i],
                    "debit" => $saldo_debit[$i],
                    "kredit" => 0,
                ]);
            }

            for ($i = 0; $i < $count_kredit; $i++) {
                $akun = Akun::find($kode_akun_kredit[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun->kode_akun,
                    "id_kode_akun" => $kode_akun_kredit[$i],
                    "keterangan" => $keterangan_kredit[$i],
                    "debit" => 0,
                    "kredit" => $saldo_kredit[$i],
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('accounting/jurnalUmum')->with("error", "Transaksi Gagal");
        }
        return redirect('accounting/jurnalUmum')->with("success", "Jurnal Umum berhasil ditambahkan");
    }

    // Akun
    public function viewAkun()
    {
        $akun = Akun::orderBy('tanggal', 'desc')->get();
        return view('akun', compact('akun'));
    }

    public function storeAkun(Request $request)
    {
        //validate
        $this->validate($request, [
            'perusahaan' => 'required',
            'tipe_akun' => 'required',
            'kode_akun' => 'required',
            'nama' => 'required',
            'tanggal' => 'required',
            'saldo_awal' => 'required',
        ]);

        //insert
        $kode_akun = substr($request->kode_akun,-1) == Akun::$perusahaan_prefix ? substr($request->kode_akun,0,-1) : $request->kode_akun;
        $prefix_kode_akun = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = new Akun;
        $akun->perusahaan = $request->perusahaan;
        $akun->tipe_akun = $request->tipe_akun;
        $akun->kode_akun = $kode_akun.$prefix_kode_akun;
        $akun->nama = $request->nama;
        $akun->tanggal = $request->tanggal;
        $akun->saldo_awal = $request->saldo_awal;
        $akun->save();

        return redirect()->back()->with('create_success', 'Data '.$request->nama.' berhasil ditambahkan');
    }

    public function duplicateAkun(Request $request)
    {
        // dd($request->all());

        $akun = Akun::find($request->id);
        $cek_nama = Akun::where('nama', $request->nama)->count();
        $cek_tgl = Akun::whereDate('tanggal', now()->format('Y-m-d'))->count();
        // dd(now()->format('Y-m-d H:i:s'), $cek_tgl);

        if ($cek_nama > 0 && $cek_tgl > 0) {
            return redirect()->back()->with('error', 'Data Duplicate sudah ada');   
        }else{
            foreach ($akun as $a => $i) {
                $duplicate = new Akun;
                $duplicate->perusahaan = $i->perusahaan;
                $duplicate->tipe_akun = $i->tipe_akun;
                $duplicate->kode_akun = $i->kode_akun;
                $duplicate->nama = $i->nama;
                $duplicate->tanggal = now()->format('Y-m-d H:i:s');
                $duplicate->saldo_awal = 0;
                $duplicate->save();
            }
            // return $akun;
            return redirect()->back()->with('create_success', 'Duplicate Data berhasil');
        }
        
    }

    public function updateAkun(Request $request)
    {
        //validate
        $this->validate($request, [
            'perusahaan' => 'required',
            'tipe_akun' => 'required',
            'kode_akun' => 'required',
            'nama' => 'required',
            'saldo_awal' => 'required',
        ]);

        //update
        $kode_akun = substr($request->kode_akun,-1) == Akun::$perusahaan_prefix ? substr($request->kode_akun,0,-1) : $request->kode_akun;
        $prefix_kode_akun = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::find($request->id);
        $akun->perusahaan = $request->perusahaan;
        $akun->tipe_akun = $request->tipe_akun;
        $akun->kode_akun = $kode_akun.$prefix_kode_akun;
        $akun->nama = $request->nama;
        $akun->tanggal = $request->tanggal;
        $akun->saldo_awal = $request->saldo_awal;
        $akun->save();

        return redirect()->back()->with('update_success', 'Data '.$request->nama.' berhasil diubah');
    }

    public function deleteAkun($id)
    {
        $akun = Akun::find($id);
        $akun->delete();

        return redirect()->back()->with('delete_success', 'Data '.$akun->nama.' berhasil dihapus');
    }

    public function dataAkun()
    {
        $akun = Akun::orderBy('kode_akun', 'asc')->get();
        $aktiva_lancar = AktivaLancar::orderBy('kode', 'asc')->get();
        $aktiva_tetap = AktivaTetap::orderBy('kode', 'asc')->get();

        //join akun, aktiva_lancar, aktiva_tetap to get data akun, aktiva_lancar, aktiva_tetap
        // $data_akun = Akun::join('aktiva_lancar', 'aktiva_lancar.kode_nama_akun', '=', 'akun.kode_akun', 'left outer')
        //     ->join('aktiva_tetap', 'aktiva_tetap.kode_nama_akun', '=', 'akun.kode_akun', 'left outer')
        //     ->select('akun.*', 'aktiva_lancar.*', 'aktiva_tetap.*')
        //     ->get();
        // return $data_akun;
        return view('akundata', compact('akun', 'aktiva_lancar', 'aktiva_tetap'));
    }

    // Aktiva Lancar
    public function viewAktivaLancar()
    {
        $aktiva_lancar = AktivaLancar::orderBy('created_at', 'desc')->get();
        $akun = Akun::where('tipe_akun', 'Aktiva Lancar')->get();
        return view('aktivalancar',compact('aktiva_lancar','akun'));
    }

    public function addAktivaLancar(Request $request)
    {
        $this->validate($request, [
            'nama_produk' => 'required',
            'jenis_aktiva' => 'required',
            'biaya' => 'required',
            'keterangan' => 'required',
            'kode' => 'required',
            'tanggal' => 'required',
            'metode_bayar' => 'required'
        ]);

        if ($request->jenis_lain == null) {
            $jenis_lain = '-';
        } else {
            $jenis_lain = $request->jenis_lain;
        }

        $aktiva_lancar = new AktivaLancar;
        $aktiva_lancar->nama_produk = $request->nama_produk;
        $aktiva_lancar->tanggal = $request->tanggal;
        $aktiva_lancar->jenis_aktiva = $request->jenis_aktiva;
        $aktiva_lancar->jenis_lain = $jenis_lain;
        $aktiva_lancar->biaya = $request->biaya;
        $aktiva_lancar->keterangan = $request->keterangan;
        $aktiva_lancar->kode = $request->kode;
        $aktiva_lancar->metode_bayar = $request->metode_bayar;
        $aktiva_lancar->kode_nama_akun = null;
        $aktiva_lancar->nama_tipe_akun = null;
        $aktiva_lancar->save();

        return redirect()->back()->with('create_success', 'Aktiva Lancar berhasil ditambahkan');
    }

    public function deleteAktivaLancar($id)
    {
        $aktiva_lancar = AktivaLancar::find($id);

        $aktiva_lancar->delete();
        return redirect()->back()->with('delete_success', 'Aktiva Lancar berhasil dihapus');
    }

    public function updateAktivaLancar(Request $request)
    {
        $this->validate($request, [
            'nama_produk' => 'required',
            'jenis_aktiva' => 'required',
            'biaya' => 'required',
            'keterangan' => 'required',
            'kode' => 'required',
            'metode_bayar' => 'required',
        ]);

        if ($request->jenis_lain == null) {
            $jenis_lain = '-';
        } else {
            $jenis_lain = $request->jenis_lain;
        }

        if($request->tanggal == null){
            $aktiva_lancar = AktivaLancar::find($request->id);
            $aktiva_lancar->nama_produk = $request->nama_produk;
            $aktiva_lancar->jenis_aktiva = $request->jenis_aktiva;
            $aktiva_lancar->jenis_lain = $jenis_lain;
            $aktiva_lancar->biaya = $request->biaya;
            $aktiva_lancar->keterangan = $request->keterangan;
            $aktiva_lancar->kode = $request->kode;
            $aktiva_lancar->metode_bayar = $request->metode_bayar;
        }else{
            $aktiva_lancar = AktivaLancar::find($request->id);
            $aktiva_lancar->tanggal = $request->tanggal;
            $aktiva_lancar->nama_produk = $request->nama_produk;
            $aktiva_lancar->jenis_aktiva = $request->jenis_aktiva;
            $aktiva_lancar->jenis_lain = $jenis_lain;
            $aktiva_lancar->biaya = $request->biaya;
            $aktiva_lancar->keterangan = $request->keterangan;
            $aktiva_lancar->kode = $request->kode;
            $aktiva_lancar->kode_nama_akun = $request->kode_nama_akun;
            $aktiva_lancar->nama_tipe_akun = $request->nama_tipe_akun;
            $aktiva_lancar->metode_bayar = $request->metode_bayar;
        }

        $aktiva_lancar->save();

        return redirect()->back()->with('update_success', 'Aktiva Lancar berhasil diubah');
    }

    // Aktiva Tetap
    public function viewAktivaTetap()
    {
        $aktiva_tetap = AktivaTetap::orderBy('created_at', 'desc')->get();
        $akun = Akun::where('tipe_akun', 'Aktiva Tetap')->get();
        return view('aktivatetap',compact('aktiva_tetap','akun'));
    }

    public function addAktivaTetap(Request $request)
    {
        $this->validate($request, [
            'jenis_aktiva' => 'required',
            'nominal' => 'required',
            'durasi_aktiva' => 'required',
            'penyusutan' => 'required',
            'keterangan' => 'required',
            'kode' => 'required',
            'tanggal' => 'required',
            'metode_bayar' => 'required',
        ]);

        if ($request->jenis_lain == null) {
            $jenis_lain = '-';
        } else {
            $jenis_lain = $request->jenis_lain;
        }

        $aktiva_tetap = new AktivaTetap;
        $aktiva_tetap->jenis_aktiva = $request->jenis_aktiva;
        $aktiva_tetap->tanggal = $request->tanggal;
        $aktiva_tetap->jenis_lain = $jenis_lain;
        $aktiva_tetap->nominal = $request->nominal;
        $aktiva_tetap->durasi_aktiva = $request->durasi_aktiva;
        $aktiva_tetap->penyusutan = $request->penyusutan;
        $aktiva_tetap->keterangan = $request->keterangan;
        $aktiva_tetap->kode = $request->kode;
        $aktiva_tetap->metode_bayar = $request->metode_bayar;
        $aktiva_tetap->nama_tipe_akun = null;
        $aktiva_tetap->kode_nama_akun = null;
        $aktiva_tetap->save();

        return redirect()->back()->with('create_success', 'Aktiva Tetap berhasil ditambahkan');
    }

    public function deleteAktivaTetap($id)
    {
        $aktiva_tetap = AktivaTetap::find($id);

        $aktiva_tetap->delete();
        return redirect()->back()->with('delete_success', 'Aktiva Tetap berhasil dihapus');
    }

    public function updateAktivaTetap(Request $request)
    {
        $this->validate($request, [
            'jenis_aktiva2' => 'required',
            'nominal2' => 'required',
            'durasi_aktiva2' => 'required',
            'penyusutan2' => 'required',
            'keterangan2' => 'required',
            'kode2' => 'required',
            'metode_bayar2' => 'required',
        ]);

        if ($request->jenis_lain2 == null) {
            $jenis_lain = '-';
        } else {
            $jenis_lain = $request->jenis_lain2;
        }

        if($request->tanggal2 == null){
            $aktiva_tetap = AktivaTetap::find($request->id2);
            $aktiva_tetap->jenis_aktiva = $request->jenis_aktiva2;
            $aktiva_tetap->jenis_lain = $jenis_lain;
            $aktiva_tetap->nominal = $request->nominal2;
            $aktiva_tetap->durasi_aktiva = $request->durasi_aktiva2;
            $aktiva_tetap->penyusutan = $request->penyusutan2;
            $aktiva_tetap->keterangan = $request->keterangan2;
            $aktiva_tetap->kode = $request->kode2;
            $aktiva_tetap->metode_bayar = $request->metode_bayar2;
        }else{
            $aktiva_tetap = AktivaTetap::find($request->id2);
            $aktiva_tetap->jenis_aktiva = $request->jenis_aktiva2;
            $aktiva_tetap->tanggal = $request->tanggal2;
            $aktiva_tetap->jenis_lain = $jenis_lain;
            $aktiva_tetap->nominal = $request->nominal2;
            $aktiva_tetap->durasi_aktiva = $request->durasi_aktiva2;
            $aktiva_tetap->penyusutan = $request->penyusutan2;
            $aktiva_tetap->keterangan = $request->keterangan2;
            $aktiva_tetap->kode = $request->kode2;
            $aktiva_tetap->metode_bayar = $request->metode_bayar2;
        }

        $aktiva_tetap->save();

        return redirect()->back()->with('update_success', 'Aktiva Tetap berhasil diubah');
    }

    public function editAktivaTetap($id)
    {
        $aktiva_tetap = AktivaTetap::find($id);
        return response()->json(['aktiva_tetap' => $aktiva_tetap]);
    }

    // Laporan Aktiva
    public function viewLaporanAktiva()
    {
        $aktiva_tetap = AktivaTetap::orderBy('created_at', 'desc')->get();
        $aktiva_lancar = AktivaLancar::orderBy('created_at', 'desc')->get();
        return view('aktivalaporan',compact('aktiva_tetap','aktiva_lancar'));
    }

    public function excelPrintLaporanAktiva(Request $req)
    {
        if ($req->jenis_aktiva == 'Semua') {
            return Excel::download(new ExportAktiva, 'Laporan Aktiva '.now().'.xlsx');
        }else if ($req->jenis_aktiva == 'Lancar') {
            return Excel::download(new ExportAktivaLancar, 'Laporan Aktiva Lancar '.now().'.xlsx');
        }else if ($req->jenis_aktiva == 'Tetap') {
            return Excel::download(new ExportAktivaTetap, 'Laporan Aktiva Tetap '.now().'.xlsx');
        }
    }

    // Bank Masuk
    public function viewBankIn(Request $request)
    {
        $transaction_type = "all";
        $startDate = Carbon::now();
        $endDate = Carbon::now();
        if ($request->start_date && $request->end_date) {
            $startDate = Carbon::parse($request->start_date);
            $endDate = Carbon::parse($request->end_date);
        }
        $startDate = $startDate->startOfDay();
        $endDate = $endDate->endOfDay();

        if ($request->jenis) {
            $transaction_type = $request->jenis;
        }

        $kode_akun = [];

        if ($transaction_type != "all") {
            $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
            $kode_akun[] = Akun::$bank . $perusahaan_prefix;
        } else {
            $kode_akun[] = Akun::$bank;
            $kode_akun[] = Akun::$bank . Akun::$perusahaan_prefix;
        }

        $akun = Akun::whereIn('kode_akun', $kode_akun)->pluck('id');

        $bank_in = DB::table('jurnal_umum')
            ->select('id', 'tanggal', 'keterangan', 'bukti_transaksi', 'debit', 'kredit')
            ->whereIn('id_kode_akun', $akun)
            ->orderBy('tanggal', 'asc')
            ->where('kredit', '0')
            ->where('bukti_transaksi', 'LIKE', 'BBM%')
            ->whereBetween('tanggal', [$startDate, $endDate])
            ->get();

        return view('bankmasuk', compact('bank_in', 'transaction_type', 'startDate', 'endDate'));
    }

    public function addBankIn()
    {
        $now = Carbon::now();
        $year = $now->format('Y');
        $month = $now->format('m');


        $like_str = 'BBM/' . $year . '/' . $month . '/%';

        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
        ->orderBy('kode_akun', 'asc')->get();

        $bukti_transaksi = DB::table('jurnal_umum')
        ->select('bukti_transaksi')
        ->where('bukti_transaksi', 'LIKE', $like_str)
            ->distinct()
            ->orderByDesc('bukti_transaksi')
            ->first();

        $carbon = Carbon::now();
        if ($bukti_transaksi == null) {
            $bukti_transaksi = 'BBM/' . $year . '/' . $month . '/' . str_pad("1", 4, '0', STR_PAD_LEFT);
        } else {
            $bukti_transaksi = $bukti_transaksi->bukti_transaksi;
            $increment = (int)substr($bukti_transaksi, 12, 4);
            $increment += 1;

            $bukti_transaksi = 'BBM/' . $year . '/' . $month . '/' . str_pad($increment, 4, '0', STR_PAD_LEFT);
        }


        return view('tambahbankmasuk', compact('akun', 'bukti_transaksi'));
    }

    public function processBankIn(Request $request)
    {
        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");

        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $debit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/bankMasuk/add')->with("error", "Detail Akun tidak boleh kosong");
            } else {
                $saldo[$i] = str_replace(",", "", $saldo[$i]);
                $debit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$bank . $perusahaan_prefix)->first();

        if ($akun != NULL) {
            DB::table("jurnal_umum")->insert([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => $debit,
                "kredit" => 0
            ]);


            for ($i = 0; $i < $count; $i++) {
                $akun_kredit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_kredit->kode_akun,
                    "id_kode_akun" => $akun_kredit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => 0,
                    "kredit" => $saldo[$i],
                ]);
            }

            return redirect('accounting/bankMasuk/add')->with("success", "Bank Masuk berhasil ditambahkan");
        } else {
            return redirect('accounting/bankMasuk/add')->with("error", "Transaksi Gagal");
        }
    }

    public function viewEditBankIn($id)
    {
        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
            ->orderBy('kode_akun', 'asc')->get();

        $bank_in = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $bank_in->bukti_transaksi)->where('id', '!=', $bank_in->id)->get();

        return view('ubahbankmasuk', compact('bank_in', 'akun', 'jurnals'));
    }

    public function processEditBankIn(Request $request)
    {
        $dt_jurnal = JurnalUmum::find($request->id_jurnal_old);

        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");

        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $debit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/bankMasuk/edit/' . $dt_jurnal->id)->with("error", "Detail Akun tidak boleh kosong");
            } else {
                $saldo[$i] = str_replace(",", "", $saldo[$i]);
                $debit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$bank . $perusahaan_prefix)->first();


        if ($akun != NULL) {
            JurnalUmum::where('bukti_transaksi', $dt_jurnal->bukti_transaksi)->delete(); 

            $bank = DB::table("jurnal_umum")->insertGetId([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => $debit,
                "kredit" => 0
            ]);


            for ($i = 0; $i < $count; $i++) {
                $akun_kredit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_kredit->kode_akun,
                    "id_kode_akun" => $akun_kredit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => 0,
                    "kredit" => $saldo[$i],
                ]);
            }

            return redirect('accounting/bankMasuk/edit/' . $bank)->with("success", "Bank Masuk berhasil diubah");
        } else {
            return redirect('accounting/bankMasuk/edit/' . $dt_jurnal->id)->with("error", "Transaksi Gagal");
        }
    }

    public function deleteBankIn($id)
    {
        $bank_in = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $bank_in->bukti_transaksi)->delete();

        return back();
    }

    // Bank Keluar
    public function viewBankOut(Request $request)
    {
        $transaction_type = "all";
        $startDate = Carbon::now();
        $endDate = Carbon::now();
        if ($request->start_date && $request->end_date) {
            $startDate = Carbon::parse($request->start_date);
            $endDate = Carbon::parse($request->end_date);
        }
        $startDate = $startDate->startOfDay();
        $endDate = $endDate->endOfDay();

        if ($request->jenis) {
            $transaction_type = $request->jenis;
        }

        if($transaction_type != "all")
        {
            $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
            $kode_akun[] = Akun::$bank . $perusahaan_prefix;
        }else{
            $kode_akun[] = Akun::$bank;
            $kode_akun[] = Akun::$bank . Akun::$perusahaan_prefix;
        }

        $akun = Akun::whereIn('kode_akun', $kode_akun)->pluck('id');

        $bank_out = DB::table('jurnal_umum')
            ->select('id', 'tanggal', 'keterangan', 'bukti_transaksi', 'debit', 'kredit')
            ->whereIn('id_kode_akun', $akun)
            ->orderBy('tanggal', 'asc')
            ->where('debit', '0')
            ->where('bukti_transaksi', 'LIKE', 'BBK%')
            ->whereBetween('tanggal', [$startDate, $endDate])
            ->get();

        return view('bankkeluar', compact('bank_out', 'transaction_type', 'startDate', 'endDate'));
    }

    public function addBankOut()
    {
        $now = Carbon::now();
        $year = $now->format('Y');
        $month = $now->format('m');


        $like_str = 'BBK/' . $year . '/' . $month . '/%';

        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
        ->orderBy('kode_akun', 'asc')->get();

        $bukti_transaksi = DB::table('jurnal_umum')
        ->select('bukti_transaksi')
        ->where('bukti_transaksi', 'LIKE', $like_str)
            ->distinct()
            ->orderByDesc('bukti_transaksi')
            ->first();

        $carbon = Carbon::now();
        if ($bukti_transaksi == null) {
            $bukti_transaksi = 'BBK/' . $year . '/' . $month . '/' . str_pad("1", 4, '0', STR_PAD_LEFT);
        } else {
            $bukti_transaksi = $bukti_transaksi->bukti_transaksi;
            $increment = (int)substr($bukti_transaksi, 12, 4);
            $increment += 1;

            $bukti_transaksi = 'BBK/' . $year . '/' . $month . '/' . str_pad($increment, 4, '0', STR_PAD_LEFT);
        }


        return view('tambahbankkeluar', compact('akun', 'bukti_transaksi'));
    }

    public function processBankOut(Request $request)
    {
        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");

        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $kredit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/bankKeluar/add')->with("error", "Detail Akun tidak boleh kosong");
            } else {
                $saldo[$i] = str_replace(",", "", $saldo[$i]);
                $kredit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$bank . $perusahaan_prefix)->first();

        if ($akun != NULL) {
            for ($i = 0; $i < $count; $i++) {
                $akun_debit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_debit->kode_akun,
                    "id_kode_akun" => $akun_debit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => $saldo[$i],
                    "kredit" => 0,
                ]);
            }

            DB::table("jurnal_umum")->insert([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => 0,
                "kredit" => $kredit
            ]);

            return redirect('accounting/bankKeluar/add')->with("success", "Bank Keluar berhasil ditambahkan");
        } else {
            return redirect('accounting/bankKeluar/add')->with("error", "Transaksi Gagal");
        }
    }

    public function viewEditBankOut($id)
    {
        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
            ->orderBy('kode_akun', 'asc')->get();

        $bank_out = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $bank_out->bukti_transaksi)->where('id', '!=', $bank_out->id)->get();

        return view('ubahbankkeluar', compact('bank_out', 'akun', 'jurnals'));
    }

    public function processEditBankOut(Request $request)
    {
        $dt_jurnal = JurnalUmum::find($request->id_jurnal_old);

        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");

        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $kredit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/bankKeluar/edit/' . $dt_jurnal->id)->with("error", "Detail Akun tidak boleh kosong");
            } else {
                $saldo[$i] = str_replace(",", "", $saldo[$i]);
                $kredit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$bank . $perusahaan_prefix)->first();

        if ($akun != NULL) {
            JurnalUmum::where('bukti_transaksi', $dt_jurnal->bukti_transaksi)->delete(); 

            for ($i = 0; $i < $count; $i++) {
                $akun_debit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_debit->kode_akun,
                    "id_kode_akun" => $akun_debit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => $saldo[$i],
                    "kredit" => 0,
                ]);
            }

            $bank = DB::table("jurnal_umum")->insertGetId([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => 0,
                "kredit" => $kredit
            ]);

            return redirect('accounting/bankKeluar/edit/' . $bank )->with("success", "Bank Keluar berhasil diubah");
        } else {
            return redirect('accounting/bankKeluar/edit/' . $dt_jurnal->id)->with("error", "Transaksi Gagal");
        }
    }

    public function deleteBankOut($id)
    {
        $bank_out = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $bank_out->bukti_transaksi)->delete();

        return back();
    }

    // Detail Jurnal
    public function viewDetailJurnal(Request $request, $id)
    {
        $jurnal = JurnalUmum::find($id);
        $jurnal->tanggal = date('Y-m-d', strtotime($jurnal->tanggal));
        $jurnals = JurnalUmum::with('kodeAkun')->where('bukti_transaksi', $jurnal->bukti_transaksi)->where('id', '!=', $jurnal->id)->get();
        return response()->json(['jurnal' => $jurnal, 'jurnals' => $jurnals]);
    }

    // Kas Masuk
    public function viewKasIn(Request $request)
    {
        $transaction_type = "all";
        $startDate = Carbon::now();
        $endDate = Carbon::now();
        if ($request->start_date && $request->end_date) {
            $startDate = Carbon::parse($request->start_date);
            $endDate = Carbon::parse($request->end_date);
        }
        $startDate = $startDate->startOfDay();
        $endDate = $endDate->endOfDay();

        if($request->jenis){
            $transaction_type = $request->jenis;
        }

        $kode_akun = [];

        if ($transaction_type != "all") {
            $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
            $kode_akun[] = Akun::$kas . $perusahaan_prefix;
        } else {
            $kode_akun[] = Akun::$kas;
            $kode_akun[] = Akun::$kas . Akun::$perusahaan_prefix;
        }

        $akun = Akun::whereIn('kode_akun', $kode_akun)->pluck('id');
        
        $kas_in = DB::table('jurnal_umum')
            ->select('id','tanggal', 'keterangan', 'bukti_transaksi', 'debit', 'kredit')
            ->whereIn('id_kode_akun', $akun)
            ->orderBy('tanggal', 'asc')
            ->where('kredit', '0')
            ->where('bukti_transaksi', 'LIKE', 'BKM%')
            ->whereBetween('tanggal', [$startDate, $endDate])
            ->get();

        return view('kasmasuk', compact('kas_in', 'transaction_type', 'startDate', 'endDate'));
    }

    public function addKasIn()
    {
        $now = Carbon::now();
        $year = $now->format('Y');
        $month = $now->format('m');


        $like_str = 'BKM/' . $year . '/' . $month . '/%';

        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
            ->orderBy('kode_akun', 'asc')->get();

        $bukti_transaksi = DB::table('jurnal_umum')
        ->select('bukti_transaksi')
        ->where('bukti_transaksi', 'LIKE', $like_str)
        ->distinct()
        ->orderByDesc('bukti_transaksi')
        ->first();

        $carbon = Carbon::now();
        if ($bukti_transaksi == null) {
            $bukti_transaksi = 'BKM/' . $year . '/' . $month . '/' . str_pad("1", 4, '0', STR_PAD_LEFT);
        } else {
            $bukti_transaksi = $bukti_transaksi->bukti_transaksi;
            $increment = (int)substr($bukti_transaksi, 12, 4);
            $increment += 1;

            $bukti_transaksi = 'BKM/' . $year . '/' . $month . '/' . str_pad($increment, 4, '0', STR_PAD_LEFT);
        }


        return view('tambahkasmasuk', compact('akun', 'bukti_transaksi'));
    }

    public function processKasIn(Request $request)
    {
        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");
        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $debit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/kasMasuk/add')->with("error", "Detail Akun tidak boleh kosong");
            } else {
                // $saldo[$i] = str_replace(",","",$saldo[$i]);
                $debit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$kas . $perusahaan_prefix)->first();

        if($akun != NULL)
        {
            DB::table("jurnal_umum")->insert([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => $debit,
                "kredit" => 0
            ]);


            for ($i = 0; $i < $count; $i++) {
                $akun_kredit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_kredit->kode_akun,
                    "id_kode_akun" => $akun_kredit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => 0,
                    "kredit" => $saldo[$i],
                ]);
            }

            return redirect('accounting/kasMasuk/add')->with("success", "Kas Masuk berhasil ditambahkan");
        }else{
            return redirect('accounting/kasMasuk/add')->with("error", "Transaksi Gagal");
        }
        
    }

    public function viewEditKasIn($id)
    {
        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
            ->orderBy('kode_akun', 'asc')->get();

        $kas_in = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $kas_in->bukti_transaksi)->where('id', '!=', $kas_in->id)->get();

        return view('ubahkasmasuk', compact('kas_in', 'akun', 'jurnals'));
    }

    public function processEditKasIn(Request $request)
    {
        $dt_jurnal = JurnalUmum::find($request->id_jurnal_old); 

        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");
        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $debit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/kasMasuk/edit/' . $dt_jurnal->id)->with("error", "Detail Akun tidak boleh kosong");
            } else {
                $saldo[$i] = str_replace(",","", $saldo[$i]);
                $debit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$kas . $perusahaan_prefix)->first();

        if ($akun != NULL) {
            JurnalUmum::where('bukti_transaksi', $dt_jurnal->bukti_transaksi)->delete(); 

            $kas = DB::table("jurnal_umum")->insertGetId([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => $debit,
                "kredit" => 0
            ]);


            for ($i = 0; $i < $count; $i++) {
                $akun_kredit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_kredit->kode_akun,
                    "id_kode_akun" => $akun_kredit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => 0,
                    "kredit" => $saldo[$i],
                ]);
            }

            return redirect('accounting/kasMasuk/edit/' . $kas)->with("success", "Kas Masuk berhasil diubah");
        } else {
            return redirect('accounting/kasMasuk/edit/' . $dt_jurnal->id)->with("error", "Transaksi Gagal");
        }
    }

    public function deleteKasIn($id)
    {
        $kas_in = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $kas_in->bukti_transaksi)->delete();

        return back();
    }

    // Kas Keluar
    public function viewKasOut(Request $request)
    {
        $transaction_type = "all";
        $startDate = Carbon::now();
        $endDate = Carbon::now();
        if ($request->start_date && $request->end_date) {
            $startDate = Carbon::parse($request->start_date);
            $endDate = Carbon::parse($request->end_date);
        }
        $startDate = $startDate->startOfDay();
        $endDate = $endDate->endOfDay();

        if ($request->jenis) {
            $transaction_type = $request->jenis;
        }

        $kode_akun = [];

        if ($transaction_type != "all") {
            $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
            $kode_akun[] = Akun::$kas . $perusahaan_prefix;
        } else {
            $kode_akun[] = Akun::$kas;
            $kode_akun[] = Akun::$kas . Akun::$perusahaan_prefix;
        }

        $akun = Akun::whereIn('kode_akun', $kode_akun)->pluck('id');

        $kas_out = DB::table('jurnal_umum')
            ->select('id', 'tanggal', 'keterangan', 'bukti_transaksi', 'debit', 'kredit')
            ->whereIn('id_kode_akun', $akun)
            ->orderBy('tanggal', 'asc')
            ->where('debit', '0')
            ->where('bukti_transaksi', 'LIKE', 'BKK%')
            ->whereBetween('tanggal', [$startDate, $endDate])
            ->get();

        return view('kaskeluar', compact('kas_out', 'transaction_type', 'startDate', 'endDate'));
    }

    public function addKasOut()
    {
        $now = Carbon::now();
        $year = $now->format('Y');
        $month = $now->format('m');


        $like_str = 'BKK/' . $year . '/' . $month . '/%';

        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
            ->orderBy('kode_akun', 'asc')->get();

        $bukti_transaksi = DB::table('jurnal_umum')
        ->select('bukti_transaksi')
        ->where('bukti_transaksi', 'LIKE', $like_str)
            ->distinct()
            ->orderByDesc('bukti_transaksi')
            ->first();

        $carbon = Carbon::now();
        if ($bukti_transaksi == null) {
            $bukti_transaksi = 'BKK/' . $year . '/' . $month . '/' . str_pad("1", 4, '0', STR_PAD_LEFT);
        } else {
            $bukti_transaksi = $bukti_transaksi->bukti_transaksi;
            $increment = (int)substr($bukti_transaksi, 12, 4);
            $increment += 1;

            $bukti_transaksi = 'BKK/' . $year . '/' . $month . '/' . str_pad($increment, 4, '0', STR_PAD_LEFT);
        }


        return view('tambahkaskeluar', compact('akun', 'bukti_transaksi'));
    }

    public function processKasOut(Request $request)
    {
        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");

        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $kredit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/kasKeluar/add')->with("error", "Detail Akun tidak boleh kosong");
            } else {
                $saldo[$i] = str_replace(",", "", $saldo[$i]);
                $kredit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$kas . $perusahaan_prefix)->first();

        if ($akun != NULL) {
            for ($i = 0; $i < $count; $i++) {
                $akun_debit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_debit->kode_akun,
                    "id_kode_akun" => $akun_debit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => $saldo[$i],
                    "kredit" => 0,
                ]);
            }
            
            DB::table("jurnal_umum")->insert([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => 0,
                "kredit" => $kredit
            ]);

            return redirect('accounting/kasKeluar/add')->with("success", "Kas Keluar berhasil ditambahkan");
        } else {
            return redirect('accounting/kasKeluar/add')->with("error", "Transaksi Gagal");
        }
    }

    public function viewEditKasOut($id)
    {
        $akun = DB::table('akun')->select(['id', 'kode_akun', 'nama'])
            ->orderBy('kode_akun', 'asc')->get();

        $kas_out = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $kas_out->bukti_transaksi)->where('id', '!=', $kas_out->id)->get();

        return view('ubahkaskeluar', compact('kas_out', 'akun', 'jurnals'));
    }

    public function processEditKasOut(Request $request)
    {
        $dt_jurnal = JurnalUmum::find($request->id_jurnal_old);

        $bukti_transaksi = $request->input("bukti_transaksi");
        $tanggal = $request->input("tanggal");

        $kode_akun = $request->input("kode_akun");
        $keterangan = $request->input("keterangan");
        $saldo = $request->input("saldo");

        $count = count($kode_akun);

        $kredit = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($kode_akun[$i] == null || $keterangan[$i] == null || $saldo[$i] == null) {
                return redirect('accounting/kasKeluar/edit/' . $dt_jurnal->id)->with("error", "Detail Akun tidak boleh kosong");
            } else {
                $saldo[$i] = str_replace(",", "", $saldo[$i]);
                $kredit += $saldo[$i];
            }
        }

        $perusahaan_prefix = $request->perusahaan == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $akun = Akun::where('kode_akun', Akun::$kas . $perusahaan_prefix)->first();

        if ($akun != NULL) {
            JurnalUmum::where('bukti_transaksi', $dt_jurnal->bukti_transaksi)->delete(); 

            for ($i = 0; $i < $count; $i++) {
                $akun_debit = Akun::find($kode_akun[$i]);

                DB::table("jurnal_umum")->insert([
                    "bukti_transaksi" => $bukti_transaksi,
                    "tanggal" => $tanggal,
                    "kode_akun" => $akun_debit->kode_akun,
                    "id_kode_akun" => $akun_debit->id,
                    "keterangan" => $keterangan[$i],
                    "debit" => $saldo[$i],
                    "kredit" => 0,
                ]);
            }

            $kas = DB::table("jurnal_umum")->insertGetId([
                "bukti_transaksi" => $bukti_transaksi,
                "tanggal" => $tanggal,
                "kode_akun" => $akun->kode_akun,
                "id_kode_akun" => $akun->id,
                "keterangan" => join(",", $request->input("keterangan")),
                "debit" => 0,
                "kredit" => $kredit
            ]);

            return redirect('accounting/kasKeluar/edit/' . $kas)->with("success", "Kas Keluar berhasil diubah");
        } else {
            return redirect('accounting/kasKeluar/edit/' . $dt_jurnal->id)->with("error", "Transaksi Gagal");
        }
    }

    public function deleteKasOut($id)
    {
        $kas_out = JurnalUmum::find($id);
        $jurnals = JurnalUmum::where('bukti_transaksi', $kas_out->bukti_transaksi)->delete();

        return back();
    }

    // Buku Besar
    public function viewBukuBesar(Request $request)
    {
        // ====== Filter Date ====== //
        $start_date = Carbon::now()->format('Y-m-01');
        $end_date = Carbon::now()->format('Y-m-t');

        if ($request->bulan) {
            $start_date = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y-m-01');
            $end_date = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y-m-t');
        }
        if ($request->start_date && $request->end_date) {
            $start_date = Carbon::create($request->start_date)->format('Y-m-d');
            $end_date = Carbon::create($request->end_date)->format('Y-m-d');
        }

        $start_date = $start_date . " 00:00:00";
        $end_date = $end_date . " 23:59:59";
        // ========================= //

        // Filter Barang
        $filter_barangs = $request->barangs ?? [];

        // Filter Search
        $filter_search = $request->search;

        $arr_transaction_type = $request->transaction_type == "all" ? [1, 2] : [$request->transaction_type ?? 1];

        // Filter Akuns
        $filter_akuns = $request->akuns ?? Akun::whereIn('perusahaan', $arr_transaction_type)->pluck('kode_akun')->toArray();

        $buku_besars = $this->getAkunBukuBesar($filter_akuns , $arr_transaction_type, $start_date, $end_date);

        if ($filter_search != null) {
            foreach ($buku_besars as $keyBukuBesar => $buku_besar) {
                if (! (strpos($buku_besar['nama']." ".$buku_besar['kode_akun'], $filter_search) !== false)) {
                    foreach ($buku_besar['jurnals'] as $keyBbj => $bbj) {
                        if (! (strpos(implode(" ", $bbj->toArray()), $filter_search) !== false)) 
                            unset($buku_besars[$keyBukuBesar]['jurnals'][$keyBbj]);
                    }
                    if (count($buku_besars[$keyBukuBesar]['jurnals']) == 0) unset($buku_besars[$keyBukuBesar]);
                }
            }
        }

        $data = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'buku_besars' => $buku_besars,
            'export_type' => $request->type
        ];
        
        if ($request->type == 'print') {
            // ================= Print ================== //
            return view('printbukubesar', $data);
        } else if ($request->type == 'pdf') {
            // ================= PDF ================== //
            $pdf = PDF::loadView('excelbukubesar', $data);
            return $pdf->download('Buku Besar ' . now() . '.pdf');
        } else if ($request->type == 'export') {
            // ================= Export ================== //
            return Excel::download(new ExportBukuBesar($data), 'Buku Besar ' . now() . '.xlsx');
        } else {
            // ================= View ================== //
            $data['akuns'] = Akun::all();
            $data['barangs'] = BarangJadi::all();
            return view('bukubesar', $data);
        }
    }

    public function getAkunBukuBesar($akuns, $transaction_types, $start_date, $end_date)
    {
        $subEndOfMonth = Carbon::parse($start_date)->subDay();
        $akuns = is_array($akuns) ? $akuns : [$akuns];
        $transaction_types = is_array($transaction_types) ? $transaction_types : [$transaction_types];
        $buku_besars = [];

        foreach ($akuns as $akun) {
            foreach($transaction_types as $transaction_type){
                $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
                $status_pajak = $transaction_type == Akun::$perusahaan_ppn ? 'ppn' : 'non_ppn';
                $transaction_query_table = $transaction_type == Akun::$perusahaan_ppn ? "transactions" : "transaction_non_ppn as transactions";

                $query_akun = Akun::where('kode_akun', $akun)->first();
                
                $saldo_awal = 0;
                $saldo_akhir_ = 0;

                $jurnals = DB::table('jurnals')
                ->select('tanggal', 'keterangan', 'bukti_transaksi', 'debit', 'kredit')
                ->where('id_kode_akun', '=', $query_akun->id)
                ->whereBetween('tanggal', [$start_date, $end_date])
                ->orderBy('tanggal', 'asc')
                ->get();
                
                $saldo_awal = SaldoAkhirController::getSaldoAkhir($query_akun, $start_date);
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;
                $saldo_awal_first = $saldo_akhir;

                $trans_buku_besar = [];
                $temp_jurnals = [];

                // =============== Aktiva Tetap ============== //
                if ($akun == Akun::$aktiva_tetap . $perusahaan_prefix) {
                    $jurnals = AktivaTetap::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, '0' as debit, nominal as kredit")->whereBetween('tanggal', [$start_date, $end_date])
                        ->whereHas('akun', function ($query) use ($transaction_type) {
                            $query->where('perusahaan', $transaction_type);
                        })->get();
                }

                $locked = [Akun::$kendaraan . $perusahaan_prefix, Akun::$akumulasi_penyusutan_kendaraan . $perusahaan_prefix, Akun::$peralatan_toko . $perusahaan_prefix, Akun::$akumulasi_penyusutan_peralatan_toko . $perusahaan_prefix];

                if (in_array($akun, $locked)) {
                    $jurnals = AktivaTetap::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, nominal as debit, '0' as kredit")->where('kode', $akun)->whereBetween('tanggal', [$start_date, $end_date])
                        ->whereHas('akun', function ($query) use ($transaction_type) {
                            $query->where('perusahaan', $transaction_type);
                        })->get();
                }
                // =========================================== //

                // =============== Aktiva Lancar ============= //
                if ($akun == Akun::$aktiva_lancar . $perusahaan_prefix) {
                    $jurnals = AktivaLancar::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, biaya as debit, '0' as kredit")->whereBetween('tanggal', [$start_date, $end_date])
                        ->whereHas('akun', function ($query) use ($transaction_type) {
                            $query->where('perusahaan', $transaction_type);
                        })->get();
                }
                // =========================================== //

                // ================= Sewa DBM & Uang Muka Pembelian ================ //
                $not_locked = [Akun::$sewa_dbm . $perusahaan_prefix, Akun::$uang_muka_pembelian . $perusahaan_prefix];
                if (in_array($akun, $not_locked)) {
                    $aktiva_lancar = AktivaLancar::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, '0' as debit, biaya as kredit")->where('kode', $akun)->whereBetween('tanggal', [$start_date, $end_date])
                        ->whereHas('akun', function ($query) use ($transaction_type) {
                            $query->where('perusahaan', $transaction_type);
                        })->get();

                    $jurnals = $jurnals->merge($aktiva_lancar)->sortBy('tanggal');
                }
                // ================================================================ //

                // ==================== Kas =================== //
                if ($akun == Akun::$kas . $perusahaan_prefix) {
                    $piutang_dagang = Receivable::selectRaw('receivable.id, receivable.kode_transaksi')
                        ->whereBetween('receivable.tanggal_transaksi', [$start_date, $end_date])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->where('receivable.metode_bayar', 'kas')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $receivable = Receivable::selectRaw("receivable.id, receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.total_barang) as debit, '0' as kredit")
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                        ->where('receivable.metode_bayar', 'kas')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, receivable.bayar as debit, '0' as kredit")
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['hutang', 'lunas'])
                        ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                        ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                        ->where('receivable.bayar', '>', 0)
                        ->where('receivable.metode_bayar', 'kas')
                        ->whereExists(function ($query) use ($transaction_query_table) {
                            $query->selectRaw('1')
                                ->from($transaction_query_table)
                                ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                        })->get();

                    $retur_pembelian = [];
                    // $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, retur_supplier.harga_modal as debit, '0' as kredit")
                    //     ->whereBetween('retur_supplier.created_at', [$start_date, $end_date])
                    //     ->join('products', 'products.id', 'retur_supplier.id_product')
                    //     ->where('retur_supplier.metode_bayar', 'kas')
                    //     ->where('products.status_pajak', $status_pajak)->get();

                    $persediaan_barang = Supply::selectRaw("supplies.created_at as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, '0' as debit, SUM(supplies.discounted_total) as kredit")
                        ->whereBetween('supplies.created_at', [$start_date, $end_date])
                        ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                        ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                        ->where('supplies.status', 'lunas')
                        ->where('payable.status', '!=', 'batal')
                        ->where('products.status_pajak', $status_pajak)
                        ->where('payable.metode_bayar', 'kas')
                        ->groupBy('supplies.kode_pasok')->get();

                    $retur_penjualan = [];
                    // $retur_penjualan = ReturCustomer::selectRaw("product_retur_customer.tanggal_transaksi as tanggal, retur_customer.keterangan as keterangan, product_retur_customer.kode_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                    //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    //     ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'kas' LIMIT 1) receivable"), function ($join) {
                    //         $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                    //     })
                    //     ->whereBetween('product_retur_customer.tanggal_transaksi', [$start_date, $end_date])
                    //     ->where('product_retur_customer.status_pajak', $status_pajak)
                    //     ->groupBy('product_retur_customer.kode_transaksi')->get();

                    $temp_jurnals = $jurnals->merge($pelunasan_piutang_dagang)
                        ->merge($receivable)
                        ->merge($persediaan_barang)
                        ->merge($retur_penjualan)
                        ->merge($retur_pembelian);
                    // $jurnals = $jurnals->merge($pelunasan_piutang_dagang)
                    //     ->merge($receivable)
                    //     ->merge($persediaan_barang)
                    //     ->merge($retur_penjualan)
                    //     ->merge($retur_pembelian)
                    //     ->sortBy('tanggal');
                }
                // ============================================ //

                // ==================== Bank ================== //
                if ($akun == Akun::$bank . $perusahaan_prefix) {
                    $bank_debit = Bank::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, nominal as debit, '0' as kredit")
                        ->whereBetween('tanggal', [$start_date, $end_date])->where('jenis', 'in')->get();

                    $bank_kredit = Bank::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, '0' as debit, nominal as kredit")
                        ->whereBetween('tanggal', [$start_date, $end_date])->where('jenis', 'out')->get();

                    $piutang_dagang = Receivable::selectRaw('receivable.id, receivable.kode_transaksi')
                        ->whereBetween('receivable.tanggal_transaksi', [$start_date, $end_date])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->where('receivable.metode_bayar', 'bank')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $receivable = Receivable::selectRaw("receivable.id, receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.total_barang) as debit, '0' as kredit")
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                        ->where('receivable.metode_bayar', 'bank')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, receivable.bayar as debit, '01' as kredit")
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['hutang', 'lunas'])
                        ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                        ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                        ->where('receivable.bayar', '>', 0)
                        ->where('receivable.metode_bayar', 'bank')
                        ->whereExists(function ($query) use ($transaction_query_table) {
                            $query->selectRaw('1')
                                ->from($transaction_query_table)
                                ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                        })->get();

                    $retur_pembelian = [];
                    // $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, retur_supplier.harga_modal as debit, '0' as kredit")
                    //     ->whereBetween('retur_supplier.created_at', [$start_date, $end_date])
                    //     ->join('products', 'products.id', 'retur_supplier.id_product')
                    //     ->where('retur_supplier.metode_bayar', 'bank')
                    //     ->where('products.status_pajak', $status_pajak)->get();

                    $persediaan_barang = Supply::selectRaw("supplies.created_at as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, '0' as debit, SUM(supplies.discounted_total) as kredit")
                        ->whereBetween('supplies.created_at', [$start_date, $end_date])
                        ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                        ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                        ->where('supplies.status', 'lunas')
                        ->where('payable.status', '!=', 'batal')
                        ->where('products.status_pajak', $status_pajak)
                        ->where('payable.metode_bayar', 'bank')
                        ->groupBy('supplies.kode_pasok')->get();

                    $retur_penjualan = [];
                    // $retur_penjualan = ReturCustomer::selectRaw("product_retur_customer.tanggal_transaksi as tanggal, retur_customer.keterangan as keterangan, product_retur_customer.kode_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                    //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    //     ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'bank' LIMIT 1) receivable"), function ($join) {
                    //         $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                    //     })
                    //     ->whereBetween('product_retur_customer.tanggal_transaksi', [$start_date, $end_date])
                    //     ->where('product_retur_customer.status_pajak', $status_pajak)
                    //     ->groupBy('product_retur_customer.kode_transaksi')->get();

                    $temp_jurnals = $jurnals->merge($bank_debit)
                        ->merge($bank_kredit)
                        ->merge($pelunasan_piutang_dagang)
                        ->merge($receivable)
                        ->merge($persediaan_barang)
                        ->merge($retur_penjualan)
                        ->merge($retur_pembelian);
                    // $jurnals = $jurnals->merge($bank_debit)
                    //     ->merge($bank_kredit)
                    //     ->merge($penulansan_piutang_dagang)
                    //     ->merge($receivable)
                    //     ->merge($persediaan_barang)
                    //     ->merge($retur_penjualan)
                    //     ->merge($retur_pembelian)
                    //     ->sortBy('tanggal');
                }
                // =========================================== //

                // ==================== Penjualan ================== //
                if ($akun == Akun::$penjualan . $perusahaan_prefix) {
                    $piutang_dagang = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit")
                        ->selectRaw("SUM(transactions.total_barang) + SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as kredit")
                        ->whereBetween('receivable.tanggal_transaksi', [$start_date, $end_date])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $receivable = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit")
                        ->selectRaw("SUM(transactions.total_barang) + SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as kredit")
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();


                    $temp_jurnals = $jurnals->merge($piutang_dagang)->merge($receivable);
                    // $jurnals = $jurnals->merge($piutang_dagang)->merge($receivable)->sortBy('tanggal');
                }
                // ================================================= //

                // ==================== Potongan Penjualan ================== //
                if ($akun == Akun::$potongan_penjualan . $perusahaan_prefix) {
                    $piutang_dagang = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as kredit")
                        ->selectRaw("SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as debit")
                        ->whereBetween('receivable.tanggal_transaksi', [$start_date, $end_date])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->where('transactions.diskon', '!=', 0)
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();
                    $receivable = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as kredit")
                        ->selectRaw("SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as debit")
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                        ->where('transactions.diskon', '!=', 0)
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $temp_jurnals = $jurnals->merge($piutang_dagang)->merge($receivable);
                    // $jurnals = $jurnals->merge($piutang_dagang)->merge($receivable)->sortBy('tanggal');
                }
                // ========================================================== //

                // ==================== Persediaan Barang ================== //
                if ($akun == Akun::$persediaan_barang . $perusahaan_prefix) {
                    $persediaan_barang = Supply::selectRaw("supplies.created_at as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, SUM(supplies.discounted_total) as debit, '0' as kredit")
                        ->whereBetween('supplies.created_at', [$start_date, $end_date])
                        ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                        ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                        ->where('supplies.status', 'lunas')
                        ->where('payable.status', '!=', 'batal')        
                        ->where('products.status_pajak', $status_pajak)
                        ->groupBy('supplies.kode_pasok')->get();

                    $temp_jurnals = $jurnals->merge($persediaan_barang)->sortBy('tanggal');
                }
                // =========================================== //

                // ==================== Retur Penjualan ================== //
                // if ($akun == Akun::$retur_penjualan . $perusahaan_prefix) {
                    // $retur_penjualan = ReturCustomer::selectRaw("retur_customer.created_at as tanggal, retur_customer.keterangan as keterangan, retur_customer.no_transaksi as bukti_transaksi, SUM(product_retur_customer.total_barang) as debit, '0' as kredit")
                    //     ->whereBetween('retur_customer.created_at', [$start_date, $end_date])
                    //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    //     ->where('product_retur_customer.status_pajak', $status_pajak)
                    //     ->groupBy('product_retur_customer.kode_transaksi')->get();

                    // $jurnals = $jurnals->merge($retur_penjualan)->sortBy('tanggal');
                // }
                // =========================================== //

                // ==================== Retur Pembelian ================== //
                // if ($akun == Akun::$retur_pembelian . $perusahaan_prefix) {
                //     $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, '0' as debit, SUM(retur_supplier.harga_modal) as kredit")
                //         ->whereBetween('retur_supplier.created_at', [$start_date, $end_date])
                //         ->join('products', 'products.id', 'retur_supplier.id_product')
                //         ->where('products.status_pajak', $status_pajak)->get();

                //     $jurnals = $jurnals->merge($retur_pembelian)->sortBy('tanggal');
                // }
                // =========================================== //

                // ==================== Piutang Dagang ================== //
                if ($akun == Akun::$piutang_dagang . $perusahaan_prefix) {
                    $piutang_dagang = Receivable::selectRaw("receivable.id, receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.total_barang) as debit, '0' as kredit")
                        ->whereBetween('receivable.tanggal_transaksi', [$start_date, $end_date])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $receivable = Receivable::selectRaw('receivable.*, SUM(transactions.total_barang) as tr_total')
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit, receivable.bayar as kredit")
                        ->whereBetween('receivable.created_at', [$start_date, $end_date])
                        ->whereIn('receivable.status', ['hutang', 'lunas'])
                        ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                        ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                        ->where('receivable.bayar', '>', 0)
                        ->whereExists(function ($query) use ($transaction_query_table) {
                            $query->selectRaw('1')
                                ->from($transaction_query_table)
                                ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                        })->get();

                    $retur_penjualan = [];
                    // $retur_penjualan = ReturCustomer::selectRaw("retur_customer.created_at as tanggal, retur_customer.keterangan as keterangan, retur_customer.no_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                    //     ->whereBetween('retur_customer.created_at', [$start_date, $end_date])
                    //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    //     ->where('product_retur_customer.status_pajak', $status_pajak)
                    //     ->groupBy('product_retur_customer.kode_transaksi')->get();

                    $temp_jurnals = $jurnals->merge($piutang_dagang)->merge($pelunasan_piutang_dagang)
                        ->merge($retur_penjualan);
                    // $jurnals = $jurnals->merge($piutang_dagang)
                    //     ->merge($pelunasan_piutang_dagang)
                    //     ->merge($retur_penjualan)
                    //     ->sortBy('tanggal');
                }
                // ====================================================== //

                // ============== Penyusutan Aktiva Tetap =============== //
                if ($akun == Akun::$akumulasi_penyusutan_kendaraan . $perusahaan_prefix || $akun == Akun::$akumulasi_penyusutan_peralatan_toko . $perusahaan_prefix) {
                    if ($akun == Akun::$akumulasi_penyusutan_kendaraan . $perusahaan_prefix) {
                        $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function ($query) use ($perusahaan_prefix) {
                            $query->where('kode_akun', Akun::$kendaraan . $perusahaan_prefix);
                        })->get();
                    } else if ($akun == Akun::$akumulasi_penyusutan_peralatan_toko . $perusahaan_prefix) {
                        $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function ($query) use ($perusahaan_prefix) {
                            $query->where('kode_akun', Akun::$peralatan_toko . $perusahaan_prefix);
                        })->get();
                    }

                    foreach ($penyusutan_aktiva_tetap as $value) {
                        $checkStartDate = Carbon::parse($value->tanggal)->format('Y-m-01');
                        $checkEndDate = Carbon::parse($value->tanggal)->addMonth($value->durasi_aktiva)->format('Y-m-t');
                        $checkNowDate = Carbon::parse($start_date->format('Y') . '-' . $start_date->format('n') . '-' . date('d', strtotime($value->tanggal)))->subMonth(1);

                        // if ($checkNowDate->between($checkStartDate, $checkEndDate)) {
                        //     $startDate = Carbon::parse($checkStartDate);
                        //     $saldo_awal += ($startDate->diffInMonths($checkNowDate) * $value->penyusutan);
                        //     $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;
                        // }

                        $aktiva_tetap = (object) [
                            "tanggal" => $checkNowDate->addMonth(1)->format("Y-m-d"),
                            "keterangan" => 'Akumulasi Penyusutan',
                            "bukti_transaksi" => '',
                            "debit" => 0,
                            "kredit" => $value->penyusutan
                        ];

                        $jurnals = $jurnals->push($aktiva_tetap);
                    }
                }
                // =========================================== //

                if ($temp_jurnals) {
                    foreach ($temp_jurnals as $value) {
                        $transGroupType = date('Y-m-d', strtotime($value->tanggal));
                        if (isset($trans_buku_besar[$transGroupType])) {
                            $trans_buku_besar[$transGroupType]->debit += $value->debit; 
                            $trans_buku_besar[$transGroupType]->kredit += $value->kredit;
                        } else {
                            $trans_buku_besar[$transGroupType] = (object) [ 
                                'tanggal' => $value->tanggal, 
                                'keterangan' => '', 
                                'bukti_transaksi' => '',
                                'debit' => $value->debit,
                                'kredit' => $value->kredit, 
                            ];
                        }
                    }
                    $jurnals = collect(array_values($trans_buku_besar))->sortBy('tanggal');
                }

                foreach ($jurnals as $jurnal) {
                    $saldo_awal = $saldo_akhir;
                    if (Akun::cekSaldoNormal($query_akun) == 'debit') {
                        $saldo_akhir = $saldo_awal + $jurnal->debit - $jurnal->kredit;
                    } else {
                        $saldo_akhir = $saldo_awal + $jurnal->kredit - $jurnal->debit;
                    }

                    $jurnal->saldo_awal = $saldo_awal;
                    $jurnal->saldo_akhir = $saldo_akhir;
                }
            }

            $akun_jurnal = collect([
                "nama" => $query_akun->nama,
                "kode_akun" => $query_akun->kode_akun,
                "saldo_awal" => $saldo_awal_first,
                "jurnals" => $jurnals
            ]);

            array_push($buku_besars, $akun_jurnal);
        }

        $buku_besars = collect($buku_besars);

        return $buku_besars;
    }

    public function viewAkunBukuBesar(Request $request)
    {
        $now = Carbon::now();

        $startOfMonth = $now->copy()->startOfMonth();
        $endOfMonth = $now->copy()->endOfMonth();
        $subEndOfMonth = $startOfMonth->subDay();

        $akun = $request->kode_akun;

        $buku_besars = [];

        foreach($akun as $a)
        {
            $query_akun = Akun::where('kode_akun', $a)->first();
            $checkAkunPerusahaan = Akun::checkAkunPerusahaan($query_akun);
            $transaction_type = $checkAkunPerusahaan['transaction_type'];
            $perusahaan_prefix = $checkAkunPerusahaan['perusahaan_prefix'];
            $transaction_query_table = $checkAkunPerusahaan['transaction_query_table'];
            $status_pajak = $checkAkunPerusahaan['status_pajak'];

            $saldo_awal = 0;
            $saldo_akhir = 0;

            $saldo_awal_debit = DB::table('jurnals')
                ->where("id_kode_akun", '=', $query_akun->id)
                ->whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])
                ->sum('debit');

            $saldo_awal_kredit = DB::table('jurnals')
                ->where("id_kode_akun", '=', $query_akun->id)
                ->whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])
                ->sum('kredit');

            $saldo_awal = $saldo_awal_debit - $saldo_awal_kredit;
            $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

            $jurnals = DB::table('jurnals')
                ->select('tanggal', 'keterangan', 'bukti_transaksi', 'debit', 'kredit')
                ->where('id_kode_akun', '=', $query_akun->id)
                ->whereBetween('tanggal', [$startOfMonth, $endOfMonth])
                ->orderBy('tanggal', 'asc')
                ->get();

            // =============== Aktiva Tetap ============== //
            if($a == Akun::$aktiva_tetap.$perusahaan_prefix){
                $saldo_awal = AktivaTetap::whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])
                    ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->sum("nominal");
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $jurnals = AktivaTetap::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, '0' as debit, nominal as kredit")->whereBetween('tanggal', [$startOfMonth, $endOfMonth])
                    ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();
            }

            $locked = [Akun::$kendaraan.$perusahaan_prefix, Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix, Akun::$peralatan_toko.$perusahaan_prefix, Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix];

            if(in_array($a, $locked)){
                $saldo_awal = AktivaTetap::where('kode', $a)->whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])->sum("nominal");
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $jurnals = AktivaTetap::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, nominal as debit, '0' as kredit")->where('kode', $a)->whereBetween('tanggal', [$startOfMonth, $endOfMonth])
                    ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();
            }
            // =========================================== //

            // =============== Aktiva Lancar ============= //
            if ($a == Akun::$aktiva_lancar.$perusahaan_prefix) {
                $saldo_awal = AktivaLancar::whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])
                    ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->sum("biaya");
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $jurnals = AktivaLancar::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, biaya as debit, '0' as kredit")->whereBetween('tanggal', [$startOfMonth, $endOfMonth])
                    ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();
            }
            // =========================================== //

            // ================= Sewa DBM & Uang Muka Pembelian ================ //
            $not_locked = [Akun::$sewa_dbm.$perusahaan_prefix, Akun::$uang_muka_pembelian.$perusahaan_prefix];
            if (in_array($a, $not_locked)) {
                $saldo_awal_aktiva_lancar = AktivaLancar::whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])->where('kode', $a)->sum("biaya");

                $saldo_awal = $saldo_awal + $saldo_awal_aktiva_lancar;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $aktiva_lancar = AktivaLancar::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, '0' as debit, biaya as kredit")->where('kode', $a)->whereBetween('tanggal', [$startOfMonth, $endOfMonth])
                    ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();

                $jurnals = $jurnals->merge($aktiva_lancar)->sortBy('tanggal');
            }
            // ================================================================ //

            // ==================== Kas =================== //
            if($a == Akun::$kas.$perusahaan_prefix){
                // ================== Debit ================== //
                $piutang_dagang = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->where('receivable.status', 'hutang')
                ->where('receivable.bayar', 0)
                ->where('receivable.metode_bayar', 'kas')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $saldo_awal_kas_receivable = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                ->where('receivable.metode_bayar', 'kas')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $saldo_awal_kas_receivable_ppn = $saldo_awal_kas_receivable->sum('tr_total');

                $saldo_awal_kas_pelunansan_piutang_dagang = Receivable::whereBetween('receivable.created_at', [$query_akun->created_at, $subEndOfMonth])
                ->whereIn('receivable.status', ['hutang', 'lunas'])
                ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                ->whereNotIn('receivable.id', $saldo_awal_kas_receivable->pluck('id')->toArray())
                ->where('receivable.bayar', '>', 0)
                ->where('receivable.metode_bayar', 'kas')
                ->whereExists(function($query) use ($transaction_query_table) {
                    $query->selectRaw('1')
                        ->from($transaction_query_table)
                        ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                })->sum('receivable.bayar');

                $saldo_awal_retur_pembelian = ReturSupplier::whereBetween('retur_supplier.created_at', [$query_akun->created_at, $subEndOfMonth])
                ->join('products', 'products.id', 'retur_supplier.id_product')
                ->where('retur_supplier.metode_bayar', 'kas')
                ->where('products.status_pajak', $status_pajak)->sum('harga_modal');
                // =========================================== //

                // ================== Kredit ================= //
                $saldo_awal_kas_persediaan_barang = Supply::whereBetween('supplies.tanggal', [$query_akun->created_at, $subEndOfMonth])
                ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                ->where('products.status_pajak', $status_pajak)
                ->where('payable.metode_bayar', 'kas')
                ->sum('supplies.discounted_total');

                $saldo_awal_kas_retur_penjualan = ReturCustomer::join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'kas' LIMIT 1) receivable"), function ($join) {
                    $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                })
                ->whereBetween('product_retur_customer.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->where('product_retur_customer.status_pajak', $status_pajak)
                ->sum('total_barang');
                // =========================================== // 

                $saldo_awal = $saldo_awal + $saldo_awal_kas_receivable_ppn +  $saldo_awal_kas_pelunansan_piutang_dagang + $saldo_awal_retur_pembelian - $saldo_awal_kas_persediaan_barang - $saldo_awal_kas_retur_penjualan;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $piutang_dagang = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                ->where('receivable.status', 'hutang')
                ->where('receivable.bayar', 0)
                ->where('receivable.metode_bayar', 'kas')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $receivable = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as debit, '0' as kredit")
                ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                    ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                    ->where('receivable.metode_bayar', 'kas')
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, receivable.bayar as debit, '0' as kredit")
                ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                ->whereIn('receivable.status', ['hutang', 'lunas'])
                ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                ->where('receivable.bayar', '>', 0)
                ->where('receivable.metode_bayar', 'kas')
                ->whereExists(function($query) use ($transaction_query_table) {
                    $query->selectRaw('1')
                        ->from($transaction_query_table)
                        ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                })->get();

                $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, retur_supplier.harga_modal as debit, '0' as kredit")
                ->whereBetween('retur_supplier.created_at', [$startOfMonth, $endOfMonth])
                ->join('products', 'products.id', 'retur_supplier.id_product')
                ->where('retur_supplier.metode_bayar', 'kas')
                ->where('products.status_pajak', $status_pajak)->get();

                $persediaan_barang = Supply::selectRaw("payable.tanggal_pasok as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, '0' as debit, SUM(supplies.discounted_total) as kredit")
                ->whereBetween('supplies.tanggal', [$startOfMonth, $endOfMonth])
                ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                ->where('products.status_pajak', $status_pajak)
                ->where('payable.metode_bayar', 'kas')
                ->groupBy('supplies.kode_pasok')->get();

                $retur_penjualan = ReturCustomer::selectRaw("product_retur_customer.tanggal_transaksi as tanggal, retur_customer.keterangan as keterangan, product_retur_customer.kode_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'kas' LIMIT 1) receivable"), function ($join) {
                    $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                })
                ->whereBetween('product_retur_customer.tanggal_transaksi', [$startOfMonth, $subEndOfMonth])
                ->where('product_retur_customer.status_pajak', $status_pajak)
                ->groupBy('product_retur_customer.kode_transaksi')->get();

                // dd($endOfMonth);

                $jurnals = $jurnals->merge($pelunasan_piutang_dagang)
                    ->merge($receivable)
                    ->merge($persediaan_barang)
                    ->merge($retur_penjualan)
                    ->merge($retur_pembelian)
                    ->sortBy('tanggal');
            }
            // ============================================ //

            // ==================== Bank ================== //
            if($a == Akun::$bank.$perusahaan_prefix){                                
                // ================== Debit ================== //
                $saldo_awal_bank_debit = Bank::whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])->where('jenis', 'in')->sum('nominal');
                
                $piutang_dagang = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->where('receivable.status', 'hutang')
                ->where('receivable.bayar', 0)
                ->where('receivable.metode_bayar', 'bank')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $saldo_awal_bank_receivable = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                ->where('receivable.metode_bayar', 'bank')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $saldo_awal_bank_receivable_ppn = $saldo_awal_bank_receivable->sum('tr_total');

                $saldo_awal_bank_penulansan_piutang_dagang = Receivable::whereBetween('receivable.created_at', [$query_akun->created_at, $subEndOfMonth])
                ->whereIn('receivable.status', ['hutang', 'lunas'])
                ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                ->whereNotIn('receivable.id', $saldo_awal_bank_receivable->pluck('id')->toArray())
                ->where('receivable.bayar', '>', 0)
                ->where('receivable.metode_bayar', 'bank')
                ->whereExists(function($query) use ($transaction_query_table) {
                    $query->selectRaw('1')
                        ->from($transaction_query_table)
                        ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                })->sum('receivable.bayar');

                $saldo_awal_retur_pembelian = ReturSupplier::whereBetween('retur_supplier.created_at', [$query_akun->created_at, $subEndOfMonth])
                ->join('products', 'products.id', 'retur_supplier.id_product')
                ->where('retur_supplier.metode_bayar', 'bank')
                ->where('products.status_pajak', $status_pajak)->sum('harga_modal');
                // ============================================ //

                // ================== Kredit ================== //

                $saldo_awal_bank_kredit = Bank::whereBetween('tanggal', [$query_akun->created_at, $subEndOfMonth])->where('jenis', 'out')->sum('nominal');

                $saldo_awal_bank_persediaan_barang = Supply::whereBetween('supplies.tanggal', [$query_akun->created_at, $subEndOfMonth])
                ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                ->where('products.status_pajak', $status_pajak)
                ->where('payable.metode_bayar', 'bank')
                ->sum('supplies.discounted_total');

                $saldo_awal_bank_retur_penjualan = ReturCustomer::join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'bank' LIMIT 1) receivable"), function($join){
                    $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                })
                ->whereBetween('product_retur_customer.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->where('product_retur_customer.status_pajak', $status_pajak)
                ->sum('total_barang');
                // ============================================ //

                $saldo_awal = $saldo_awal + $saldo_awal_bank_debit + $saldo_awal_bank_receivable_ppn +  $saldo_awal_bank_penulansan_piutang_dagang + $saldo_awal_retur_pembelian - $saldo_awal_bank_kredit - $saldo_awal_bank_persediaan_barang - $saldo_awal_bank_retur_penjualan;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $bank_debit = Bank::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, nominal as debit, '0' as kredit")
                ->whereBetween('tanggal', [$startOfMonth, $endOfMonth])->where('jenis', 'in')->get();

                $bank_kredit = Bank::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, '0' as debit, nominal as kredit")
                ->whereBetween('tanggal', [$startOfMonth, $endOfMonth])->where('jenis', 'out')->get();

                $piutang_dagang = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                ->where('receivable.status', 'hutang')
                ->where('receivable.bayar', 0)
                ->where('receivable.metode_bayar', 'bank')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $receivable = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as debit, '0' as kredit")
                ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                ->where('receivable.metode_bayar', 'bank')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $penulansan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, receivable.bayar as debit, '0' as kredit")
                ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                ->whereIn('receivable.status', ['hutang', 'lunas'])
                ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                ->where('receivable.bayar', '>', 0)
                ->where('receivable.metode_bayar', 'bank')
                ->whereExists(function($query) use ($transaction_query_table) {
                    $query->selectRaw('1')
                        ->from($transaction_query_table)
                        ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                })->get();

                $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, retur_supplier.harga_modal as debit, '0' as kredit")
                ->whereBetween('retur_supplier.created_at', [$startOfMonth, $endOfMonth])
                ->join('products', 'products.id', 'retur_supplier.id_product')
                ->where('retur_supplier.metode_bayar', 'bank')
                ->where('products.status_pajak', $status_pajak)->get();

                $persediaan_barang = Supply::selectRaw("payable.tanggal_pasok as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, '0' as debit, SUM(supplies.discounted_total) as kredit")
                ->whereBetween('supplies.tanggal', [$startOfMonth, $endOfMonth])
                ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                ->where('products.status_pajak', $status_pajak)
                ->where('payable.metode_bayar', 'bank')
                ->groupBy('supplies.kode_pasok')->get();

                $retur_penjualan = ReturCustomer::selectRaw("product_retur_customer.tanggal_transaksi as tanggal, retur_customer.keterangan as keterangan, product_retur_customer.kode_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'bank' LIMIT 1) receivable"), function ($join) {
                    $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                })
                ->whereBetween('product_retur_customer.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                ->where('product_retur_customer.status_pajak', $status_pajak)
                ->groupBy('product_retur_customer.kode_transaksi')->get();

                $jurnals = $jurnals->merge($bank_debit)
                ->merge($bank_kredit)
                ->merge($penulansan_piutang_dagang)
                ->merge($receivable)
                ->merge($persediaan_barang)
                ->merge($retur_penjualan)
                ->merge($retur_pembelian)
                ->sortBy('tanggal');

                         
            }
            // =========================================== //

            // ==================== Penjualan ================== //
            if($a == Akun::$penjualan.$perusahaan_prefix){
                $piutang_dagang = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * transactions.harga) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->where('receivable.status', 'hutang')
                ->where('receivable.bayar', 0)
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();
                
                $saldo_awal_penjualan_receivable = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * transactions.harga) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $saldo_awal_piutang_dagang_ppn = $piutang_dagang->sum('tr_total');

                $saldo_awal_penjualan_receivable_ppn = $saldo_awal_penjualan_receivable->sum('tr_total');

                $saldo_awal = $saldo_awal_piutang_dagang_ppn + $saldo_awal_penjualan_receivable_ppn;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $piutang_dagang = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit, SUM(transactions.jumlah * transactions.harga) as kredit")
                ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                ->where('receivable.status', 'hutang')
                ->where('receivable.bayar', 0)
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $receivable = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit, SUM(transactions.jumlah * transactions.harga) as kredit")
                ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->groupBy('receivable.kode_transaksi')
                ->get();

                $jurnals = $jurnals->merge($piutang_dagang)->merge($receivable)->sortBy('tanggal');
            }
            // ================================================= //

            // ==================== Potongan Penjualan ================== //
            if($a == Akun::$potongan_penjualan.$perusahaan_prefix){

                $piutang_dagang = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * transactions.diskon) as tr_diskon')
                    ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                    ->where('receivable.status', 'hutang')
                    ->where('receivable.bayar', 0)
                    ->where('transactions.diskon', '!=', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $saldo_awal_penjualan_receivable = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * transactions.diskon) as tr_diskon')
                    ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                    ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                    ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                    ->where('transactions.diskon', '!=', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $saldo_awal_piutang_dagang_ppn = $piutang_dagang->sum('tr_diskon');

                $saldo_awal_penjualan_receivable_ppn = $saldo_awal_penjualan_receivable->sum('tr_diskon');

                $saldo_awal = $saldo_awal_piutang_dagang_ppn + $saldo_awal_penjualan_receivable_ppn;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $piutang_dagang = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.jumlah * transactions.diskon) as debit, '0' as kredit")
                    ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    ->where('receivable.status', 'hutang')
                    ->where('receivable.bayar', 0)
                    ->where('transactions.diskon', '!=', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();
                $receivable = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.jumlah * transactions.diskon) as debit, '0' as kredit")
                    ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                    ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                    ->where('transactions.diskon', '!=', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $jurnals = $jurnals->merge($piutang_dagang)->merge($receivable)->sortBy('tanggal');
            }
            // ========================================================== //

            // ==================== Persediaan Barang ================== //
            if($a == Akun::$persediaan_barang.$perusahaan_prefix){
                $saldo_awal_persediaan_barang = Supply::whereBetween('supplies.tanggal', [$query_akun->created_at, $subEndOfMonth])
                ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                ->where('products.status_pajak', $status_pajak)
                ->sum('supplies.discounted_total');

                $saldo_awal = $saldo_awal + $saldo_awal_persediaan_barang;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $persediaan_barang = Supply::selectRaw("payable.tanggal_pasok as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, SUM(supplies.discounted_total) as debit, '0' as kredit")
                ->whereBetween('supplies.tanggal', [$startOfMonth, $endOfMonth])
                ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                ->where('products.status_pajak', $status_pajak)
                ->groupBy('supplies.kode_pasok')->get();

                $jurnals = $jurnals->merge($persediaan_barang)->sortBy('tanggal');
            }
            // =========================================== //

            // ==================== Retur Penjualan ================== //
            if($a == Akun::$retur_penjualan.$perusahaan_prefix){
                $saldo_awal_retur_penjualan = ReturCustomer::whereBetween('retur_customer.created_at', [$query_akun->created_at, $subEndOfMonth])
                ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                ->where('product_retur_customer.status_pajak', $status_pajak)
                ->sum('product_retur_customer.total_barang');

                $saldo_awal = $saldo_awal + $saldo_awal_retur_penjualan;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $retur_penjualan = ReturCustomer::selectRaw("retur_customer.created_at as tanggal, retur_customer.keterangan as keterangan, retur_customer.no_transaksi as bukti_transaksi, SUM(product_retur_customer.total_barang) as debit, '0' as kredit")
                ->whereBetween('retur_customer.created_at', [$startOfMonth, $endOfMonth])
                ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                ->where('product_retur_customer.status_pajak', $status_pajak)
                ->groupBy('product_retur_customer.kode_transaksi')->get();

                $jurnals = $jurnals->merge($retur_penjualan)->sortBy('tanggal');
            }
            // =========================================== //

            // ==================== Retur Pembelian ================== //
            if($a == Akun::$retur_pembelian.$perusahaan_prefix){
                $saldo_awal_retur_pembelian = ReturSupplier::whereBetween('retur_supplier.created_at', [$query_akun->created_at, $subEndOfMonth])
                ->join('products', 'products.id', 'retur_supplier.id_product')
                ->where('products.status_pajak', $status_pajak)->sum('retur_supplier.harga_modal');

                $saldo_awal = $saldo_awal + $saldo_awal_retur_pembelian;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, '0' as debit, SUM(retur_supplier.harga_modal) as kredit")
                ->whereBetween('retur_supplier.created_at', [$startOfMonth, $endOfMonth])
                ->join('products', 'products.id', 'retur_supplier.id_product')
                ->where('products.status_pajak', $status_pajak)->get();

                $jurnals = $jurnals->merge($retur_pembelian)->sortBy('tanggal');
            }
            // =========================================== //

            // ==================== Piutang Dagang ================== //
            if ($a == Akun::$piutang_dagang.$perusahaan_prefix) {
                // ================== Debit ================== //
                $saldo_awal_piutang_dagang = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                    ->where('receivable.status', 'hutang')
                    ->where('receivable.bayar', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $saldo_awal_piutang_receivable_ppn = $saldo_awal_piutang_dagang->sum('tr_total');
                // ============================================ //

                // ================== Kredit ================== //
                $saldo_awal_receivable = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$query_akun->created_at, $subEndOfMonth])
                    ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                    ->whereNotIn('receivable.kode_transaksi', $saldo_awal_piutang_dagang->pluck('kode_transaksi')->toArray())
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $saldo_awal_pelunasan_piutang_dagang = Receivable::whereBetween('receivable.created_at', [$query_akun->created_at, $subEndOfMonth])
                    ->whereIn('receivable.status', ['hutang', 'lunas'])
                    ->whereNotIn('receivable.id', $saldo_awal_piutang_dagang->pluck('id')->toArray())
                    ->whereNotIn('receivable.id', $saldo_awal_receivable->pluck('id')->toArray())
                    ->where('receivable.bayar', '>', 0)
                    ->whereExists(function($query) use ($transaction_query_table) {
                        $query->selectRaw('1')
                            ->from($transaction_query_table)
                            ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                    })->sum('receivable.bayar');

                $saldo_awal_retur_penjualan = ReturCustomer::whereBetween('retur_customer.created_at', [$query_akun->created_at, $subEndOfMonth])
                    ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    ->where('product_retur_customer.status_pajak', $status_pajak)
                    ->sum('product_retur_customer.total_barang');
                // ============================================ //

                $saldo_awal = $saldo_awal + $saldo_awal_piutang_receivable_ppn - $saldo_awal_pelunasan_piutang_dagang - $saldo_awal_retur_penjualan;
                $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;

                $piutang_dagang = Receivable::selectRaw("receivable.id, receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as debit, '0' as kredit")
                ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    ->where('receivable.status', 'hutang')
                    ->where('receivable.bayar', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $receivable = Receivable::selectRaw('receivable.*, SUM(transactions.jumlah * (transactions.harga - transactions.diskon)) as tr_total')
                ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    ->whereIn('receivable.status', ['lunas', 'retur', 'batal'])
                    ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit, receivable.bayar as kredit")
                    ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                    ->whereIn('receivable.status', ['hutang', 'lunas'])
                    ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                    ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                    ->where('receivable.bayar', '>', 0)
                    ->whereExists(function($query) use ($transaction_query_table) {
                        $query->selectRaw('1')
                            ->from($transaction_query_table)
                            ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                    })->get();

                $retur_penjualan = ReturCustomer::selectRaw("retur_customer.created_at as tanggal, retur_customer.keterangan as keterangan, retur_customer.no_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                ->whereBetween('retur_customer.created_at', [$startOfMonth, $endOfMonth])
                    ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    ->where('product_retur_customer.status_pajak', $status_pajak)
                    ->groupBy('product_retur_customer.kode_transaksi')->get();

                $jurnals = $jurnals->merge($piutang_dagang)
                    ->merge($pelunasan_piutang_dagang)
                    ->merge($retur_penjualan)
                    ->sortBy('tanggal');
            }
            // ====================================================== //

            // ============== Penyusutan Aktiva Tetap =============== //
            if($a == Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix || $a == Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix){
                if($a == Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix)
                {
                    $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function ($query) use($perusahaan_prefix) {
                        $query->where('kode_akun', Akun::$kendaraan.$perusahaan_prefix);
                    })->get();
                }else if ($a == Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix){
                    $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function ($query) use($perusahaan_prefix) {
                        $query->where('kode_akun', Akun::$peralatan_toko.$perusahaan_prefix);
                    })->get();
                }

                $saldo_awal = 0;

                foreach ($penyusutan_aktiva_tetap as $value) {
                    $checkStartDate = Carbon::parse($value->tanggal)->format('Y-m-01');
                    $checkEndDate = Carbon::parse($value->tanggal)->addMonth($value->durasi_aktiva)->format('Y-m-t');
                    $checkNowDate = Carbon::parse($now->format('Y') . '-' . $now->format('n') . '-' . date('d', strtotime($value->tanggal)))->subMonth(1);
                
                    if ($checkNowDate->between($checkStartDate, $checkEndDate)) {
                        $startDate = Carbon::parse($checkStartDate);
                        $saldo_awal += ($startDate->diffInMonths($checkNowDate) * $value->penyusutan);
                        $saldo_akhir = $query_akun->saldo_awal + $saldo_awal;
                    }

                    $aktiva_tetap = (object) [
                        "tanggal" => $checkNowDate->addMonth(1)->format("Y-m-d"),
                        "keterangan" => 'Akumulasi Penyusutan',
                        "bukti_transaksi" => '',
                        "debit" => 0,
                        "kredit" => $value->penyusutan
                    ];

                    $jurnals = $jurnals->push($aktiva_tetap);
                }

            }
            // =========================================== //
            
            foreach($jurnals as $jurnal)
            {
                $saldo_awal = $saldo_akhir;
                if (Akun::cekSaldoNormal($query_akun) == 'debit') {
                    $saldo_akhir = $saldo_awal + $jurnal->debit - $jurnal->kredit;
                } else {
                    $saldo_akhir = $saldo_awal + $jurnal->kredit - $jurnal->debit;
                }
                
                $jurnal->saldo_awal = $saldo_awal;
                $jurnal->saldo_akhir = $saldo_akhir;
            }

            $akun_jurnal = collect([
                "nama" => $query_akun->nama,
                "kode_akun" => $query_akun->kode_akun,
                "jurnals" => $jurnals 
            ]);

            array_push($buku_besars, $akun_jurnal);
        }

        $buku_besars = collect($buku_besars);

        $data = [
            'akun' => $akun,
            'startOfMonth' => $startOfMonth,
            'endOfMonth' => $endOfMonth,
            'now' => $now,
            'buku_besars' => $buku_besars,
            'export_type' => $request->type
        ];

        if ($request->type == 'pdf') {
            // ================= PDF ================== //
            $pdf = PDF::loadView('excelbukubesar', $data);
            return $pdf->download('Buku Besar ' . now() . '.pdf');
        } else if ($request->type == 'export') {
            // ================= Export ================== //
            return Excel::download(new ExportBukuBesar($data), 'Buku Besar ' . now() . '.xlsx');
        } else {
            // ================= View ================== //
            return view('bukubesarview', $data);
        }
    }

    public function tahunBukuBesar(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required',
        ]);

        $tahun = $request->tahun;

        // $month = Carbon::createFromFormat('Y-m', $request->bulan)->format('n');
        // $years = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y');
        
        $akun = Akun::orderBy('kode_akun','asc')->
        whereYear('tanggal', $tahun)->
        get();

        $aktiva_lancar = AktivaLancar::orderBy('kode','asc')->
        whereYear('tanggal',$tahun)->
        get();
        $aktiva_tetap = AktivaTetap::orderBy('kode','asc')->
        whereYear('tanggal', $tahun)->
        get();

        $bank = Bank::orderBy('kode','asc')->
        whereYear('tanggal', $tahun)->
        get();
        
        // $jumlah_all_tr = 0;
        // $total_tr = Transaction::whereYear('created_at', $tahun)->sum('total_barang');

        // $total_tr_nppn = TransactionNonPPN::whereYear('created_at', $tahun)->sum('total_barang');

        // $jumlah_all_tr = (int) $total_tr + $total_tr_nppn;

        // $dt_tr = Transaction::whereYear('created_at', $tahun)->latest()->first();

        // $dt_tr_nppn = TransactionNonPPN::whereYear('created_at', $tahun)->latest()->first();

        // if($dt_tr_nppn){
        //     $dt_tr_nppn = $dt_tr_nppn->created_at;
        // }
        // if (!$dt_tr) {
        //     return redirect()->back()->with('error', 'Tidak ada transaksi pada tahun '.$tahun);
        // }
        
        // $tgl = $dt_tr->created_at;
        // $tgl_nppn = $dt_tr_nppn;

        
        // if($tgl->format('dmYHis') > $dt_tr_nppn->format('dmYHis')){
            // $tgl_terakhir = $tgl;
        // }else{
        //     $tgl_terakhir = $dt_tr_nppn;
        // }

        $sum_aktiva_lancar = AktivaLancar::whereYear('tanggal', $tahun)->sum('biaya');

        return view('bukubesar', compact('bank','akun', 'aktiva_lancar', 'aktiva_tetap', 'sum_aktiva_lancar','tahun'));
    }

    public function excelBukuBesar(Request $req)
    {
        $bulan = $req->bulan3;
        $tahun = $req->tahun3;
        return Excel::download(new ExportBukuBesar($bulan,$tahun), 'Buku Besar '.now().'.xlsx');
    }

    public function pdfBukuBesar(Request $request)
    {
        $tahun = $request->tahun2;
        $bulan = $request->bulan2;

        if ($tahun != null && $bulan == null) {
            $akun = Akun::orderBy('kode_akun','asc')->
            whereYear('tanggal', $tahun)->
            get();

            $aktiva_lancar = AktivaLancar::orderBy('kode','asc')->
            whereYear('tanggal',$tahun)->
            get();
            $aktiva_tetap = AktivaTetap::orderBy('kode','asc')->
            whereYear('tanggal', $tahun)->
            get();

            $bank = Bank::orderBy('kode','asc')->
            whereYear('tanggal', $tahun)->
            get();
            
            $jumlah_all_tr = 0;
            $total_tr = Transaction::whereYear('created_at', $tahun)->sum('total_barang');

            $total_tr_nppn = TransactionNonPPN::whereYear('created_at', $tahun)->sum('total_barang');

            $jumlah_all_tr = (int) $total_tr + $total_tr_nppn;

            $dt_tr = Transaction::whereYear('created_at', $tahun)->latest()->first();

            $dt_tr_nppn = TransactionNonPPN::whereYear('created_at', $tahun)->latest()->first();

            if($dt_tr_nppn){
                $dt_tr_nppn = $dt_tr_nppn->created_at;
            }
            if (!$dt_tr) {
                return redirect()->back()->with('error', 'Tidak ada transaksi pada tahun '.$tahun);
            }
            
            $tgl = $dt_tr->created_at;
            $tgl_nppn = $dt_tr_nppn;

            
            // if($tgl->format('dmYHis') > $dt_tr_nppn->format('dmYHis')){
                $tgl_terakhir = $tgl;
            // }else{
            //     $tgl_terakhir = $dt_tr_nppn;
            // }

            $sum_aktiva_lancar = AktivaLancar::whereYear('tanggal', $tahun)->sum('biaya');

            $pdf = PDF::loadView('excelbukubesar', compact('bank','akun', 'aktiva_lancar', 'aktiva_tetap', 'sum_aktiva_lancar', 'jumlah_all_tr','tgl_terakhir','tahun','bulan'));
            return $pdf->download('Buku Besar '.now().'.pdf');

        }else{
            $bulan = Carbon::createFromFormat('Y-m', $request->bulan2)->format('n');

            $akun = Akun::orderBy('kode_akun','asc')->
            whereMonth('tanggal', $bulan)->
            whereYear('tanggal', $tahun)->
            get();

            $aktiva_lancar = AktivaLancar::orderBy('kode','asc')->
            whereMonth('tanggal', $bulan)->
            whereYear('tanggal',$tahun)->
            get();
            $aktiva_tetap = AktivaTetap::orderBy('kode','asc')->
            whereMonth('tanggal', $bulan)->
            whereYear('tanggal', $tahun)->
            get();

            $bank = Bank::orderBy('kode','asc')->
            whereMonth('tanggal', $bulan)->
            whereYear('tanggal', $tahun)->
            get();
            
            $jumlah_all_tr = 0;
            $total_tr = Transaction::whereMonth('created_at', $bulan)->
            whereYear('created_at', $tahun)->
            sum('total_barang');

            $total_tr_nppn = TransactionNonPPN::whereMonth('created_at', $bulan)->
            whereYear('created_at', $tahun)->
            sum('total_barang');

            $jumlah_all_tr = (int) $total_tr + $total_tr_nppn;

            $dt_tr = Transaction::whereMonth('created_at', $bulan)->
            whereYear('created_at', $tahun)
            ->latest()->first();

            $dt_tr_nppn = TransactionNonPPN::whereMonth('created_at', $bulan)->
            whereYear('created_at', $tahun)->latest()->first();

            if($dt_tr_nppn){
                $dt_tr_nppn = $dt_tr_nppn->created_at;
            }
            if (!$dt_tr) {
                // dd('tidak ada data');
                return redirect()->back()->with('error', 'Tidak ada transaksi pada tahun '.$tahun);
            }
            
            $tgl = $dt_tr->created_at;
            $tgl_nppn = $dt_tr_nppn;

            
            // if($tgl->format('dmYHis') > $dt_tr_nppn->format('dmYHis')){
                $tgl_terakhir = $tgl;
            // }else{
            //     $tgl_terakhir = $dt_tr_nppn;
            // }

            $sum_aktiva_lancar = AktivaLancar::whereMonth('tanggal', $bulan)->
            whereYear('tanggal', $tahun)->
            sum('biaya');
            
            $pdf = PDF::loadView('excelbukubesar', compact('bank','akun', 'aktiva_lancar', 'aktiva_tetap', 'sum_aktiva_lancar', 'jumlah_all_tr','tgl_terakhir','tahun','bulan'));
            return $pdf->download('Buku Besar '.now().'.pdf');
        }

    }

    // Laba Rugi
    public function viewLabaRugi(Request $request)
    {
        $transaction_type = $request->transaction_type;

        $now = Carbon::now();
        $month = Carbon::now()->format('n');
        $bln = Carbon::now()->isoFormat('MMMM');
        $years = Carbon::now()->format('Y');

        if ($request->bulan) {
            $month = Carbon::createFromFormat('Y-m', $request->bulan)->format('n');
            $bln = Carbon::createFromFormat('Y-m', $request->bulan)->isoFormat('MMMM');
            $years = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y');
            $now = Carbon::createFromFormat('Y-m', $request->bulan);
        }

        $startOfMonth = $now->copy()->startOfMonth();
        $endOfMonth = $now->copy()->endOfMonth();
        $subEndOfMonth = Carbon::parse($startOfMonth)->subDay(1);

        $comparisons = [$startOfMonth];
        if ($request->comparison == "last_month") {
            $comparisons = [$subEndOfMonth, $startOfMonth];
        } else if ($request->comparison == "current_year") {
            $comparisons = [];
            for ($i=1; $i<=12; $i++) {
                $iMonth = Carbon::createFromFormat("Y-m", $now->format('Y')."-".$i);
                if ($iMonth->format('Y-m') <= $now->format('Y-m')) {
                    $comparisons[] = $iMonth;
                }
            }
        }

        $labarugis = [];
        foreach ($comparisons as $comparison) {
            $labarugis[] = $this->fnLabaRugi(new Request(['bulan' => $comparison->format('Y-m')]), $transaction_type);
        }

        $data = [
            'month' => $month,
            'bln' => $bln,
            'years' => $years,
            'now' => $now,
            'labarugis' => $labarugis,
            'comparisons' => $comparisons,
            'export_type' => $request->type
        ];

        if ($request->type == 'pdf') {
            // ================= PDF ================== //
            $pdf = PDF::loadView('excellabarugi', $data);
            return $pdf->download('Laba Rugi '.now().'.pdf');
        } else if ($request->type == 'export') {
            // ================= Export ================== //
            return Excel::download(new ExportLabaRugi($data), 'Laba Rugi '.now().'.xlsx');
        } else {
            // ================= View ================== //
            return view('labarugi', $data);
        }
    }

    public function fnLabaRugi($request, $transaction_type)
    {
        $now = Carbon::now();
        $month = Carbon::now()->format('n');
        $bln = Carbon::now()->isoFormat('MMMM');
        $years = Carbon::now()->format('Y');
        $transaction_type = $transaction_type ?? Akun::$perusahaan_ppn;
        $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $status_pajak = $transaction_type == Akun::$perusahaan_ppn ? 'ppn' : 'non_ppn';
        $transaction_query_table = $transaction_type == Akun::$perusahaan_ppn ? "transactions" : "transaction_non_ppn as transactions";

        if ($request->bulan) {
            $month = Carbon::createFromFormat('Y-m', $request->bulan)->format('n');
            $bln = Carbon::createFromFormat('Y-m', $request->bulan)->isoFormat('MMMM');
            $years = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y');
            $now = Carbon::createFromFormat('Y-m', $request->bulan);
        }

        $startOfMonth = $now->copy()->startOfMonth();
        $endOfMonth = $now->copy()->endOfMonth();

        $list_akun_labarugi = Akun::whereIn('tipe_akun', ['Pendapatan', 'Beban'])->orderBy('kode_akun')
            ->where('perusahaan', $transaction_type)->get();

        $labarugi = [];

        foreach ($list_akun_labarugi as $akun) {
            $saldo_total = 0;
            $saldo_debit = 0;
            $saldo_kredit = 0;
            $jurnals = DB::table('jurnals')
                ->select('tanggal', 'keterangan', 'bukti_transaksi', 'debit', 'kredit')
                ->where('id_kode_akun', '=', $akun->id)
                ->whereBetween('tanggal', [$startOfMonth, $endOfMonth])
                ->orderBy('tanggal', 'asc')
                ->get();

            $saldo_normal = Akun::cekSaldoNormal($akun);
            if ($saldo_normal == 'debit') {
                $saldo_total += $jurnals->sum('debit') - $jurnals->sum('kredit');
                $saldo_debit += $jurnals->sum('debit') - $jurnals->sum('kredit');
            } else {
                $saldo_total += $jurnals->sum('kredit') - $jurnals->sum('debit');
                $saldo_kredit += $jurnals->sum('kredit') - $jurnals->sum('debit');
            }

            // ================= Penjualan =============== //
            if ($akun->kode_akun == Akun::$penjualan.$perusahaan_prefix) {
                $piutang_dagang = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit")
                    ->selectRaw("SUM(transactions.total_barang) + SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as kredit")
                    ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    ->where('receivable.status', 'hutang')
                    ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $receivable = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit")
                    ->selectRaw("SUM(transactions.total_barang) + SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as kredit")
                    ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                    ->whereIn('receivable.status', ['lunas'])
                    ->where('transactions.status', 'lunas')
                    ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $saldo_total += $piutang_dagang->sum('kredit') + $receivable->sum('kredit');
                $saldo_kredit += $piutang_dagang->sum('kredit') + $receivable->sum('kredit');
            }
            // =========================================== //

            // ==================== Potongan Penjualan ================== //
            if($akun->kode_akun == Akun::$potongan_penjualan.$perusahaan_prefix){
                $piutang_dagang = Receivable::selectRaw("receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as kredit")
                    ->selectRaw("SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as debit")
                    ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    ->where('receivable.status', 'hutang')
                    ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                    ->where('transactions.diskon', '!=', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $receivable = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as kredit")
                    ->selectRaw("SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as debit")
                    ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                    ->whereIn('receivable.status', ['lunas'])
                    ->where('transactions.status', 'lunas')
                    ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                    ->where('transactions.diskon', '!=', 0)
                    ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                    ->groupBy('receivable.kode_transaksi')
                    ->get();

                $saldo_total -= $piutang_dagang->sum('debit') + $receivable->sum('debit');
                $saldo_debit += $piutang_dagang->sum('debit') + $receivable->sum('debit');
            }
            // ========================================================== //

            // ==================== Persediaan Barang ================== //
            if($akun->kode_akun == Akun::$persediaan_barang.$perusahaan_prefix){
                $persediaan_barang = Supply::selectRaw("supplies.created_at as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, SUM(supplies.discounted_total) as debit, '0' as kredit")
                    ->whereBetweejurnalsn('supplies.created_at', [$startOfMonth, $endOfMonth])
                    ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                    ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                    ->where('supplies.status', 'lunas')
                    ->where('payable.status', '!=', 'batal')  
                    ->where('products.status_pajak', $status_pajak)
                    ->groupBy('supplies.kode_pasok')->get();

                $saldo_total -= $persediaan_barang->sum('debit');
                $saldo_debit += $persediaan_barang->sum('debit');
            }
            // =========================================== //

            // ================= Retur Penjualan =============== //
            // if ($akun->kode_akun == Akun::$retur_penjualan.$perusahaan_prefix) {
            //     $retur_penjualan = ReturCustomer::selectRaw("retur_customer.created_at as tanggal, retur_customer.keterangan as keterangan, retur_customer.no_transaksi as bukti_transaksi, SUM(product_retur_customer.total_barang) as debit, '0' as kredit")
            //         ->whereBetween('retur_customer.created_at', [$startOfMonth, $endOfMonth])
            //         ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
            //         ->where('product_retur_customer.status_pajak', $status_pajak)
            //         ->groupBy('product_retur_customer.kode_transaksi')->get();

            //     $saldo_total -= $retur_penjualan->sum('debit');
            //     $saldo_debit += $retur_penjualan->sum('debit');
            // }
            // =========================================== //

            // ================= Retur Pembelian =============== //
            // if ($akun->kode_akun == Akun::$retur_pembelian.$perusahaan_prefix) {
            //     $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, '0' as debit, SUM(retur_supplier.harga_modal) as kredit")
            //         ->whereBetween('retur_supplier.created_at', [$startOfMonth, $endOfMonth])
            //         ->join('products', 'products.id', 'retur_supplier.id_product')
            //         ->where('products.status_pajak', $status_pajak)->get();

            //     $saldo_total += $retur_pembelian->sum('kredit');
            //     $saldo_kredit += $retur_pembelian->sum('kredit');
            // }
            // =========================================== //

            // ================= Biata Penyusutan Perlengkapan =============== //
            if ($akun->kode_akun == Akun::$biaya_perlengkapan.$perusahaan_prefix) {
                $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function($query) use($perusahaan_prefix) {
                    $query->where('kode_akun', Akun::$peralatan_toko.$perusahaan_prefix);
                })->get();
                foreach ($penyusutan_aktiva_tetap as $value) {
                    $checkStartDate = Carbon::parse($value->tanggal)->format('Y-m-01');
                    $checkEndDate = Carbon::parse($value->tanggal)->addMonth($value->durasi_aktiva)->format('Y-m-t');
                    $checkNowDate = Carbon::parse($years.'-'.$month.'-'.date('d', strtotime($value->tanggal)));
                    if ($checkNowDate->between($checkStartDate, $checkEndDate)) {
                        $saldo_total += $value->penyusutan;
                        $saldo_debit += $value->penyusutan;
                    }
                }
            }

            // ================= Biata Penyusutan Kendaran =============== //
            if ($akun->kode_akun == Akun::$biaya_penyusutan_kendaraan.$perusahaan_prefix) {
                $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function($query) use($perusahaan_prefix) {
                    $query->where('kode_akun', Akun::$kendaraan.$perusahaan_prefix);
                })->get();
                foreach ($penyusutan_aktiva_tetap as $value) {
                    $checkStartDate = Carbon::parse($value->tanggal)->format('Y-m-01');
                    $checkEndDate = Carbon::parse($value->tanggal)->addMonth($value->durasi_aktiva)->format('Y-m-t');
                    $checkNowDate = Carbon::parse($years.'-'.$month.'-'.date('d', strtotime($value->tanggal)));
                    if ($checkNowDate->between($checkStartDate, $checkEndDate)) {
                        $saldo_total += $value->penyusutan;
                        $saldo_debit += $value->penyusutan;
                    }
                }
            }
            // =========================================== //

            $labarugi[$akun->tipe_akun][] = [
                'kode_akun' => $akun->kode_akun,
                'nama_akun' => $akun->nama,
                'saldo_normal' => $saldo_normal,
                'saldo_debit' => $saldo_debit,
                'saldo_kredit' => $saldo_kredit,
                'saldo_total' => $saldo_total,
            ];
        }

        return [
            'month' => $month,
            'bln' => $bln,
            'years' => $years,
            'now' => $now,
            'labarugi' => $labarugi
        ];
    }

    // Neraca
    public function viewNeraca(Request $request)
    {
        $now = Carbon::now();
        $transaction_type = $request->transaction_type ?? Akun::$perusahaan_ppn;
        $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
        $status_pajak = $transaction_type == Akun::$perusahaan_ppn ? 'ppn' : 'non_ppn';
        $transaction_query_table = $transaction_type == Akun::$perusahaan_ppn ? "transactions" : "transaction_non_ppn as transactions";

        if ($request->bulan) {
            $now = Carbon::createFromFormat('Y-m', $request->bulan);
        }

        $startOfMonth = $now->copy()->startOfMonth();
        $endOfMonth = $now->copy()->endOfMonth();
        $subEndOfMonth = Carbon::parse($startOfMonth)->subDay(1);

        $comparisons = [$startOfMonth];
        if ($request->comparison == "last_month") {
            $comparisons = [$subEndOfMonth, $startOfMonth];
        } else if ($request->comparison == "current_year") {
            $comparisons = [];
            for ($i=1; $i<=12; $i++) {
                $iMonth = Carbon::createFromFormat("Y-m", $now->format('Y')."-".$i);
                if ($iMonth->format('Y-m') <= $now->format('Y-m')) {
                    $comparisons[] = $iMonth;
                }
            }
        }

        $akun = Akun::whereIn('tipe_akun', ['Aktiva Tetap', 'Aktiva Lancar', 'Kewajiban', "Modal"])
            ->where('perusahaan', $transaction_type)->get();

        $aktiva = ['Aktiva Tetap', 'Aktiva Lancar'];
        $passiva = ["Kewajiban", "Modal"];

        $neracas = [
            "Aktiva" => [
                "Aktiva Tetap" => [],
                "Aktiva Lancar" => [],
            ],
            "Passiva" => [
                "Kewajiban" => [],
                "Modal" => []
            ]
        ];

        foreach($akun as $a)
        {
            $saldo_akhir_comparisons = [];

            foreach ($comparisons as $comparison) {
                $startOfMonth = $comparison->copy()->startOfMonth();
                $endOfMonth = $comparison->copy()->endOfMonth();
                $subEndOfMonth = Carbon::parse($startOfMonth)->subDay(1);

                $jurnals = DB::table('jurnals')
                    ->select('debit', 'kredit')
                    ->where('id_kode_akun', '=', $a->id)
                    ->whereBetween('tanggal', [$startOfMonth, $endOfMonth])
                    ->get(); 

                $saldo_akhir = 0;

                $saldo_normal = Akun::cekSaldoNormal($a);
                $saldo_awal = SaldoAkhirController::getSaldoAkhir($a, $startOfMonth);
                if($saldo_normal == 'debit'){
                    $saldo_akhir = $saldo_awal + $jurnals->sum('debit') - $jurnals->sum('kredit');
                }else{
                    $saldo_akhir = $saldo_awal + $jurnals->sum('kredit') - $jurnals->sum('debit');
                }


                // ================= Kas =============== //
                if($a->kode_akun == Akun::$kas.$perusahaan_prefix){
                    $piutang_dagang = Receivable::selectRaw('receivable.id, receivable.kode_transaksi')
                        ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->where('receivable.metode_bayar', 'kas')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $receivable = Receivable::selectRaw("receivable.id, receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.total_barang) as debit, '0' as kredit")
                        ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                        ->where('receivable.metode_bayar', 'kas')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, receivable.bayar as debit, '0' as kredit")
                        ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                        ->whereIn('receivable.status', ['hutang', 'lunas'])
                        ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                        ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                        ->where('receivable.metode_bayar', 'kas')
                        ->whereExists(function($query) use ($transaction_query_table) {
                            $query->selectRaw('1')
                                ->from($transaction_query_table)
                                ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                        })->get();

                    $retur_pembelian = [];
                    // $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, retur_supplier.harga_modal as debit, '0' as kredit")
                    //     ->whereBetween('retur_supplier.created_at', [$startOfMonth, $endOfMonth])
                    //     ->join('products', 'products.id', 'retur_supplier.id_product')
                    //     ->where('retur_supplier.metode_bayar', 'kas')
                    //     ->where('products.status_pajak', $status_pajak)->get();

                    $persediaan_barang = Supply::selectRaw("supplies.created_at as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, '0' as debit, SUM(supplies.discounted_total) as kredit")
                        ->whereBetween('supplies.created_at', [$startOfMonth, $endOfMonth])
                        ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                        ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                        ->where('supplies.status', 'lunas')
                        ->where('payable.status', '!=', 'batal')
                        ->where('products.status_pajak', $status_pajak)
                        ->where('payable.metode_bayar', 'kas')
                        ->groupBy('supplies.kode_pasok')->get();

                    $retur_penjualan = [];
                    // $retur_penjualan = ReturCustomer::selectRaw("product_retur_customer.tanggal_transaksi as tanggal, retur_customer.keterangan as keterangan, product_retur_customer.kode_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                    //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    //     ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'kas' LIMIT 1) receivable"), function ($join) {
                    //         $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                    //     })
                    //     ->whereBetween('product_retur_customer.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    //     ->where('product_retur_customer.status_pajak', $status_pajak)
                    //     ->groupBy('product_retur_customer.kode_transaksi')->get();

                    // $saldo_akhir = $saldo_akhir + $receivable->sum('debit') + $pelunasan_piutang_dagang->sum('debit') + $retur_pembelian->sum('debit') - $persediaan_barang->sum('kredit') - $retur_penjualan->sum('kredit');
                    $saldo_akhir = $saldo_akhir + $receivable->sum('debit') + $pelunasan_piutang_dagang->sum('debit') - $persediaan_barang->sum('kredit');
                }
                // ==================================== //

                // ================= Bank ============== //
                if($a->kode_akun == Akun::$bank.$perusahaan_prefix){
                    $bank_debit = Bank::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, nominal as debit, '0' as kredit")
                        ->whereBetween('tanggal', [$startOfMonth, $endOfMonth])->where('jenis', 'in')->get();

                    $bank_kredit = Bank::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, '0' as debit, nominal as kredit")
                        ->whereBetween('tanggal', [$startOfMonth, $endOfMonth])->where('jenis', 'out')->get();

                    $piutang_dagang = Receivable::selectRaw('receivable.id, receivable.kode_transaksi')
                        ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->where('receivable.metode_bayar', 'bank')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $receivable = Receivable::selectRaw("receivable.id, receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.total_barang) as debit, '0' as kredit")
                        ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                        ->where('receivable.metode_bayar', 'bank')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, receivable.bayar as debit, '0' as kredit")
                        ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                        ->whereIn('receivable.status', ['hutang', 'lunas'])
                        ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                        ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                        ->where('receivable.metode_bayar', 'bank')
                        ->whereExists(function($query) use ($transaction_query_table) {
                            $query->selectRaw('1')
                                ->from($transaction_query_table)
                                ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                        })->get();

                    $retur_pembelian = [];
                    // $retur_pembelian = ReturSupplier::selectRaw("retur_supplier.created_at as tanggal, retur_supplier.keterangan as keterangan, ' ' as bukti_transaksi, retur_supplier.harga_modal as debit, '0' as kredit")
                    //     ->whereBetween('retur_supplier.created_at', [$startOfMonth, $endOfMonth])
                    //     ->join('products', 'products.id', 'retur_supplier.id_product')
                    //     ->where('retur_supplier.metode_bayar', 'bank')
                    //     ->where('products.status_pajak', $status_pajak)->get();

                    $persediaan_barang = Supply::selectRaw("supplies.created_at as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, '0' as debit, SUM(supplies.discounted_total) as kredit")
                        ->whereBetween('supplies.created_at', [$startOfMonth, $endOfMonth])
                        ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                        ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                        ->where('supplies.status', 'lunas')
                        ->where('payable.status', '!=', 'batal')
                        ->where('products.status_pajak', $status_pajak)
                        ->where('payable.metode_bayar', 'bank')
                        ->groupBy('supplies.kode_pasok')->get();

                    $retur_penjualan = [];
                    // $retur_penjualan = ReturCustomer::selectRaw("product_retur_customer.tanggal_transaksi as tanggal, retur_customer.keterangan as keterangan, product_retur_customer.kode_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                    //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    //     ->join(DB::raw("(SELECT receivable.metode_bayar, receivable.kode_transaksi, receivable.status FROM receivable WHERE receivable.metode_bayar = 'bank' LIMIT 1) receivable"), function ($join) {
                    //         $join->on('product_retur_customer.kode_transaksi', 'receivable.kode_transaksi');
                    //     })
                    //     ->whereBetween('product_retur_customer.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                    //     ->where('product_retur_customer.status_pajak', $status_pajak)
                    //     ->groupBy('product_retur_customer.kode_transaksi')->get();

                    // $saldo_akhir = $saldo_akhir + $bank_debit->sum('debit') + $receivable->sum('debit') + $pelunasan_piutang_dagang->sum('debit') + $retur_pembelian->sum('debit') - $bank_kredit->sum('kredit') - $persediaan_barang->sum('kredit') - $retur_penjualan->sum('kredit');
                    $saldo_akhir = $saldo_akhir + $bank_debit->sum('debit') + $receivable->sum('debit') + $pelunasan_piutang_dagang->sum('debit') - $bank_kredit->sum('kredit') - $persediaan_barang->sum('kredit');
                }
                // ===================================== //

                // =============== Aktiva Lancar Locked ============= //
                // if ($a->kode_akun == Akun::$aktiva_lancar) {
                //     $aktiva_lancar = AktivaLancar::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, biaya as debit, '0' as kredit")->whereBetween('tanggal', [$startOfMonth, $endOfMonth])->get();
                
                //     $saldo_akhir = $aktiva_lancar->sum('debit');
                // }
                // ================================================= //

                // ================= Aktiva Lancar Unlocked ======================= //
                $not_locked = [Akun::$sewa_dbm.$perusahaan_prefix, Akun::$uang_muka_pembelian.$perusahaan_prefix];
                if (in_array($a->kode_akun, $not_locked)) {
                    $aktiva_lancar = AktivaLancar::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, biaya as debit, '0' as kredit")->where('kode', $a)->whereBetween('tanggal', [$startOfMonth, $endOfMonth])->get();

                    $saldo_akhir = $saldo_akhir + $aktiva_lancar->sum('debit');
                }
                // ================================================================ //

                // =============== Aktiva Tetap Locked ============== //
                $locked = [Akun::$kendaraan.$perusahaan_prefix, Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix, Akun::$peralatan_toko.$perusahaan_prefix, Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix];

                if (in_array($a->kode_akun, $locked)) {
                    $aktiva_tetap = AktivaTetap::selectRaw("tanggal, keterangan, ' ' as bukti_transaksi, nominal as debit, '0' as kredit")->where('kode', $a)->whereBetween('tanggal', [$startOfMonth, $endOfMonth])->get();

                    $saldo_akhir = $aktiva_tetap->sum('debit');
                }
                // =========================================== //

                // ==================== Persediaan Barang ================== //
                if ($a->kode_akun == Akun::$persediaan_barang.$perusahaan_prefix) {
                    $persediaan_barang = Supply::selectRaw("supplies.created_at as tanggal, ' ' as keterangan, payable.kode_pasok as bukti_transaksi, SUM(supplies.discounted_total) as debit, '0' as kredit")
                    ->whereBetween('supplies.created_at', [$startOfMonth, $endOfMonth])
                    ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                    ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                    ->where('supplies.status', 'lunas')
                    ->where('payable.status', '!=', 'batal')
                    ->where('products.status_pajak', $status_pajak)
                    ->groupBy('supplies.kode_pasok')->get();

                    $saldo_akhir = $saldo_akhir + $persediaan_barang->sum('debit');
                }
                // ====================================================== //

                // ==================== Piutang Dagang ================== //
                if($a->kode_akun == Akun::$piutang_dagang.$perusahaan_prefix){
                    $piutang_dagang = Receivable::selectRaw("receivable.id, receivable.tanggal_transaksi as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, SUM(transactions.total_barang) as debit, '0' as kredit")
                        ->whereBetween('receivable.tanggal_transaksi', [$startOfMonth, $endOfMonth])
                        ->where('receivable.status', 'hutang')
                        ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $receivable = Receivable::selectRaw('receivable.*, SUM(transactions.total_barang) as tr_total')
                        ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                        ->whereIn('receivable.status', ['lunas'])
                        ->where('transactions.status', 'lunas')
                        ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('bukti_transaksi')->toArray())
                        ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->groupBy('receivable.kode_transaksi')
                        ->get();

                    $pelunasan_piutang_dagang = Receivable::selectRaw("receivable.created_at as tanggal, ' ' as keterangan, receivable.kode_transaksi as bukti_transaksi, '0' as debit, receivable.bayar as kredit")
                        ->whereBetween('receivable.created_at', [$startOfMonth, $endOfMonth])
                        ->whereIn('receivable.status', ['hutang', 'lunas'])
                        ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                        ->whereNotIn('receivable.id', $receivable->pluck('id')->toArray())
                        ->whereExists(function($query) use ($transaction_query_table) {
                            $query->selectRaw('1')
                                ->from($transaction_query_table)
                                ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi');
                        })->get();

                    $retur_penjualan = [];
                    // $retur_penjualan = ReturCustomer::selectRaw("retur_customer.created_at as tanggal, retur_customer.keterangan as keterangan, retur_customer.no_transaksi as bukti_transaksi, '0' as debit, SUM(product_retur_customer.total_barang) as kredit")
                    //     ->whereBetween('retur_customer.created_at', [$startOfMonth, $endOfMonth])
                    //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
                    //     ->where('product_retur_customer.status_pajak', $status_pajak)
                    //     ->groupBy('product_retur_customer.kode_transaksi')->get();

                    // $saldo_akhir = $saldo_akhir + $piutang_dagang->sum('debit') - $pelunasan_piutang_dagang->sum('kredit') - $retur_penjualan->sum('kredit');
                    $saldo_akhir = $saldo_akhir + $piutang_dagang->sum('debit') - $pelunasan_piutang_dagang->sum('kredit');
                }
                // ====================================================== //

                // ============== Penyusutan Aktiva Tetap =============== //
                if ($a->kode_akun == Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix || $a->kode_akun == Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix) {
                    if ($a->kode_akun == Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix) {
                        $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function ($query) use($perusahaan_prefix) {
                            $query->where('kode_akun', Akun::$kendaraan.$perusahaan_prefix);
                        })->get();
                    } else if ($a->kode_akun == Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix) {
                        $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')->whereHas('akun', function ($query) use($perusahaan_prefix) {
                            $query->where('kode_akun', Akun::$peralatan_toko.$perusahaan_prefix);
                        })->get();
                    }

                    $saldo_akhir = 0;

                    foreach ($penyusutan_aktiva_tetap as $value) {
                        $checkStartDate = Carbon::parse($value->tanggal)->format('Y-m-01');
                        $checkEndDate = Carbon::parse($value->tanggal)->addMonth($value->durasi_aktiva)->format('Y-m-t');
                        $checkNowDate = Carbon::parse($now->format('Y') . '-' . $now->format('n') . '-' . date('d', strtotime($value->tanggal)));

                        if ($checkNowDate->between($checkStartDate, $checkEndDate)) {
                            $startDate = Carbon::parse($checkStartDate);
                            $saldo_akhir = ($startDate->diffInMonths($checkNowDate) * $value->penyusutan);
                        }
                    }
                }
                // =========================================== //
                $saldo_akhir_comparisons[] = $saldo_akhir;
            }

            if(in_array($a->tipe_akun, $aktiva)){
                $neracas["Aktiva"][$a->tipe_akun][] = [
                    "nama_akun" => $a->nama,
                    "kode_akun" => $a->kode_akun,
                    "saldo_normal" => $saldo_normal,
                    "saldo_akhir" => $saldo_akhir_comparisons
                ];
            }else if(in_array($a->tipe_akun, $passiva)){
                $neracas["Passiva"][$a->tipe_akun][] = [
                    "nama_akun" => $a->nama,
                    "kode_akun" => $a->kode_akun,
                    "saldo_normal" => $saldo_normal,
                    "saldo_akhir" => $saldo_akhir_comparisons
                ];
            }
        }

        $saldo_akhir_comparisons = [];
        foreach ($comparisons as $comparison) {
            $laba_rugi = $this->fnLabaRugi(new Request(['bulan' => $comparison->format('Y-m')]), $transaction_type)['labarugi'];
            $saldo_akhir_comparisons[] = collect($laba_rugi['Pendapatan'] ?? [])->sum('saldo_total') - collect($laba_rugi['Beban'] ?? [])->sum('saldo_total');
        }

        $neracas["Passiva"]["Modal"][] = [
            "nama_akun" => "Laba/Rugi",
            "kode_akun" => 0,
            "saldo_normal" => 'debit',
            "saldo_akhir" => $saldo_akhir_comparisons
        ];


        $data = [
            "neracas" => $neracas,
            "now" => $now,
            "export_type" => $request->type,
            "comparisons" => $comparisons
        ];


        if ($request->type == 'pdf') {
            // ================= PDF ================== //
            $pdf = PDF::loadView('excelneraca', $data);
            return $pdf->download('Neraca ' . now() . '.pdf');
        } else if ($request->type == 'export') {
            // ================= Export ================== //
            return Excel::download(new ExportNeraca($data), 'Neraca ' . now() . '.xlsx');
        } else {
            // ================= View ================== //
            return view('neraca', $data);
        }
    }

    public function filterNeraca(Request $req)
    {
        $this->validate($req, [
            'bulan' => 'required',
        ]);
        $fbulan = $req->bulan;
        $bulan = Carbon::createFromFormat('Y-m', $req->bulan)->format('n');
        $namabulan = Carbon::createFromFormat('Y-m', $req->bulan)->isoformat('MMMM');
        $tahun = Carbon::createFromFormat('Y-m', $req->bulan)->format('Y');

        $aktiva_lancar = AktivaLancar::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->get();
        $total_lancar = AktivaLancar::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->sum('biaya');

        $aktiva_tetap = AktivaTetap::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->get();
        $total_tetap = AktivaTetap::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->sum('nominal');

        $hutang = Payable::whereMonth('created_at', $bulan)->whereYear('created_at', $tahun)->sum('subtotal');

        $total_hutang = $hutang;

        $total_aktiva = $total_lancar + $total_tetap;

        // $supplies = Supply::all();
        return view('neraca', compact('aktiva_lancar', 'aktiva_tetap', 'total_lancar', 'total_tetap', 'total_aktiva', 'hutang', 'bulan', 'tahun', 'namabulan', 'fbulan'));
    }

    public function excelNeraca(Request $req)
    {
        $bulan = $req->bulan3;
        return Excel::download(new ExportNeraca($bulan), 'Neraca '.now().'.xlsx');
    }

    public function pdfNeraca(Request $req)
    {
        $fbulan = $req->bulan2;
        if ($req->bulan2 != null){
            $bulan = Carbon::createFromFormat('Y-m', $req->bulan2)->format('n');
            $namabulan = Carbon::createFromFormat('Y-m', $req->bulan2)->isoformat('MMMM');
            $tahun = Carbon::createFromFormat('Y-m', $req->bulan2)->format('Y');

            $aktiva_lancar = AktivaLancar::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->get();
            $total_lancar = AktivaLancar::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->sum('biaya');

            $aktiva_tetap = AktivaTetap::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->get();
            $total_tetap = AktivaTetap::whereMonth('tanggal', $bulan)->whereYear('tanggal', $tahun)->sum('nominal');

            $hutang = Payable::whereMonth('created_at', $bulan)->whereYear('created_at', $tahun)->sum('subtotal');

            $total_hutang = $hutang;

            $total_aktiva = $total_lancar + $total_tetap;

            $pdf = PDF::loadView('excelneraca', compact('aktiva_lancar', 'aktiva_tetap', 'total_lancar', 'total_tetap', 'total_aktiva', 'hutang', 'bulan', 'tahun', 'namabulan', 'fbulan'));
            return $pdf->download('Neraca '.now().'.pdf');
        }else{
            $aktiva_lancar = AktivaLancar::whereMonth('tanggal', now()->month)->whereYear('tanggal', now()->year)->get();
            $total_lancar = AktivaLancar::whereMonth('tanggal', now()->month)->whereYear('tanggal', now()->year)->sum('biaya');

            $aktiva_tetap = AktivaTetap::whereMonth('tanggal', now()->month)->whereYear('tanggal', now()->year)->get();
            $total_tetap = AktivaTetap::whereMonth('tanggal', now()->month)->whereYear('tanggal', now()->year)->sum('nominal');

            $hutang = Payable::whereMonth('created_at', now()->month)->whereYear('created_at', now()->year)->sum('subtotal');

            $total_hutang = $hutang;

            $total_aktiva = $total_lancar + $total_tetap;
            $pdf = PDF::loadView('excelneraca', compact('aktiva_lancar', 'aktiva_tetap', 'total_lancar', 'total_tetap', 'total_aktiva', 'hutang'));
            return $pdf->download('Neraca '.now().'.pdf');
        }
    }

    // Jurnal Harian
    public function viewJurnalHarian(Request $request)
    {
        // SaldoAkhirController::checkSaldoAkhir();

        // ====== Filter Date ====== //
        $start_date = Carbon::now()->format('Y-m-01');
        $end_date = Carbon::now()->format('Y-m-t');

        if ($request->bulan) {
            $start_date = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y-m-01');
            $end_date = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y-m-t');
        }
        if ($request->start_date && $request->end_date) {
            $start_date = Carbon::create($request->start_date)->format('Y-m-d');
            $end_date = Carbon::create($request->end_date)->format('Y-m-d');
        }

        $start_date = $start_date . " 00:00:00";
        $end_date = $end_date . " 23:59:59";
        $subEndOfMonth = Carbon::parse($start_date)->subDay(1);
        // ========================= //

        // Filter Akuns
        $filter_akuns = $request->akuns ?? [];

        // Filter Barang
        $filter_barangs = $request->barangs ?? [];

        // Filter Search
        $filter_search = $request->search;

        $jurnal_harians = [];
        $trans_jurnal_harian = [];

        $arr_transaction_type = $request->transaction_type == "all" ? [1, 2] : [$request->transaction_type ?? 1];
        $saldo_awal_first = 0;

        foreach ($arr_transaction_type as $transaction_type) {
            $perusahaan_prefix = $transaction_type == Akun::$perusahaan_ppn ? Akun::$perusahaan_prefix : "";
            $status_pajak = $transaction_type == Akun::$perusahaan_ppn ? 'ppn' : 'non_ppn';

            // ====== Termasuk Dalam $aktiva_lancar ====== //
            $aktiva_lancar = AktivaLancar::with('akun')->whereBetween('tanggal', [$start_date, $end_date])
                ->whereHas('akun', function($query) use ($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();
            $transaction_query_table = $transaction_type == Akun::$perusahaan_ppn ? "transactions" : "transaction_non_ppn as transactions";
            $piutang_dagang = Receivable::selectRaw('receivable.id, receivable.kode_transaksi, receivable.tanggal_transaksi, SUM(transactions.total_barang) as tr_total')
                ->selectRaw("SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as tr_diskon")
                ->whereBetween('receivable.tanggal_transaksi', [$start_date, $end_date])
                ->where('receivable.status', 'hutang')
                ->whereRaw('receivable.id = (select id from receivable as sub_r where sub_r.kode_transaksi = receivable.kode_transaksi and sub_r.status = "hutang" limit 1)')
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->when($filter_barangs, function($query, $filter_barangs) { $query->whereIn('transactions.kode_barang', $filter_barangs); })
                ->groupBy('receivable.kode_transaksi')->get();
                
            $kas = Receivable::selectRaw('receivable.id, receivable.kode_transaksi, receivable.metode_bayar, receivable.created_at, SUM(transactions.total_barang) as tr_total')
                ->selectRaw("SUM(CASE WHEN transactions.jenis_kemasan='Pack' THEN (transactions.jumlah * transactions.diskon) ELSE ((transactions.jumlah / (SELECT products.pack from products WHERE products.kode_barang = transactions.kode_barang)) * transactions.diskon) END) as tr_diskon")
                ->whereBetween('receivable.created_at', [$start_date, $end_date])
                ->whereIn('receivable.status', ['lunas'])
                ->where('transactions.status', 'lunas')
                ->whereNotIn('receivable.kode_transaksi', $piutang_dagang->pluck('kode_transaksi')->toArray())
                ->join($transaction_query_table, 'transactions.kode_transaksi', 'receivable.kode_transaksi')
                ->when($filter_barangs, function($query, $filter_barangs) { $query->whereIn('transactions.kode_barang', $filter_barangs); })
                ->groupBy('receivable.kode_transaksi')->get();
            
            $bank = Bank::whereBetween('tanggal', [$start_date, $end_date])->get();
            $pelunasan_piutang_dagang = Receivable::whereBetween('receivable.created_at', [$start_date, $end_date])
                ->select('receivable.id', 'receivable.kode_transaksi', 'receivable.metode_bayar', 'receivable.created_at', 'receivable.bayar')
                ->whereIn('receivable.status', ['hutang', 'lunas'])
                ->whereNotIn('receivable.id', $piutang_dagang->pluck('id')->toArray())
                ->whereNotIn('receivable.id', $kas->pluck('id')->toArray())
                ->where('receivable.bayar', '>', 0)
                ->whereExists(function($query) use ($transaction_query_table, $filter_barangs) {
                    $query->selectRaw('1')
                        ->from($transaction_query_table)
                        ->whereColumn('transactions.kode_transaksi', 'receivable.kode_transaksi')
                        ->when($filter_barangs, function($query2, $filter_barangs) { $query2->whereIn('transactions.kode_barang', $filter_barangs); });
                })->get();

            $persediaan_barang = Supply::whereBetween('supplies.created_at', [$start_date, $end_date])
                ->select('supplies.*', 'payable.metode_bayar')
                ->selectRaw('SUM(supplies.discounted_total) as sum_discounted_total')
                ->join('products', 'products.kode_barang', 'supplies.kode_barang')
                ->join('payable', 'payable.kode_pasok', 'supplies.kode_pasok')
                ->where('supplies.status', 'lunas')
                ->where('payable.status', '!=', 'batal')
                ->where('products.status_pajak', $status_pajak)
                ->when($filter_barangs, function($query, $filter_barangs) { $query->whereIn('supplies.kode_barang', $filter_barangs); })
                ->groupBy('supplies.kode_pasok')->get();
            // =========================================== //

            // ====== Termasuk Dalam $aktiva_tetap ======= //
            $aktiva_tetap = AktivaTetap::with('akun')->whereBetween('tanggal', [$start_date, $end_date])
                ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();
            $penyusutan_aktiva_tetap =  AktivaTetap::with('akun')
                ->whereHas('akun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();
            // =========================================== //

            // ========= Termasuk Dalam $retur_penjualan ========== //
            // $retur_penjualan = ReturCustomer::select('product_retur_customer.*', 'retur_customer.keterangan')
            //     ->selectRaw('(SELECT receivable.metode_bayar FROM receivable WHERE receivable.kode_transaksi = product_retur_customer.kode_transaksi LIMIT 1) as metode_bayar')
            //     ->selectRaw('(SELECT receivable.status FROM receivable WHERE receivable.kode_transaksi = product_retur_customer.kode_transaksi LIMIT 1) as r_status')
            //     ->selectRaw('SUM(product_retur_customer.total_barang) as sum_total_barang')->whereBetween('retur_customer.created_at', [$start_date, $end_date])
            //     ->join('product_retur_customer', 'product_retur_customer.kode_transaksi', 'retur_customer.no_transaksi')
            //     ->where('product_retur_customer.status_pajak', $status_pajak)
            //     ->when($filter_barangs, function($query, $filter_barangs) { $query->whereIn('product_retur_customer.kode_barang', $filter_barangs); })
            //     ->groupBy('product_retur_customer.kode_transaksi')->get();
            // =========================================== //

            // ========= Termasuk Dalam $retur_pembelian ========== //
            // $retur_pembelian = ReturSupplier::whereBetween('retur_supplier.created_at', [$start_date, $end_date])
            //     ->select('retur_supplier.*')
            //     ->join('products', 'products.id', 'retur_supplier.id_product')
            //     ->when($filter_barangs, function($query, $filter_barangs) { $query->whereIn('products.kode_barang', $filter_barangs); })
            //     ->where('products.status_pajak', $status_pajak)->get();
            // =========================================== //

            // ========= Termasuk Dalam $jurnal ========== //
            $jurnal = Jurnal::with('kodeAkun')->whereBetween('tanggal', [$start_date, $end_date])
                ->whereHas('kodeAkun', function($query) use($transaction_type) { $query->where('perusahaan', $transaction_type); })->get();
            // =========================================== //

            // ================ List Akun ================ //
            $akun_kas = Akun::where('kode_akun', Akun::$kas.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Kas';
            $akun_bank = Akun::where('kode_akun', Akun::$bank.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Bank';
            $akun_penjualan = Akun::where('kode_akun', Akun::$penjualan.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Penjualan';
            $akun_potongan_penjualan = Akun::where('kode_akun', Akun::$potongan_penjualan.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Potongan Penjualan';
            $akun_piutang = Akun::where('kode_akun', Akun::$piutang_dagang.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Piutang Dagang';
            $akun_persediaan_barang = Akun::where('kode_akun', Akun::$persediaan_barang.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Persediaan Barang';
            $akun_retur_penjualan = Akun::where('kode_akun', Akun::$retur_penjualan.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Retur Penjualan';
            $akun_retur_pembelian = Akun::where('kode_akun', Akun::$retur_pembelian.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Retur Pembelian';
            $akun_akumulasi_penyusutan_kendaraan = Akun::where('kode_akun', Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Akumulasi Penyusutan Kendaraan';
            $akun_akumulasi_penyusutan_peralatan_toko = Akun::where('kode_akun', Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Akumulasi Penyusutan Peralatan Toko';
            $akun_biaya_penyusutan_kendaraan = Akun::where('kode_akun', Akun::$biaya_penyusutan_kendaraan.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Biaya Penyusutan Kendaraan';
            $akun_biaya_perlengkapan = Akun::where('kode_akun', Akun::$biaya_perlengkapan.$perusahaan_prefix)->where('perusahaan', $transaction_type)->first() ?? 'Biaya Penyusutan Perlengkapan';
            $akun_hpp = Akun::where('kode_akun', Akun::$hpp.$perusahaan_prefix)->first() ?? 'HPP';
            // =========================================== //

            // ============== Aktiva Lancar ============== //
            foreach ($aktiva_lancar as $value) {
                $debit = [];
                $kredit = [];

                array_push($debit, [
                    'kode_akun' => $value->akun ? $value->akun->kode_akun : '',
                    'nama_akun' => $value->akun ? $value->akun->nama : '',
                    'keterangan' => $value->keterangan,
                    'nominal' => $value->biaya,
                ]);
                array_push($kredit, [
                    'kode_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->kode_akun ?? Akun::$bank.$perusahaan_prefix) : ($akun_kas->kode_akun ?? Akun::$kas.$perusahaan_prefix),
                    'nama_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->nama ?? $akun_bank) : ($akun_kas->nama ?? $akun_kas),
                    'keterangan' => 'Kas Keluar',
                    'nominal' => $value->biaya,
                ]);

                array_push($jurnal_harians, [ 'tanggal' => $value->tanggal, 'debit' => $debit, 'kredit' => $kredit ]);
            }
            // =========================================== //

            // =================== Kas =================== //
            foreach ($kas as $value) {
                $debit = [];
                $kredit = [];
                $transGroupType = 'penjualan_'.$value->metode_bayar;
                array_push($debit, [
                    'kode_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->kode_akun ?? Akun::$bank.$perusahaan_prefix) :($akun_kas->kode_akun ?? Akun::$kas.$perusahaan_prefix),
                    'nama_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->nama ?? $akun_bank) :($akun_kas->nama ?? $akun_kas),
                    'keterangan' => 'Kas Masuk',
                    'nominal' => $value->tr_total,
                    // 'ref' => $value->kode_transaksi,
                ]);
                if ($value->tr_diskon > 0) {
                    $transGroupType = "potongan_penjualan_".$value->metode_bayar;
                    array_push($debit, [
                        'kode_akun' => $akun_potongan_penjualan->kode_akun ?? Akun::$potongan_penjualan.$perusahaan_prefix,
                        'nama_akun' => $akun_potongan_penjualan->nama ?? $akun_potongan_penjualan,
                        'keterangan' => 'Potongan Penjualan',
                        'nominal' => $value->tr_diskon,
                        // 'ref' => $value->kode_transaksi,
                    ]);
                }
                array_push($kredit, [
                    'kode_akun' => $akun_penjualan->kode_akun ?? Akun::$penjualan.$perusahaan_prefix,
                    'nama_akun' => $akun_penjualan->nama ?? $akun_penjualan,
                    'keterangan' => 'Penjualan',
                    'nominal' => $value->tr_total + $value->tr_diskon,
                    // 'ref' => $value->kode_transaksi,
                ]);
                $transGroupType = date('Y-m-d', strtotime($value->created_at)) . '_' . $transGroupType;
                if (isset($trans_jurnal_harian[$transGroupType])) {
                    $trans_jurnal_harian[$transGroupType]['debit'][0]['nominal'] += $debit[0]['nominal']; 
                    if (isset($debit[1])) $trans_jurnal_harian[$transGroupType]['debit'][1]['nominal'] += $debit[1]['nominal']; 
                    $trans_jurnal_harian[$transGroupType]['kredit'][0]['nominal'] += $kredit[0]['nominal'];
                } else {
                    $trans_jurnal_harian[$transGroupType] = [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ];
                }
                // array_push($jurnal_harians, [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ]);

                // HPP Persediaan Barang
                // $debit = [];
                // $kredit = [];
                // array_push($debit, [
                //     'kode_akun' => $akun_hpp->kode_akun ?? Akun::$hpp.$perusahaan_prefix,
                //     'nama_akun' => $akun_hpp->nama ?? $akun_hpp,
                //     'keterangan' => 'HPP',
                //     'nominal' => 0,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($kredit, [
                //     'kode_akun' => $akun_persediaan_barang->kode_akun ?? Akun::$persediaan_barang.$perusahaan_prefix,
                //     'nama_akun' => $akun_persediaan_barang->nama ?? $akun_persediaan_barang,
                //     'keterangan' => 'HPP',
                //     'nominal' => 0,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($jurnal_harians, [ 'tanggal' => $value->tanggal_transaksi, 'debit' => $debit, 'kredit' => $kredit ]);
            }
            // =========================================== //

            // ================== Bank =================== //
            foreach ($bank as $value) {
                $debit = [];
                $kredit = [];
                $transGroupType = "bank";
                array_push($debit, [
                    'kode_akun' => $value->kode_nama_akun,
                    'nama_akun' => $value->nama_tipe_akun,
                    'keterangan' => 'Bank '.$value->jenis,
                    'nominal' => $value->nominal,
                ]);
                array_push($kredit, [
                    'kode_akun' => $value->kode_nama_akun,
                    'nama_akun' => $value->nama_bank == "Lain-lain" ? $value->bank_lain : $value->nama_bank,
                    'keterangan' => $value->keterangan,
                    'nominal' => $value->nominal,
                ]);
                array_push($jurnal_harians, [ 'tanggal' => $value->tanggal, 'debit' => $debit, 'kredit' => $kredit ]);
            }
            // =========================================== //

            // ============ Piutang Dagang =============== //
            foreach ($piutang_dagang as $value) {
                $debit = [];
                $kredit = [];
                $transGroupType = 'piutang';
                array_push($debit, [
                    'kode_akun' => $akun_piutang->kode_akun ?? Akun::$piutang_dagang.$perusahaan_prefix,
                    'nama_akun' => $akun_piutang->nama ?? $akun_piutang,
                    'keterangan' => 'Penjualan Kredit',
                    'nominal' => $value->tr_total,
                    // 'ref' => $value->kode_transaksi,
                ]);
                if ($value->tr_diskon > 0) {
                    $transGroupType = 'potongan_piutang';
                    array_push($debit, [
                        'kode_akun' => $akun_potongan_penjualan->kode_akun ?? Akun::$potongan_penjualan.$perusahaan_prefix,
                        'nama_akun' => $akun_potongan_penjualan->nama ?? $akun_potongan_penjualan,
                        'keterangan' => 'Potongan Penjualan',
                        'nominal' => $value->tr_diskon,
                        // 'ref' => $value->kode_transaksi,
                    ]);
                }
                array_push($kredit, [
                    'kode_akun' => $akun_penjualan->kode_akun ?? Akun::$penjualan.$perusahaan_prefix,
                    'nama_akun' => $akun_penjualan->nama ?? $akun_penjualan,
                    'keterangan' => 'Penjualan Kredit',
                    'nominal' => $value->tr_total + $value->tr_diskon,
                    // 'ref' => $value->kode_transaksi,
                ]);
                $transGroupType = date('Y-m-d', strtotime($value->tanggal_transaksi)) . '_' . $transGroupType;
                if (isset($trans_jurnal_harian[$transGroupType])) {
                    $trans_jurnal_harian[$transGroupType]['debit'][0]['nominal'] += $debit[0]['nominal'];
                    if (isset($debit[1])) $trans_jurnal_harian[$transGroupType]['debit'][1]['nominal'] += $debit[1]['nominal'];
                    $trans_jurnal_harian[$transGroupType]['kredit'][0]['nominal'] += $kredit[0]['nominal'];
                } else {
                    $trans_jurnal_harian[$transGroupType] = [ 'tanggal' => $value->tanggal_transaksi, 'debit' => $debit, 'kredit' => $kredit ];
                }
                // array_push($jurnal_harians, [ 'tanggal' => $value->tanggal_transaksi, 'debit' => $debit, 'kredit' => $kredit ]);

                // HPP Persediaan Barang
                // $debit = [];
                // $kredit = [];
                // array_push($debit, [
                //     'kode_akun' => $akun_hpp->kode_akun ?? Akun::$hpp.$perusahaan_prefix,
                //     'nama_akun' => $akun_hpp->nama ?? $akun_hpp,
                //     'keterangan' => 'HPP',
                //     'nominal' => 0,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($kredit, [
                //     'kode_akun' => $akun_persediaan_barang->kode_akun ?? Akun::$persediaan_barang.$perusahaan_prefix,
                //     'nama_akun' => $akun_persediaan_barang->nama ?? $akun_persediaan_barang,
                //     'keterangan' => 'HPP',
                //     'nominal' => 0,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($jurnal_harians, [ 'tanggal' => $value->tanggal_transaksi, 'debit' => $debit, 'kredit' => $kredit ]);
            }
            // =========================================== //

            // ======== Pelunasan Piutang Dagang ========= //
            foreach ($pelunasan_piutang_dagang as $value) {
                $debit = [];
                $kredit = [];
                $transGroupType = 'pelunasan_piutang_'.$value->metode_bayar;
                array_push($debit, [
                    'kode_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->kode_akun ?? Akun::$bank.$perusahaan_prefix) :($akun_kas->kode_akun ?? Akun::$kas.$perusahaan_prefix),
                    'nama_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->nama ?? $akun_bank) :($akun_kas->nama ?? $akun_kas),
                    'keterangan' => 'Pelunasan Piutang',
                    'nominal' => $value->bayar,
                    // 'ref' => $value->kode_transaksi,
                ]);
                array_push($kredit, [
                    'kode_akun' => $akun_piutang->kode_akun ?? Akun::$piutang_dagang.$perusahaan_prefix,
                    'nama_akun' => $akun_piutang->nama ?? $akun_piutang,
                    'keterangan' => 'Pelunasan Piutang',
                    'nominal' => $value->bayar,
                    // 'ref' => $value->kode_transaksi,
                ]);
                $transGroupType = date('Y-m-d', strtotime($value->created_at)) . '_' . $transGroupType;
                if (isset($trans_jurnal_harian[$transGroupType])) {
                    $trans_jurnal_harian[$transGroupType]['debit'][0]['nominal'] += $debit[0]['nominal'];
                    $trans_jurnal_harian[$transGroupType]['kredit'][0]['nominal'] += $kredit[0]['nominal'];
                } else {
                    $trans_jurnal_harian[$transGroupType] = [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ];
                }
                // array_push($jurnal_harians, [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ]);
            }
            // =========================================== //

            // =========== Persediaan Barang ============= //
            foreach ($persediaan_barang as $value) {
                $debit = [];
                $kredit = [];
                $transGroupType = 'persediaan_'.$value->metode_bayar;
                array_push($debit, [
                    'kode_akun' => $akun_persediaan_barang->kode_akun ?? Akun::$persediaan_barang.$perusahaan_prefix,
                    'nama_akun' => $akun_persediaan_barang->nama ?? $akun_persediaan_barang,
                    'keterangan' => 'Pasok Barang',
                    'nominal' => $value->sum_discounted_total,
                    // 'ref' => $value->kode_pasok,
                ]);
                array_push($kredit, [
                    'kode_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->kode_akun ?? Akun::$bank.$perusahaan_prefix) :($akun_kas->kode_akun ?? Akun::$kas.$perusahaan_prefix),
                    'nama_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->nama ?? $akun_bank) :($akun_kas->nama ?? $akun_kas),
                    'keterangan' => 'Pasok Barang',
                    'nominal' => $value->sum_discounted_total,
                    // 'ref' => $value->kode_pasok,
                ]);
                $transGroupType = date('Y-m-d', strtotime($value->created_at)) . '_' . $transGroupType;
                if (isset($trans_jurnal_harian[$transGroupType])) {
                    $trans_jurnal_harian[$transGroupType]['debit'][0]['nominal'] += $debit[0]['nominal'];
                    $trans_jurnal_harian[$transGroupType]['kredit'][0]['nominal'] += $kredit[0]['nominal'];
                } else {
                    $trans_jurnal_harian[$transGroupType] = [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ];
                }
                // array_push($jurnal_harians, [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ]);
            }
            // =========================================== //

            // ============== Aktiva Tetap =============== //
            foreach ($aktiva_tetap as $value) {
                $debit = [];
                $kredit = [];
                array_push($debit, [
                    'kode_akun' => $value->akun ? $value->akun->kode_akun : '',
                    'nama_akun' => $value->akun ? $value->akun->nama : '',
                    'keterangan' => $value->keterangan,
                    'nominal' => $value->nominal,
                ]);
                array_push($kredit, [
                    'kode_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->kode_akun ?? Akun::$bank.$perusahaan_prefix) : ($akun_kas->kode_akun ?? Akun::$kas.$perusahaan_prefix),
                    'nama_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->nama ?? $akun_bank) : ($akun_kas->nama ?? $akun_kas),
                    'keterangan' => 'Kas Keluar',
                    'nominal' => $value->nominal,
                ]);
                array_push($jurnal_harians, [ 'tanggal' => $value->tanggal, 'debit' => $debit, 'kredit' => $kredit ]);
            }
            // =========================================== //

            // ============== Penyusutan Aktiva Tetap =============== //
            foreach ($penyusutan_aktiva_tetap as $value) {
                $checkStartDate = Carbon::parse($value->tanggal)->format('Y-m-01');
                $checkEndDate = Carbon::parse($value->tanggal)->addMonth($value->durasi_aktiva)->format('Y-m-t');
                $checkNowDate = Carbon::parse(date('Y-m', strtotime($start_date)).'-'.date('d', strtotime($value->tanggal)));
                if ($checkNowDate->between($checkStartDate, $checkEndDate)) {
                    $debit = [];
                    $kredit = [];
                    if ($value->akun && $value->akun->kode_akun == Akun::$peralatan_toko.$perusahaan_prefix) {
                        // Penyusutan Peralatan Toko
                        array_push($debit, [
                            'kode_akun' => $akun_biaya_perlengkapan->kode_akun ?? Akun::$biaya_perlengkapan.$perusahaan_prefix,
                            'nama_akun' => $akun_biaya_perlengkapan->nama ?? $akun_biaya_perlengkapan,
                            'keterangan' => $value->keterangan,
                            'nominal' => $value->penyusutan,
                        ]);
                        array_push($kredit, [
                            'kode_akun' => $akun_akumulasi_penyusutan_peralatan_toko->kode_akun ?? Akun::$akumulasi_penyusutan_peralatan_toko.$perusahaan_prefix,
                            'nama_akun' => $akun_akumulasi_penyusutan_peralatan_toko->nama ?? $akun_akumulasi_penyusutan_peralatan_toko,
                            'keterangan' => 'Penyusutan',
                            'nominal' => $value->penyusutan,
                        ]);
                    } else if ($value->akun && $value->akun->kode_akun == Akun::$kendaraan.$perusahaan_prefix) {
                        // Penyusutan Kendaraan
                        array_push($debit, [
                            'kode_akun' => $akun_biaya_penyusutan_kendaraan->kode_akun ?? Akun::$biaya_penyusutan_kendaraan.$perusahaan_prefix,
                            'nama_akun' => $akun_biaya_penyusutan_kendaraan->nama ?? $akun_biaya_penyusutan_kendaraan,
                            'keterangan' => $value->keterangan,
                            'nominal' => $value->penyusutan,
                        ]);
                        array_push($kredit, [
                            'kode_akun' => $akun_akumulasi_penyusutan_kendaraan->kode_akun ?? Akun::$akumulasi_penyusutan_kendaraan.$perusahaan_prefix,
                            'nama_akun' => $akun_akumulasi_penyusutan_kendaraan->nama ?? $akun_akumulasi_penyusutan_kendaraan,
                            'keterangan' => 'Penyusutan',
                            'nominal' => $value->penyusutan,
                        ]);
                    }

                    if ($debit && $kredit) array_push($jurnal_harians, [ 'tanggal' => $checkNowDate->format('Y-m-d'), 'debit' => $debit, 'kredit' => $kredit ]);
                }
            }
            // =========================================== //

            // ============ Retur Penjualan  ============ //
            // foreach ($retur_penjualan as $value) {
                // $debit = [];
                // $kredit = [];
                // array_push($debit, [
                //     'kode_akun' => $akun_retur_penjualan->kode_akun ?? Akun::$retur_penjualan.$perusahaan_prefix,
                //     'nama_akun' => $akun_retur_penjualan->nama ?? $akun_retur_penjualan, 
                //     'keterangan' => $value->keterangan,
                //     'nominal' => $value->sum_total_barang,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($kredit, [
                //     'kode_akun' => $value->r_status == 'hutang' 
                //         ? ($akun_piutang->kode_akun ?? Akun::$piutang_dagang.$perusahaan_prefix)
                //         : ($value->metode_bayar == 'bank' ? ($akun_bank->kode_akun ?? Akun::$bank.$perusahaan_prefix) :($akun_kas->kode_akun ?? Akun::$kas.$perusahaan_prefix)),
                //     'nama_akun' => $value->r_status == 'hutang' 
                //         ? ($akun_piutang->nama ?? $akun_piutang)
                //         : ($value->metode_bayar == 'bank' ? ($akun_bank->nama ?? $akun_bank) :($akun_kas->nama ?? $akun_bank)),
                //     'keterangan' => 'Retur Penjualan',
                //     'nominal' => $value->sum_total_barang,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($jurnal_harians, [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ]);

                // HPP Persediaan Barang
                // $debit = [];
                // $kredit = [];
                // array_push($debit, [
                //     'kode_akun' => $akun_persediaan_barang->kode_akun ?? Akun::$persediaan_barang.$perusahaan_prefix,
                //     'nama_akun' => $akun_persediaan_barang->nama ?? $akun_persediaan_barang,
                //     'keterangan' => 'HPP',
                //     'nominal' => 0,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($kredit, [
                //     'kode_akun' => $akun_hpp->kode_akun ?? Akun::$hpp.$perusahaan_prefix,
                //     'nama_akun' => $akun_hpp->nama ?? $akun_hpp,
                //     'keterangan' => 'HPP',
                //     'nominal' => 0,
                //     'ref' => $value->kode_transaksi,
                // ]);
                // array_push($jurnal_harians, [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ]);
            // }
            // =========================================== //

            // ============ Retur Pembelian  ============ //
            // foreach ($retur_pembelian as $value) {
            //     $debit = [];
            //     $kredit = [];
            //     array_push($kredit, [
            //         'kode_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->kode_akun ?? Akun::$bank.$perusahaan_prefix) :($akun_kas->kode_akun ?? Akun::$kas.$perusahaan_prefix),
            //         'nama_akun' => $value->metode_bayar == 'bank' ? ($akun_bank->nama ?? $akun_bank) :($akun_kas->nama ?? $akun_bank),
            //         'keterangan' => $value->keterangan,
            //         'nominal' => $value->harga_modal,
            //     ]);
            //     array_push($debit, [
            //         'kode_akun' => $akun_retur_pembelian->kode_akun ?? Akun::$retur_pembelian.$perusahaan_prefix,
            //         'nama_akun' => $akun_retur_pembelian->nama ?? $akun_retur_pembelian, 
            //         'keterangan' => 'Retur Pembelian',
            //         'nominal' => $value->harga_modal,
            //     ]);
            //     array_push($jurnal_harians, [ 'tanggal' => $value->created_at, 'debit' => $debit, 'kredit' => $kredit ]);
            // }
            // =========================================== //

            // ================= Jurnal ================== //
            $jurnalGroupBys = $jurnal->groupBy('bukti_transaksi');
            foreach ($jurnalGroupBys as $jurnalGroupBy) {
                $debit = [];
                $kredit = [];
                foreach ($jurnalGroupBy as $jgb) {
                    if ($jgb->debit != 0) {
                        array_push($debit, [
                            'kode_akun' => $jgb->kodeAkun->kode_akun,
                            'nama_akun' => $jgb->kodeAKun->nama,
                            'keterangan' => $jgb->keterangan,
                            'nominal' => $jgb->debit,
                        ]);
                    } else if ($jgb->kredit != 0) {
                        array_push($kredit, [
                            'kode_akun' => $jgb->kodeAkun->kode_akun,
                            'nama_akun' => $jgb->kodeAkun->nama,
                            'keterangan' => $jgb->keterangan,
                            'nominal' => $jgb->kredit,
                        ]);
                    }
                }
                array_push($jurnal_harians, [ 'tanggal' => $jgb->tanggal, 'debit' => $debit, 'kredit' => $kredit]);
            }
            // =========================================== //
            // $all_akuns = Akun::where('perusahaan', $transaction_type)->get();
            // foreach ($all_akuns as $all_akun) {
            //     $saldo_awal_first += SaldoAkhirController::getSaldoAkhir($all_akun, $subEndOfMonth);
            // }
            $saldo_awal_first += SaldoAkhir::where('date', $subEndOfMonth->format('Y-m-d'))->sum('saldo');
        }

        $jurnal_harians = array_merge($jurnal_harians, array_values($trans_jurnal_harian));

        // =============== Filter Akun =============== //
        if ($filter_akuns || $filter_search != null) {
            foreach ($jurnal_harians as $keyJurnalHarian => $jurnal_harian) {
                foreach (array_merge($jurnal_harian['debit'], $jurnal_harian['kredit']) as $jh) {
                    $checked_filtered = false;
                    if ($filter_akuns && in_array($jh['kode_akun'], $filter_akuns)) $checked_filtered = true;
                    if ($filter_search != null && strpos(implode(" ", $jh), $filter_search) !== false) $checked_filtered = true;
                    if ($checked_filtered == true) continue 2;
                }
                // Unset Jurnal Harian
                unset($jurnal_harians[$keyJurnalHarian]);
            }
        }
        // =========================================== //

        // Sorty by Tanggal
        $jurnal_harians = collect($jurnal_harians)->sortBy(function($item) {
            return strtotime($item['tanggal']);
        })->values()->all();;

        $data = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'saldo_awal_first' => $saldo_awal_first,
            'aktiva_lancar' => $aktiva_lancar,
            'aktiva_tetap' => $aktiva_tetap,
            'bank' => $bank,
            'jurnal_harians' => $jurnal_harians,
            'export_type' => $request->type
        ];
        if ($request->type == 'print') {
            // ================= Print ================== //
            return view('printjurnalharian', $data);
        } else if ($request->type == 'pdf') {
            // ================= PDF ================== //
            $pdf = PDF::loadView('exceljurnalharian', $data);
            return $pdf->download('Jurnal Harian '.now().'.pdf');
        } else if ($request->type == 'export') {
            // ================= Export ================== //
            return Excel::download(new ExportJurnalHarian($data), 'Jurnal Harian '.now().'.xlsx');
        } else {
            // ================= View ================== //
            $data['akuns'] = Akun::all();
            $data['barangs'] = Product::all();
            return view('jurnalharian', $data);
        }
    }

    public function viewJurnalHarianSingle(Request $request)
    {
        // ====== Filter Date ====== //
        $start_date = Carbon::now()->format('Y-m-01');
        $end_date = Carbon::now()->format('Y-m-t');

        if ($request->bulan) {
            $start_date = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y-m-01');
            $end_date = Carbon::createFromFormat('Y-m', $request->bulan)->format('Y-m-t');
        }
        if ($request->start_date && $request->end_date) {
            $start_date = Carbon::create($request->start_date)->format('Y-m-d');
            $end_date = Carbon::create($request->end_date)->format('Y-m-d');
        }

        $start_date = $start_date . " 00:00:00";
        $end_date = $end_date . " 23:59:59";
        // ========================= //

        // Filter Barang
        $filter_barangs = $request->barangs ?? [];

        // Filter Search
        $filter_search = $request->search;

        $arr_transaction_type = $request->transaction_type == "all" ? [1, 2] : [$request->transaction_type ?? 1];

        // Filter Akuns
        $filter_akuns = $request->akuns ?? Akun::whereIn('perusahaan', $arr_transaction_type)->pluck('kode_akun')->toArray();

        $buku_besars = $this->getAkunBukuBesar($filter_akuns , $arr_transaction_type, $start_date, $end_date);

        if ($filter_search != null) {
            foreach ($buku_besars as $keyBukuBesar => $buku_besar) {
                if (! (strpos($buku_besar['nama']." ".$buku_besar['kode_akun'], $filter_search) !== false)) {
                    foreach ($buku_besar['jurnals'] as $keyBbj => $bbj) {
                        if (! (strpos(implode(" ", $bbj->toArray()), $filter_search) !== false)) 
                            unset($buku_besars[$keyBukuBesar]['jurnals'][$keyBbj]);
                    }
                    if (count($buku_besars[$keyBukuBesar]['jurnals']) == 0) unset($buku_besars[$keyBukuBesar]);
                }
            }
        }

        $data = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'buku_besars' => $buku_besars,
            'export_type' => $request->type
        ];

        if ($request->type == 'print') {
            // ================= Print ================== //
            return view('printjurnalhariansingle', $data);
        } else if ($request->type == 'pdf') {
            // ================= PDF ================== //
            $pdf = PDF::loadView('exceljurnalhariansingle', $data);
            return $pdf->download('Jurnal Harian per Akun ' . now() . '.pdf');
        } else if ($request->type == 'export') {
            // ================= Export ================== //
            return Excel::download(new ExportJurnalHarianSingle($data), 'Jurnal Harian per Akun ' . now() . '.xlsx');
        } else {
            abort(404);
        }
    }
}
