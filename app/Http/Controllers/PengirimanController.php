<?php

namespace App\Http\Controllers;

use App\Models\Pengiriman;
use App\Models\SPK;
use Illuminate\Http\Request;

class PengirimanController extends Controller
{
    public function show()
    {
        $pengiriman = Pengiriman::all();
        return view('suratjalan', compact('pengiriman'));
    }

    public function showSPK()
    {
        $no_spk = SPK::all();
        return view('tambahpengiriman', compact('no_spk'));
    }

    public function edit(Request $req)
    {
        $pengiriman = Pengiriman::find($req->id);
        $no_spk = SPK::all();
        return view('ubahpengiriman', compact('pengiriman', 'no_spk'));
    }

    public function doAddPengiriman(Request $req)
    {
        Pengiriman::create([
            'no_spk' => $req->no_spk,
            'no_surat_jalan'=>$req->no_surat_jalan,
            'no_kendaraan'=>$req->no_kendaraan,
            'nama_pengiriman'=>$req->nama_pengiriman,
            'nama_penerima'=>$req->nama_penerima,
            'alamat_penerima'=>$req->alamat_penerima,
            'qty'=>$req->qty,
            'tanggal'=>$req->tanggal,
            'status_pengiriman' => 0,
        ]);
        return redirect("/suratjalan");
    }

    public function doEditPengiriman(Request $req)
    {
        Pengiriman::find($req->no_spk)->update([
            'no_surat_jalan'=>$req->no_surat_jalan,
            'no_kendaraan'=>$req->no_kendaraan,
            'nama_pengiriman'=>$req->nama_pengiriman,
            'nama_penerima'=>$req->nama_penerima,
            'alamat_penerima'=>$req->alamat_penerima,
            'qty'=>$req->qty,
            'tanggal'=>$req->tanggal
        ]);
        return redirect("/suratjalan");
    }

    public function delete(Request $request , $id)
    {
        $pengiriman = Pengiriman::withTrashed()->find($id);
        if($pengiriman->trashed()){
            $result = $pengiriman->restore();
        }else{
            $result = $pengiriman->delete();
        }
        if ($result) {
            return redirect('/suratjalan');
        } else {
            return redirect('/suratjalan');
        }
    }

    public function accPengiriman(Request $request)
    {
        Pengiriman::find($request->no_spk)->update([
            'status_pengiriman' => 1,
        ]);
        return back();
    }

    public function declinePengiriman(Request $request)
    {
        Pengiriman::find($request->no_spk)->update([
            'status_pengiriman' => 0,
        ]);
        return back();
    }
}
